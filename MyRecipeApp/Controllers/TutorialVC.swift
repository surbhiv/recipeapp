//
//  TutorialVC.swift
//  MyRecipeApp
//
//  Created by Surbhi on 15/12/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class TutorialVC: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var tutCollection: UICollectionView!
    var tutArray = [AnyObject]()
    @IBOutlet weak var pager: UIPageControl!
    @IBOutlet weak var skipButton: UIButton!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gettutorial()
        
        if NSUserDefaults.standardUserDefaults().boolForKey("ApplicationLaunchedFirstTime") == true {
            dispatch_async(dispatch_get_main_queue(), {
                self.skipButton.setTitle("Done", forState: UIControlState.Normal)
            })
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), {
                self.skipButton.setTitle("Skip", forState: UIControlState.Normal)

            })
        }
        pager.currentPage = 0

    }

    func gettutorial() {
        if Reachability.isConnectedToNetwork() {
            SVProgressHUD.show()
            
            var url = "\(Constants.BASEURL)tutorials"
            
            if DeviceType.IS_IPAD {
                url = "\(Constants.BASEURL)tutorials?device_type=2"
            }
            print(url)
            
            Server.getRequestWithURL(url, completionHandler: { (response) in
                if response[Constants.STATE] as! Int == 1 {
                    SVProgressHUD.dismiss()
                    self.tutArray = response["data"] as! Array<AnyObject>
                    dispatch_async(dispatch_get_main_queue())
                    {
                        self.pager.numberOfPages = self.tutArray.count
                        self.tutCollection.reloadData()
                    }
                }
                else {
                    SVProgressHUD.dismiss()
                    CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"",
                        "message":response["message"]!])
                }
            })
        }
        else {
            
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK:- COLLECTIONVIEW
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        return CGSizeMake(collectionView.bounds.size.width, collectionView.bounds.size.height)
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tutArray.count
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("tutCollectionCell", forIndexPath: indexPath)
        
        let catImage = cell.contentView.viewWithTag(1301) as! UIImageView
        catImage.image = nil
        catImage.contentMode = UIViewContentMode.Redraw
        
        let myActivityIndicator = UIActivityIndicatorView()
        myActivityIndicator.center = cell.contentView.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.activityIndicatorViewStyle = .Gray
        catImage.addSubview(myActivityIndicator)
        myActivityIndicator.startAnimating()

        let urlString = tutArray[indexPath.item]["image"] as! String
        
        Helper.loadImageFromUrlWithIndicator(urlString, view: catImage, indic: myActivityIndicator)
        
        return cell
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        for var cell in self.tutCollection.visibleCells() {
            let itemIndexpath = self.tutCollection.indexPathForCell(cell)
            let item = itemIndexpath!.item
            pager.currentPage = item
        }
    }
    
    @IBAction func skipPressed(sender: AnyObject) {

        
        if NSUserDefaults.standardUserDefaults().boolForKey("ApplicationLaunchedFirstTime") == true {
            dispatch_async(dispatch_get_main_queue(), {
                let viewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("SideMenuNavigation")
                UIApplication.sharedApplication().keyWindow?.rootViewController = viewController
            })
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), {
                NSUserDefaults.standardUserDefaults().setBool(true, forKey: "ApplicationLaunchedFirstTime")
                let login = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("LoginView")
                self.presentViewController(login, animated: true, completion: nil)
            })
        }

        
//        dispatch_async(dispatch_get_main_queue(), {
//            let login = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("LoginView")
//            self.presentViewController(login, animated: true, completion: nil)
//        })

    }

}
