//
//  NotesVC.swift
//  MyRecipeApp
//
//  Created by Surbhi on 17/11/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class NotesVC: UIViewController {
    
    //MARK:- IBOutlets
    
    @IBOutlet weak var noCount: UILabel!
    @IBOutlet weak var notesTable: UITableView!
    
    var notesArray = [AnyObject]()
    
    //MARK:- Start
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if DeviceType.IS_IPAD{
            notesTable.estimatedRowHeight = 125.0
        }
        else{
            notesTable.estimatedRowHeight = 95.0
        }
        
        print(notesTable.contentInset)
        notesTable.rowHeight = UITableViewAutomaticDimension
        getUserNotes()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- SIDE MENU ACTIONS
    @IBAction func toggleSideMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        backItem.tintColor = UIColor.lightGrayColor()
        navigationItem.backBarButtonItem = backItem
    }

    
    //MARK:- Load Notes
    func getUserNotes()
    {
        let userID = NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID)! as String
        if Reachability.isConnectedToNetwork()
        {
            let method = "users/my_notes?user_id="
            Constants.appDelegate.startIndicator()
            
            Server.getRequestWithURL(Constants.BASEURL+method+userID, completionHandler: { (response) in
                
                if response[Constants.STATE] as! Int == 1
                {
                    self.notesArray = response["data"] as! Array<AnyObject>
                    dispatch_async(dispatch_get_main_queue())
                    {
                        CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"",
                            "message":response["message"]!])
                        self.UPDATEView()
                    }
                    Constants.appDelegate.stopIndicator()
                }
                else
                {
                    Constants.appDelegate.stopIndicator()
                    CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"",
                        "message":response["message"]!])
                }

            })
        }
        else
        {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    func UPDATEView()
    {
        if self.notesArray.count > 0 {
            notesTable.hidden = false
            noCount.hidden = true
            notesTable.reloadData()
        }
        else
        {
            notesTable.hidden = true
            noCount.hidden = false
        }
    }
    
    //MARK:- Tableview
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return notesArray.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("NotesCell", forIndexPath: indexPath)
        cell.layer.cornerRadius = 10
        cell.clipsToBounds = true
        
        var dict  =  Dictionary<String, AnyObject>()
        dict = notesArray[indexPath.section] as! Dictionary<String, AnyObject>
        
        
        let name = cell.contentView.viewWithTag(1111) as! UILabel
        let note = cell.contentView.viewWithTag(1112) as! UILabel
        let date = cell.contentView.viewWithTag(1113) as! UILabel
        
        
        let nameStr  = dict["recipe_name"] as? String
        let noteStr  = dict["recipe_note"] as? String
        let dateStr  = dict["created_on"] as? String

        name.text = nameStr!
        note.text = noteStr!
        date.text = dateStr!

       
        return cell
    }
}
