//
//  EditProfileView.swift
//  MyRecipeApp
//
//  Created by Surbhi on 24/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class EditProfileView: UIViewController,ENSideMenuDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource
{

    // MARK: OUTLETS
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var changePasswordButton: UIButton!
    @IBOutlet weak var editprofileButton: UIButton!
    @IBOutlet weak var deactivateAccountButton: UIButton!

    @IBOutlet weak var editPictureButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var mobileText: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var add1Text: UITextField!
    @IBOutlet weak var add2Text: UITextField!
    @IBOutlet weak var stateText: UITextField!
    @IBOutlet weak var cityText: UITextField!
    @IBOutlet weak var pinText: UITextField!
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!

    var profileDict = Dictionary<String,AnyObject>()
    let imagePicker = UIImagePickerController()
    var imageDataIMG  : UIImage?

    var imageData : String = ""
    var stateID = String()
    var cityID = String()
    var gender = String()
    var stateArray = [AnyObject]()
    var cityArray = [AnyObject]()
    var pickerView_Profile = UIPickerView()
    
    
    
    
    // MARK: Start
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.sideMenuController()?.sideMenu?.delegate = self
        self.navigationController?.navigationBarHidden = true
        
        editprofileButton.selected = false
        editPictureButton.hidden = true
        saveButton.hidden = true
        nameText.userInteractionEnabled = false
        mobileText.userInteractionEnabled = false
        emailText.userInteractionEnabled = false
        add1Text.userInteractionEnabled = false
        add2Text.userInteractionEnabled = false
        stateText.userInteractionEnabled = false
        cityText.userInteractionEnabled = false
        pinText.userInteractionEnabled = false
        maleButton.userInteractionEnabled = false
        femaleButton.userInteractionEnabled = false
        imagePicker.delegate = self
        
        profileDict = NSUserDefaults.standardUserDefaults().objectForKey(Constants.PROFILE) as! Dictionary<String,AnyObject>
        
        getState()
        setProfile()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(passwordChangedNotiCalled), name: "PasswordChanged", object: nil)
        
    }
    //MARK: - SETTING PROFILE
    func setProfile() {
        nameText.text = profileDict["name"] as? String
        mobileText.text = profileDict["mobile"] as? String
        emailText.text = profileDict["email"] as? String
        add1Text.text = profileDict["address"] as? String
        add2Text.text = profileDict["address_2"] as? String
        stateText.text = profileDict["state"] as? String
        cityText.text = profileDict["city"] as? String
        pinText.text = profileDict["pincode"] as? String
        
        let pict = profileDict["profile_pic"] as? String
        
        profileImage.contentMode = UIViewContentMode.ScaleAspectFit
        
        if pict != "" || pict != nil {
            print("inside")
            Helper.loadImageFromUrl(pict!, view:profileImage,width: 125.0)
        }
        
        let genderstr = profileDict["gender"] as? String
        if genderstr != "" || genderstr != nil {
            if genderstr?.lowercaseString == "male" {
                maleButton.selected = true
                femaleButton.selected = false
                gender = "male"
            }
            else if genderstr?.lowercaseString == "female" {
                maleButton.selected = false
                femaleButton.selected = true
                gender = "female"
            }
        }
        
        
        if profileDict["state_id"] == nil {
            stateID = ""
            cityID = ""
        }
        else {
            stateID = profileDict["state_id"] as! String
            cityID = profileDict["city_id"] as! String
        }

        self.setStackView()
    }
    
    func setStackView() {
        
        let passwordBlank = String(NSUserDefaults.standardUserDefaults().objectForKey(Constants.PASSWORD)!)

            self.stackView.removeArrangedSubview(changePasswordButton)
            self.stackView.removeArrangedSubview(deactivateAccountButton)

            if passwordBlank == "0"
            {
                self.stackView.addArrangedSubview(changePasswordButton)
                self.stackView.addArrangedSubview(deactivateAccountButton)
            }
            else
            {
                self.stackView.addArrangedSubview(deactivateAccountButton)
            }
        
        stackView.translatesAutoresizingMaskIntoConstraints = false;
    }
    
    // MARK:- GET STATE
    func getState() {
        if Reachability.isConnectedToNetwork() {
            Constants.appDelegate.startIndicator()
            let method = "users/states"
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                if response[Constants.STATE] as! Int == 1 {
                    self.stateArray = response["data"] as! Array<AnyObject>
                    if self.stateArray.count > 0 {
                        self.pickerView_Profile = UIPickerView.init()
                        self.pickerView_Profile.dataSource = self
                        self.pickerView_Profile.delegate = self
                        self.pickerView_Profile.reloadAllComponents()
                        
                        self.stateText.inputView = self.pickerView_Profile
                        self.stateText.delegate = self
                        
                        self.cityText.inputView = self.pickerView_Profile
                        self.cityText.delegate = self
                    }
                    Constants.appDelegate.stopIndicator()

                }
                else {
                    Constants.appDelegate.stopIndicator()

                    CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"",
                        "message":"Unable to fetch State List!"])
                }

            })
        }
        else {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }

    // MARK : GET CIty
    func getCity(stateID : String) {
        resgnTextFields()
        
        if Reachability.isConnectedToNetwork() {
            let method = "users/cities?state_id="+stateID
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                if response[Constants.STATE] as! Int == 1 {
                    self.cityArray = response["data"] as! Array<AnyObject>
                }
                else {
                    CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"",
                        "message":"Unable to fetch City List!"])
                }
            })

        }
        else {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    // MARK : GET CIty
    func GetUpdatedProfiel() {
        
        if Reachability.isConnectedToNetwork() {
            Constants.appDelegate.startIndicator()
            let method = "users/"+NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID)!
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                if response[Constants.STATE] as! Int == 1 {
                    let dict = response["data"] as! Array<AnyObject>
                    NSUserDefaults.standardUserDefaults().setObject(dict[0], forKey:Constants.PROFILE)
                    self.profileDict = dict[0] as! Dictionary<String,AnyObject>

                    dispatch_async(dispatch_get_main_queue()) {
                        
                        let mo = self.sideMenuController()?.sideMenu?.menuViewController as! MenuTableView
                        mo.tableView.reloadData()
                        
                        self.EditProfilePressed(self.editprofileButton)
                        self.editprofileButton.selected = false
                        self.setProfile()
                    }
                    Constants.appDelegate.stopIndicator()

                }
                else {
                    Constants.appDelegate.stopIndicator()

                    CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"",
                        "message":"Unable to fetch City List!"])
                }

            })

        }
        else {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    // MARK:- Navigation Buttons
    
    
    @IBAction func toggleSideMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    // MARK:- Button Actions

    @IBAction func changePasswordPressed(sender: AnyObject) {
        
        resgnTextFields()
        let forgot = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("ForgotPassword") as! ForgotPasswordView
        forgot.isForgetView = false
        forgot.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext
        self.presentViewController(forgot, animated: true, completion: nil)

    }
    
    @IBAction func EditProfilePressed(sender: AnyObject) {
        
        if !editprofileButton.selected
        {
            editPictureButton.hidden = false
            saveButton.hidden = false
            nameText.userInteractionEnabled = true
            mobileText.userInteractionEnabled = true
            emailText.userInteractionEnabled = false
            add1Text.userInteractionEnabled = true
            add2Text.userInteractionEnabled = true
            stateText.userInteractionEnabled = true
            Helper.setImageOntextField(stateText)
            
            cityText.userInteractionEnabled = true
            Helper.setImageOntextField(cityText)

            pinText.userInteractionEnabled = true
            maleButton.userInteractionEnabled = true
            femaleButton.userInteractionEnabled = true
            editprofileButton.selected = true
        }
        else
        {
            editprofileButton.selected = false
            editPictureButton.hidden = true
            saveButton.hidden = true
            nameText.userInteractionEnabled = false
            mobileText.userInteractionEnabled = false
            emailText.userInteractionEnabled = false
            add1Text.userInteractionEnabled = false
            add2Text.userInteractionEnabled = false
            stateText.userInteractionEnabled = false
            cityText.userInteractionEnabled = false
            pinText.userInteractionEnabled = false
            maleButton.userInteractionEnabled = false
            femaleButton.userInteractionEnabled = false
        }
    }
    
    @IBAction func DeActivateccountPressed(sender: AnyObject) {
        
        resgnTextFields()
        let alert = UIAlertController.init(title: "Culineria", message: "Are you sure, you want to deactivate your account.", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
            
            self.deactivateMethod()
        }))
        alert.addAction(UIAlertAction.init(title: "CANCEL", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
            self.dismissViewControllerAnimated(true, completion: nil)
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func SaveProfilePressed(sender: AnyObject) {
        
        resgnTextFields()
        
        if nameText.text == "" || nameText.text == nil
        {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message": "Enter user name !"] )
        }
        else if mobileText.text != "" && !Helper.isValidPhoneNumber(mobileText.text!){
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message": "Enter valid mobile number !"] )
        }
        else
        {
            if profileImage.image == nil
            {
                // no imagedata
                let param = "user_id=\(NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID)!)&name=\(nameText.text! as String)&email=\(emailText.text! as String)&mobile=\(mobileText.text!)&profile_pic=&gender=\(gender)&address=\(add1Text.text!)&address_2=\(add2Text.text!)&state_id=\(stateID)&city_id=\(cityID)&pincode=\(pinText.text!)"
                saveEditedProfile(param)
            }
            else
            {
                if imageDataIMG == "" || imageDataIMG == nil {
                    // no imagedata
                    let param = "user_id=\(NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID)!)&name=\(nameText.text! as String)&email=\(emailText.text! as String)&mobile=\(mobileText.text!)&profile_pic=&gender=\(gender)&address=\(add1Text.text!)&address_2=\(add2Text.text!)&state_id=\(stateID)&city_id=\(cityID)&pincode=\(pinText.text!)"
                    saveEditedProfile(param)

                }
                else {
                    
                    let imgData = UIImagePNGRepresentation(imageDataIMG!)! as NSData
                    let imageSize: Int = imgData.length

                    if imageSize/1024 >  800 {
                        let alert = UIAlertController.init(title: "Culineria", message: "Image size is larger than allowed. Please select another image!", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in

                        }))
                        self.presentViewController(alert, animated: true, completion: nil)

                    }
                    else {
                        imageData = imgData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.EncodingEndLineWithCarriageReturn)
                        let param = "user_id=\(NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID)!)&name=\(nameText.text! as String)&email=\(emailText.text! as String)&mobile=\(mobileText.text!)&profile_pic=\(imageData)&gender=\(gender)&address=\(add1Text.text!)&address_2=\(add2Text.text!)&state_id=\(stateID)&city_id=\(cityID)&pincode=\(pinText.text!)"
                        saveEditedProfile(param)
                    }
                }
            }
        }
    }
    
    @IBAction func MaleButtonPressed(sender: AnyObject) {
        resgnTextFields()

        maleButton.selected = true
        femaleButton.selected = false
        gender = "male"
    }
    
    @IBAction func FeMaleButtonPressed(sender: AnyObject) {
        resgnTextFields()

        maleButton.selected = false
        femaleButton.selected = true
        gender = "female"
    }
   
    @IBAction func EditPicturePressed(sender: AnyObject) {
        resgnTextFields()

        imagePicker.allowsEditing = false
        imagePicker.sourceType = .PhotoLibrary
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: IMAGE PICKER Delegates
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            profileImage.contentMode = .ScaleAspectFit
            profileImage.image = pickedImage
            
            let resizedImage = Helper.resizeImage(pickedImage, newWidth: 350.0)
            imageDataIMG = resizedImage
        }
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK: - RESIGN Textfield
    func resgnTextFields() {
        nameText.resignFirstResponder()
        mobileText.resignFirstResponder()
        emailText.resignFirstResponder()
        add1Text.resignFirstResponder()
        add2Text.resignFirstResponder()
        stateText.resignFirstResponder()
        cityText.resignFirstResponder()
        pinText.resignFirstResponder()
    }
    
    //MARK: - DEACTIVATE Method

    func passwordChangedNotiCalled(noti: NSNotification) {
        
        self.performSelector(#selector(logoutAfterDeactivate), withObject: nil, afterDelay: 2.0)
//        self.logoutAfterDeactivate()
    }
    
    
    
    func deactivateMethod() {
        
        if Reachability.isConnectedToNetwork() {
            
            let method = "users/deactivate"
            let param = "user_id=\(NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID)!)"
            Constants.appDelegate.startIndicator()
            Server.postRequestWithURL(Constants.BASEURL+method, paramString: param) { (response) in
                
                Constants.appDelegate.stopIndicator()
                
                if response[Constants.STATE] as! Int == 1 {
                    let alert = UIAlertController.init(title: "Culineria", message: response["message"]! as? String, preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                        self.logoutAfterDeactivate()
                    }))
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                }
                else {
                    let alert = UIAlertController.init(title: "Culineria", message: response["message"]! as? String, preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                    }))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }
        }
        else {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    //MARK: - LOGOUT Method

    
    func logoutAfterDeactivate() {
        
        GIDSignIn.sharedInstance().signOut()
        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
        loginManager.logOut()
        
        Helper.resetCount()
        
        NSUserDefaults.standardUserDefaults().removeObjectForKey(Constants.UserID)
        NSUserDefaults.standardUserDefaults().removeObjectForKey(Constants.PROFILE)
        NSUserDefaults.standardUserDefaults().removeObjectForKey(Constants.PASSWORD)

        let mo = self.sideMenuController()?.sideMenu?.menuViewController as! MenuTableView
        mo.UpdateMenuView()
        
        let destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("LoginView")
        self.presentViewController(destViewController, animated: true, completion: nil);
        
    }

    //MARK: - EDIT PROFILE Method
    
    
    func saveEditedProfile(parameter : String) {
        if Reachability.isConnectedToNetwork() {
            let method = "users/edit_profile"
            Constants.appDelegate.startIndicator()
            Server.postRequestWithURL(Constants.BASEURL+method, paramString: parameter) { (response) in
                
                Constants.appDelegate.stopIndicator()

                if response[Constants.STATE] as! Int == 1 {
                    let alert = UIAlertController.init(title: "Culineria", message: response["message"]! as? String, preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                        
                        
                        self.GetUpdatedProfiel()
                    }))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                else {
                    let mess = response["message"] as! String
                    CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"", "message":mess])
                }
            }
        }
        else
        {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    //MARK:- PICK DELEGATES
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if stateText.isFirstResponder() {
            if stateArray.count > 0 {
                return stateArray.count
            }
        }
        else if cityText.isFirstResponder() {
            if cityArray.count > 0 {
                return cityArray.count
            }
        }
        return 0
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if stateText.isFirstResponder() {
            if stateArray.count > 0 {
                
                let dict = stateArray[row] as! Dictionary<String,AnyObject>
                let name = dict["name"] as! String
                return name
            }
        }
        else if cityText.isFirstResponder() {
            if cityArray.count > 0 {

                let dict = cityArray[row] as! Dictionary<String,AnyObject>
                let name = dict["name"] as! String
                return name
            }
        }
        return ""
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if stateText.isFirstResponder() {
            
            if stateArray.count > 0 {
                let dict = stateArray[row] as! Dictionary<String,AnyObject>
                let name = dict["name"] as! String
                stateID = dict["id"] as! String
                stateText.text = name
                getCity(stateID)
            }
            
        }
        else if cityText.isFirstResponder() {
            
            if cityArray.count > 0 {
                let dict = cityArray[row] as! Dictionary<String,AnyObject>
                let name = dict["name"] as! String
                cityID = dict["id"] as! String
                cityText.text = name
            }
        }
    }
    
    
    //MARK:- Textfield
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == mobileText {
            if (range.location > 9)
            {
                return false
            }
        }
        if textField == pinText {
            if (range.location > 5)
            {
                return false
            }
        }
        return true
    }
}
