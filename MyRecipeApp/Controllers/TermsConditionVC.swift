//
//  TermsConditionVC.swift
//  MyRecipeApp
//
//  Created by Surbhi on 11/11/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class TermsConditionVC: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var mywebview: UIWebView!
    var loadingURLSTR = String()
    @IBOutlet weak var backButton: UIButton!
    var isComingFromSideMenu : Bool = false
    
    @IBOutlet weak var searchrecipeButton: UIButton!
    
    //MARK: - VIEW LOADING
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mywebview.layer.shadowColor = Colors.ShadowColor.CGColor
        mywebview.layer.borderColor = Colors.ShadowColor.CGColor
        mywebview.layer.borderWidth = 0.5
        mywebview.clipsToBounds = true
        mywebview.dataDetectorTypes = UIDataDetectorTypes.Link//.dataDetectorTypes=UIDataDetectorTypeLink
        mywebview.delegate = self
        
        self.navigationController?.navigationBarHidden = true
        loadDescription()
        
        if self.navigationController != nil {
            isComingFromSideMenu = true
            searchrecipeButton.hidden = false
            backButton.setImage(UIImage.init(named: "sideMenu"), forState: .Normal)
        }
        else
        {
            isComingFromSideMenu = false
            searchrecipeButton.hidden = true
            backButton.setImage(UIImage.init(named: "back"), forState: .Normal)

        }
    }
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if request.URL?.scheme == "mailto" {
            
            print(request.URL)
            let url = request.URL!
            UIApplication.sharedApplication().openURL(url)
            
        }
        
        return true
    }
    
    
    //MARK: - GET DESCRIPTION
    func loadDescription() // recipes/featured/user_id/
    {
        if Reachability.isConnectedToNetwork()
        {
            Constants.appDelegate.startIndicator()
            let method = "content/"
            
            Server.getRequestWithURL(Constants.BASEURL+method+"terms-of-use", completionHandler: { (response) in
                
                Constants.appDelegate.stopIndicator()
                if response[Constants.STATE] as! Int == 1
                {
             
                    let dict = response["data"] as! Array<AnyObject>
                    self.loadingURLSTR = dict[0]["description"] as! String
                    self.mywebview.loadHTMLString(self.loadingURLSTR, baseURL: nil)
                }
                else
                {
                    let alert = UIAlertController.init(title: "Culineria", message: response["message"]! as? String, preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                        self.dismissViewControllerAnimated(true, completion: nil)
                    }))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            })
        }
        else
        {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
//    - (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
//    if ([[[request URL] scheme] isEqual:@"mailto"]) {
//    [[UIApplication sharedApplication] openURL:[request URL]];
//    return NO;
//    }
//    return YES;
//    }
    
    
    @IBAction func BackButtonPressed(sender: AnyObject)
    {
        if isComingFromSideMenu == true
        {
            toggleSideMenuView()
        }
        else
        {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        backItem.tintColor = UIColor.lightGrayColor()
        navigationItem.backBarButtonItem = backItem
    }
    
}
