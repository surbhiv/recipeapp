//
//  modelSelectionVC.swift
//  MyRecipeApp
//
//  Created by Surbhi on 21/11/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class modelSelectionVC: UIViewController{//, UITableViewDelegate {//, UITableViewDataSource

    
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var modelTable: UITableView!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var CancelButton: UIButton!
    
    var modelArray = [AnyObject]()
    var TopModelArray = [AnyObject]()
    var selectedTopModel = Int()

    var selectedSections = NSMutableIndexSet()
    
    var modelArraySelected = [AnyObject]()

    //MARK:- START
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if DeviceType.IS_IPAD
        {
            viewHeight.constant = (ScreenSize.SCREEN_HEIGHT - 150)/2
        }
        else if DeviceType.IS_IPHONE_4_OR_LESS
        {
            viewHeight.constant = 300
        }
        else if DeviceType.IS_IPHONE_5
        {
            viewHeight.constant = 380
        }
        else if DeviceType.IS_IPHONE_6
        {
            viewHeight.constant = 430
        }
        else
        {
            viewHeight.constant = 475
        }
        
        self.getData()

    }
    
    func getData(){
        
        if Reachability.isConnectedToNetwork() {
            Constants.appDelegate.startIndicator()
            
            let method = "microwaves?top_category_id=0"
            
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                Constants.appDelegate.stopIndicator()
                
                if response[Constants.STATE] as! Int == 1 {

                    var arr = [AnyObject]()

                    for item in response["data"] as! Array<AnyObject>
                    {
                        let dict = item as! Dictionary<String, AnyObject>
                        let newDIct = NSMutableDictionary()
                        
                        let idStr = dict["id"] as! String
                        let nameStr = dict["title"] as! String
                        let imageStr = dict["image"] as! String
                        let volStr = dict["capacity"] as! String
                        let topCatID = dict["top_category_id"] as! String
                        
                        newDIct.setObject(idStr, forKey: "id")
                        newDIct.setObject(nameStr, forKey: "name")
                        newDIct.setObject(imageStr, forKey: "image")
                        newDIct.setObject(volStr, forKey: "capacity")
                        newDIct.setObject(topCatID, forKey: "top_category_id")
                        
                        arr.append(newDIct)
                    }
                    
                    self.modelArray = arr
                    if self.modelArray.count > 0{
                        dispatch_async(dispatch_get_main_queue()) {
                            self.modelTable.reloadData()
                        }
                    }
                    else{
                        CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"No Record Found"])
                        self.selectedSections.removeAllIndexes()
                        self.dismissViewControllerAnimated(true, completion: nil)
                    }
                }
                else{
                    CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Some Error Occurred"])
                    self.selectedSections.removeAllIndexes()
                    self.dismissViewControllerAnimated(true, completion: nil)

                }
                Constants.appDelegate.stopIndicator()
            })
        }
        else {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }

    override func viewDidAppear(animated: Bool) {
        selectedSections.removeAllIndexes()
    }
    
    @IBAction func cancelPressed(sender: AnyObject) {
        
        selectedSections.removeAllIndexes()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func OkPressed(sender: AnyObject) {
        
        modelArraySelected.removeAll()

        if selectedSections.count > 0 {
            if selectedSections.containsIndex(0) {
                
                for diction in modelArray {
                    let dict = diction as! Dictionary<String,AnyObject>
                    let modelName = dict["name"] as? String
                    let modelId = dict["id"] as? String
                    let topModelId = dict["top_category_id"] as? String
                    var dict2 = [String: String]()
                    dict2 = ["name" : modelName!, "id" : modelId!, "top_category_id" : topModelId! ]
                    modelArraySelected.append(dict2)
                }
                NSNotificationCenter.defaultCenter().postNotificationName("ModelsSelectedNoti", object: nil, userInfo: ["infoData": modelArraySelected,"AllModelSelectedKey":true])

                
            }
            else {
                for index in selectedSections {
                    let dict = modelArray[index - 1] as! Dictionary<String,AnyObject>
                    let modelName = dict["name"] as? String
                    let modelId = dict["id"] as? String
                    let topModelId = dict["top_category_id"] as? String

                    var dict2 = [String: String]()
                    dict2 = ["name" : modelName!, "id" : modelId! , "top_category_id" : topModelId!]
                    modelArraySelected.append(dict2)
                }
                NSNotificationCenter.defaultCenter().postNotificationName("ModelsSelectedNoti", object: nil, userInfo: ["infoData": modelArraySelected,"AllModelSelectedKey":false])
            }
            
            
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
}


// MARK: - UITableViewDataSource
extension modelSelectionVC: UITableViewDelegate, UITableViewDataSource {

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modelArray.count+1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("microwaveSelectionCell", forIndexPath: indexPath)
        
        let title = cell.contentView.viewWithTag(1075) as! UILabel
        let img2 = cell.contentView.viewWithTag(1076) as! UIImageView
        
        if indexPath.row == 0 {
            title.text = "All Models"
        }
        else {
            let dict = modelArray[indexPath.row - 1] as! Dictionary<String,AnyObject>
            title.text = dict["name"] as? String
        }
        
        let CurrentZero : Bool  = selectedSections.containsIndex(0)
        
        if CurrentZero == true {
            img2.image = UIImage.init(named: "CheckBoxSelected")
        }
        else {
            let current : Bool = selectedSections.containsIndex(indexPath.row)
            if current == true {
                img2.image = UIImage.init(named: "CheckBoxSelected")
            }
            else {
                img2.image = UIImage.init(named: "checkBoxEmpty")
            }
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
        let current : Bool = selectedSections.containsIndex(indexPath.row)
        let containZero : Bool = selectedSections.containsIndex(0)
        
        if indexPath.row == 0 && current == false {
            if  selectedSections.count > 0 {
                selectedSections.removeAllIndexes()
            }
            
            selectedSections.addIndexesInRange(NSRange.init(location: 0, length: modelArray.count + 1))
            dispatch_async(dispatch_get_main_queue()) {
                self.modelTable.reloadSections(NSIndexSet.init(index: 0), withRowAnimation: .Automatic)
            }
        }
        else if indexPath.row == 0 && current == true {
            selectedSections.removeAllIndexes()
            dispatch_async(dispatch_get_main_queue()) {
                self.modelTable.reloadSections(NSIndexSet.init(index: 0), withRowAnimation: .Automatic)
            }
        }
        else {
            if current == true && containZero == true {
                let indexp = NSIndexPath.init(forRow: 0, inSection: 0)
                selectedSections.removeIndex(0)
                selectedSections.removeIndex(indexPath.row)
                dispatch_async(dispatch_get_main_queue()) {
                    self.modelTable.reloadRowsAtIndexPaths([indexPath,indexp], withRowAnimation: UITableViewRowAnimation.Automatic)
                }
            }
            else if current == true && containZero ==  false {
                selectedSections.removeIndex(indexPath.row)
                dispatch_async(dispatch_get_main_queue()) {
                    self.modelTable.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
                }
            }
            else {
                selectedSections.addIndex(indexPath.row)
                dispatch_async(dispatch_get_main_queue()) {
                    self.modelTable.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
                }
            }
        }
    }
}
