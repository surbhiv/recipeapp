//
//  SearchRecipeView.swift
//  MyRecipeApp
//
//  Created by Surbhi on 24/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class SearchRecipeView: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var searchText: UITextField?
    @IBOutlet weak var tableView: UITableView?

    var txtAfterUpdate : NSString!
    var recipeArray=[AnyObject]()
    var copyRecipeArray = [AnyObject]()
    let maxViewBeforeLogin = 9
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchText?.delegate = self
        
        self.navigationController?.navigationBarHidden = true
        
        searchText!.attributedPlaceholder = NSAttributedString(string:"Search Recipe",
                                                               attributes:[NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        
        searchText?.becomeFirstResponder()
        recipeArray = NSUserDefaults.standardUserDefaults().objectForKey(Constants.RECIPE) as! [AnyObject]
        dispatch_async(dispatch_get_main_queue()) {
            // Do stuff to UI
            self.tableView?.reloadData()
            
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
    }
    
    @IBAction func backButtonPressed(sender: AnyObject) {
        
        if self.navigationController == nil
        {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        else
        {
            self.navigationController?.navigationBarHidden = true
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    @IBAction func  searchTextFieldDidChange(sender: AnyObject)
    {
//        if !(searchText?.text?.isEmpty)! {
//            print("not Empty")
//        }
//        else
//        {
//            print("EMPTY")
//        }
    }
    
    
    // MARK: TABLEVIEW 
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recipeArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("searchCell")
        
        if (cell == nil) {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "searchCell")
            cell!.backgroundColor = UIColor.clearColor()
        }
        
        cell?.contentView.layer.borderWidth = 1.0
        cell?.contentView.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        let dishPic = cell?.contentView.viewWithTag(1001) as! UIImageView
        dishPic.image = nil
        dishPic.contentMode = UIViewContentMode.ScaleAspectFill
        let imgURL: String = (recipeArray[indexPath.row]["image_1"] as? String)!
        
        let myActivityIndicator = UIActivityIndicatorView()
        myActivityIndicator.center = dishPic.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.activityIndicatorViewStyle = .Gray
        dishPic.addSubview(myActivityIndicator)
        myActivityIndicator.startAnimating()
        
        Helper.loadImageFromUrlWithIndicator(imgURL, view: dishPic, indic: myActivityIndicator)
        
        let dishName = cell?.contentView.viewWithTag(1002) as! UILabel
        dishName.text = recipeArray[indexPath.row][Constants.NAME] as? String
        
        let dishRate = cell?.contentView.viewWithTag(1003) as! TPFloatRatingView
        dishRate.backgroundColor = UIColor.clearColor()
        dishRate.emptySelectedImage = UIImage.init(named: "starEmpty")
        dishRate.fullSelectedImage = UIImage.init(named: "starFull")
        dishRate.contentMode = UIViewContentMode.ScaleAspectFill
        dishRate.editable = false
        dishRate.halfRatings = true;
        dishRate.floatRatings = true;
        dishRate.maxRating = 5
        dishRate.minRating = 0
        
        if recipeArray[indexPath.row]["star_rating"] != nil
        {
            let rate  = Float((recipeArray[indexPath.row]["star_rating"] as? String)!)
            dishRate.rating = CGFloat.init(rate!)
        }
        else
        {
            dishRate.rating = 0.0
        }

        
        let dishTime = cell?.contentView.viewWithTag(1004) as! UILabel
        
        let time  = Int((recipeArray[indexPath.row]["cooking_time"] as? String)!)
        
        if time < 60
        {
            let aStr = String(format: "%d Min", time!)
            dishTime.text = aStr
        }
        else
        {
            let aStr = String(format: "%d Min", time!)
            dishTime.text = aStr
        }
        

        
//        dishTime.text = recipeArray[indexPath.row]["cooking_time"] as? String
        dishTime.layer.cornerRadius = 5.0
        dishTime.clipsToBounds = true
        
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let destViewController  = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("RecipeDetail") as! RecipeDetailView
        
        var dict = Dictionary<String,AnyObject>()
        dict = recipeArray[indexPath.row] as! Dictionary<String,AnyObject>
        destViewController.recipeIDSTR = dict["id"] as! String
        
        
        let count = Helper.getOpenCount()

        if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == ""
        {
            if count <= maxViewBeforeLogin {
                
                Helper.incrementOpenCount()
                
                if self.navigationController == nil {
                    self.presentViewController(destViewController, animated: true, completion: nil)
                }
                else
                {
                    self.navigationController?.pushViewController(destViewController, animated: true)
                }
                
            }
            else
            {
                
                let alert = UIAlertController.init(title: "Culineria", message: "Login to view the recipe details.", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction.init(title: "LOGIN", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                    
                    let destinationController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("LoginView") as! LoginView
                    Constants.appDelegate.isRecipeDetail = true
                    self.navigationController?.presentViewController(destinationController, animated: true, completion: nil)
                    
                }))
                alert.addAction(UIAlertAction.init(title: "CANCEL", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                    self.dismissViewControllerAnimated(true, completion: nil)
                }))
                
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
        else
        {
            if self.navigationController == nil {
                self.presentViewController(destViewController, animated: true, completion: nil)
            }
            else
            {
                self.navigationController?.pushViewController(destViewController, animated: true)
            }
            Helper.resetCount()
        }
    }
    
    
    // MARK: Search Recipe
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        txtAfterUpdate = self.searchText!.text! as NSString
        txtAfterUpdate = txtAfterUpdate.stringByReplacingCharactersInRange(range, withString: string)
        
        self.textFieldDidChange()
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        return true;
    }


    func textFieldDidChange() {
        
        copyRecipeArray = NSUserDefaults.standardUserDefaults().objectForKey(Constants.RECIPE) as! [AnyObject]

        if txtAfterUpdate == "" || txtAfterUpdate == nil {
            recipeArray.removeAll(keepCapacity: false)
            recipeArray = copyRecipeArray
        }
        else
        {
            recipeArray.removeAll(keepCapacity: false)
            
            for dict in copyRecipeArray {
                let recipeName = dict[Constants.NAME] as! String
                if recipeName.lowercaseString.rangeOfString(txtAfterUpdate.lowercaseString) != nil
                {
                    recipeArray.append(dict)
                }
            }

        }
        tableView?.reloadData()
    }
   
}
