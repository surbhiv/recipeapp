//
//  RecipeCategoryVC.swift
//  MyRecipeApp
//
//  Created by Surbhi on 15/11/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class RecipeCategoryVC: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,ENSideMenuDelegate, UITableViewDataSource, UITableViewDelegate {

    //MARK:- IBOUTLETS
    
    @IBOutlet weak var recipeCollection: UICollectionView!
    @IBOutlet weak var modelTypeView: UIView!
    @IBOutlet weak var modelSelectionView: UIView!
    @IBOutlet weak var modelTable: UITableView!
    
    @IBOutlet weak var topCategoryView: UIView!
    @IBOutlet weak var topCategoryTable: UITableView!
    
    @IBOutlet weak var viwAllBtn: UIButton!
    @IBOutlet weak var selectModBtn: UIButton!

    
    //MARK:- GLOBAL VARIABLES
    var topModelArray = [AnyObject]() // super Category Selection -- Model
    var selectedTopCategory = Int() // super Category Selection --  Model

    
    var recipeCatArray = [AnyObject]()
    let myActivityIndicator = UIActivityIndicatorView()
    var modelArray = [AnyObject]()
    var builtInOvenArray = [AnyObject]() // super Category Selection -- Model
    var builtInMicroArray = [AnyObject]() // super Category Selection -- Model

    var selectedSections = NSMutableIndexSet()
    var mwotopCatId = Int() // microwave top mmodel category id
    //MARK:- START
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viwAllBtn.layer.borderColor = UIColor.init(red: 101.0/255.0, green: 76.0/255.0, blue: 59.0/255.0, alpha: 1.0).CGColor
        viwAllBtn.layer.borderWidth = 1.5
        selectModBtn.layer.borderColor = UIColor.whiteColor().CGColor
        selectModBtn.layer.borderWidth = 1.5
        selectModBtn.clipsToBounds = true
        viwAllBtn.clipsToBounds = true
        
        topModelArray = NSUserDefaults.standardUserDefaults().objectForKey(Constants.TOPMODEL) as! Array<AnyObject>

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(setUPView), name: "ShowModelSelectionView", object: nil)
        
        dispatch_async(dispatch_get_main_queue()) {
            self.topCategoryView.hidden = true
            self.modelTypeView.hidden = true
            self.modelSelectionView.hidden = true
            self.recipeCollection.hidden = true
            self.setUPView()
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    //MARK:- SIDE MENU ACTIONS
    
    @IBAction func toggleSideMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "goToRecipesInCategory" {
            let backItem = UIBarButtonItem()
            backItem.title = ""
            backItem.tintColor = UIColor.lightGrayColor()
            navigationItem.backBarButtonItem = backItem
            navigationController?.navigationBarHidden = true
        }
        else
        {
            let backItem = UIBarButtonItem()
            backItem.title = ""
            backItem.tintColor = UIColor.lightGrayColor()
            navigationItem.backBarButtonItem = backItem
        }
    }
    
    
    //MARK:- LOAD DATA
    
    func getRecipeData()
    {
        if Reachability.isConnectedToNetwork()
        {
            Constants.appDelegate.startIndicator()

            var modelID = String()
            
            
            if NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED) == "" || NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED) == nil
            {
                modelID = ""
            }
            else
            {
                modelID = NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED)!
            }
            
            
            let method = "recipes/categories?model_id=\(modelID)&mwo_top_category_id=\(mwotopCatId)"

            print(method)
            
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                if response[Constants.STATE] as! Int == 1
                {
                    NSUserDefaults.standardUserDefaults().setObject(response["data"], forKey:Constants.RECIPECAT)
                    self.recipeCatArray = response["data"] as! Array<AnyObject>
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        self.recipeCollection.hidden = false
                        self.recipeCollection.reloadData()
                    }
                    Constants.appDelegate.stopIndicator()
                }
                else
                {
                    Constants.appDelegate.stopIndicator()
                    dispatch_async(dispatch_get_main_queue()) {
                        self.topCategoryView.hidden = true
                        self.modelSelectionView.hidden = true
                        self.modelTypeView.hidden = true
                        self.recipeCollection.hidden = true
                    }
                    CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"",
                        "message":response["message"]!])
                }

            })
        }
        else
        {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
            self.recipeCollection.hidden = true
        }
    }
    
    //MARK:- VIEW SETUP
    func setUPView()
    {
        if NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED) == "" || NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED) == nil
        {
            dispatch_async(dispatch_get_main_queue()){
                self.topCategoryView.hidden = false
                self.modelSelectionView.hidden = true
                self.modelTypeView.hidden = true
                self.view.bringSubviewToFront(self.topCategoryView)
                self.topCategoryTable.reloadData()
            }
        }
        else
        {
            let topCatID = NSUserDefaults.standardUserDefaults().stringForKey(Constants.TOPModelSELCTED)
            mwotopCatId = Int.init(topCatID!)!
            self.getRecipeData()
        }
    }
    
    //MARK:- COLLECTIONVIEW
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        return CGSizeMake(collectionView.bounds.size.width, collectionView.bounds.size.height)
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return recipeCatArray.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("RecipeCategoryCell", forIndexPath: indexPath)
        
        let catImage = cell.contentView.viewWithTag(1071) as! UIImageView
        catImage.image = nil
        
        
        
        
        catImage.contentMode = UIViewContentMode.ScaleAspectFill
        let prevButt = cell.contentView.viewWithTag(1072) as! UIButton
        let nextButt = cell.contentView.viewWithTag(1073) as! UIButton

        if recipeCatArray.count > 0 {
            let dict = recipeCatArray[indexPath.item]
            
            let img =  dict["image"] as! String
            
            myActivityIndicator.center = cell.contentView.center//catImage.center
            myActivityIndicator.hidesWhenStopped = true
            myActivityIndicator.activityIndicatorViewStyle = .Gray

            catImage.addSubview(myActivityIndicator)
            myActivityIndicator.startAnimating()

            Helper.loadImageFromUrlWithIndicator(img, view: catImage, indic: myActivityIndicator)
            
            prevButt.addTarget(self, action: #selector(PreviousButtonPresed), forControlEvents: UIControlEvents.TouchUpInside)
            nextButt.addTarget(self, action: #selector(NextButtonPresed), forControlEvents: UIControlEvents.TouchUpInside)
        }
        
        if recipeCatArray.count == 1 {
            prevButt.hidden = true
            nextButt.hidden = true
        }
        else{
            prevButt.hidden = false
            nextButt.hidden = false
        }

        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        dispatch_async(dispatch_get_main_queue(), {
            let destination = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("CategoriesRecipeVC") as! CategoriesRecipeVC
            let dict = self.recipeCatArray[indexPath.item]
            destination.catID = dict["id"] as! String
            destination.MWO_topCAtID = self.mwotopCatId
            destination.CategoryName = dict["title"] as! String
            print(dict["title"] as! String)
            self.navigationController?.pushViewController(destination, animated:true)
        })
    }
    
    
    //MARK:- PREVIOUS BUTTON
    
    @IBAction func PreviousButtonPresed(sender: AnyObject)
    {
        let cell = (sender.superview)!!.superview as! UICollectionViewCell
        let indexp = recipeCollection.indexPathForCell(cell)
        
        if indexp!.item == 0 {
            // do Nothing
        }
        else
        {
            recipeCollection.scrollToItemAtIndexPath(NSIndexPath.init(forItem: (indexp!.item) - 1, inSection: 0), atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: true)
        }
    }
    
    //MARK:- NEXT BUTTON
    
    @IBAction func NextButtonPresed(sender: AnyObject)
    {
        let cell = (sender.superview)!!.superview as! UICollectionViewCell
        let indexp = recipeCollection.indexPathForCell(cell)
        
        if indexp!.item == recipeCatArray.count-1 {
            // do Nothing
        }
        else
        {
            recipeCollection.scrollToItemAtIndexPath(NSIndexPath.init(forItem: (indexp!.item) + 1, inSection: 0), atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: true)
        }
    }
    
    
    //MARK:- View All Recipe / Select model
    @IBAction func ViewAllRecipePressed(sender: AnyObject) {
        
        if selectedTopCategory == 0 {
            mwotopCatId = 1
        }
        else if selectedTopCategory == 1{
            mwotopCatId = 2
        }
        else if selectedTopCategory == 2{
            mwotopCatId = 3
        }
        
        self.getRecipeData()
        topCategoryView.hidden = true
        modelSelectionView.hidden = true
        modelTypeView.hidden = true
    }
 
    @IBAction func SelectModelPressed(sender: AnyObject) {
        
        modelSelectionView.hidden = true
        modelTypeView.hidden = false
        topCategoryView.hidden = true

        if modelArray.count > 0 {
            modelTable.reloadData()
        }
        else
        {
            topCategoryView.hidden = true
            modelSelectionView.hidden = true
            modelTypeView.hidden = true
        }
    }
    
    //MARK:- View All Recipe / Select model

    @IBAction func CancelPressed(sender: AnyObject) {
        dispatch_async(dispatch_get_main_queue()) {
            self.topCategoryView.hidden = true
            self.modelSelectionView.hidden = false
            self.modelTypeView.hidden = true
            self.view.bringSubviewToFront(self.modelSelectionView)
        }

    }
    
    @IBAction func OkPressed(sender: AnyObject) {
        
        if selectedSections.count == 1
        {
            var selectedIndex = Int ()
            for index in selectedSections {
                selectedIndex = index
            }
            
            let dict = modelArray[selectedIndex] as! Dictionary<String,AnyObject>
            NSUserDefaults.standardUserDefaults().setObject(dict["id"], forKey: Constants.ModelSELCTED)
            NSUserDefaults.standardUserDefaults().setObject("\(selectedTopCategory + 1)", forKey: Constants.TOPModelSELCTED)

            let mo = sideMenuController()?.sideMenu?.menuViewController as! MenuTableView
            mo.UPDATEHeaderView(2)
            
            modelSelectionView.hidden = true
            modelTypeView.hidden = true
            topCategoryView.hidden = true

            if selectedTopCategory == 0 {
                mwotopCatId = 1
            }
            else if selectedTopCategory == 1{
                mwotopCatId = 2
            }
            else if selectedTopCategory == 2{
                mwotopCatId = 3
            }
            self.getRecipeData()
            
        }
        else
        {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title": "Culineria",
                "message": "Please select any one model !"])

        }
    }
    
    
    //MARK:- TABLEVIEW
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == topCategoryTable{
            return topModelArray.count
        }
        
        return modelArray.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if tableView == topCategoryTable{
            topCategoryTable.backgroundColor = UIColor.init(red: 70.0/255.0, green: 70.0/255.0, blue: 70.0/255.0, alpha: 1.0)
            let cell = tableView.dequeueReusableCellWithIdentifier("TopCategoryCell", forIndexPath: indexPath)
            cell.contentView.backgroundColor = UIColor.init(red: 70.0/255.0, green: 70.0/255.0, blue: 70.0/255.0, alpha: 1.0)
            let title = cell.contentView.viewWithTag(9901) as! UILabel
            let img2 = cell.contentView.viewWithTag(9902) as! UIImageView
            
            img2.image = UIImage.init(named: "radio_whiteOFF")
            let STR = topModelArray[indexPath.row]["name"] as? String
            title.text = STR!.uppercaseString
            return cell        }
        else{
            modelTable.backgroundColor = UIColor.whiteColor()
            
            let cell = tableView.dequeueReusableCellWithIdentifier("microwaveSelectionCell", forIndexPath: indexPath)
            cell.contentView.backgroundColor = UIColor.whiteColor()
            
            let dict = modelArray[indexPath.row] as! Dictionary<String,AnyObject>
            
            let title = cell.contentView.viewWithTag(1075) as! UILabel
            title.text = dict["name"] as? String
            
            let img2 = cell.contentView.viewWithTag(1076) as! UIImageView
            
            let current : Bool = selectedSections.containsIndex(indexPath.row)
            if current == true {
                img2.image = UIImage.init(named: "radioSelected")
            }
            else {
                img2.image = UIImage.init(named: "radioUnselected")
            }
            
            return cell
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
        if tableView == topCategoryTable{

            let cell = tableView.cellForRowAtIndexPath(indexPath)
            let img2 = cell!.contentView.viewWithTag(9902) as! UIImageView
            img2.image = UIImage.init(named: "radio_whiteON")
            selectedTopCategory = indexPath.row
            
            self.GetARRAYforTopCatSelected(selectedTopCategory + 1)
        }
        else
        {
            if  selectedSections.count > 0 {
                selectedSections.removeAllIndexes()
            }
            
            selectedSections.addIndex(indexPath.row)
            dispatch_async(dispatch_get_main_queue()) {
                self.modelTable.reloadData()
            }
        }
    }
    
    
    //MARK:- CHECK FOR ACTION AFTER TOP LEVEL SELECTION
    func GetARRAYforTopCatSelected(topCat: Int){
        
        if Reachability.isConnectedToNetwork() {
            SVProgressHUD.show()
            let method = "microwaves?top_category_id=\(topCat)"
            
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                SVProgressHUD.dismiss()
                
                if response[Constants.STATE] as! Int == 1 {
                    NSUserDefaults.standardUserDefaults().setObject(response["data"], forKey:Constants.RANGE)
                    var arr = [AnyObject]()
                    
                    for item in response["data"] as! Array<AnyObject>
                    {
                        let dict = item as! Dictionary<String, AnyObject>
                        let newDIct = NSMutableDictionary()
                        
                        let idStr = dict["id"] as! String
                        let nameStr = dict["title"] as! String
                        let imageStr = dict["image"] as! String
                        let volStr = dict["capacity"] as! String
                        let topCatID = dict["top_category_id"] as! String
                        
                        newDIct.setObject(idStr, forKey: "id")
                        newDIct.setObject(nameStr, forKey: "name")
                        newDIct.setObject(imageStr, forKey: "image")
                        newDIct.setObject(volStr, forKey: "capacity")
                        newDIct.setObject(topCatID, forKey: "top_category_id")
                        
                        arr.append(newDIct)
                    }
                    
                    NSUserDefaults.standardUserDefaults().setObject(arr, forKey: Constants.MODEL)
                    
                    self.getArraysForAllCategories(topCat)
                    
                }
                else{
                    
                }
            })
        }
        else {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    //MARK:- Arrays for Categories
    func getArraysForAllCategories(topCat: Int) {
        
        modelArray = NSUserDefaults.standardUserDefaults().objectForKey(Constants.MODEL) as! Array

        dispatch_async(dispatch_get_main_queue()) {
            self.topCategoryView.hidden = true
            self.modelSelectionView.hidden = false
            self.modelTypeView.hidden = true
            self.view.bringSubviewToFront(self.modelSelectionView)
        }
    }
}
