//
//  MyRecipeVC.swift
//  MyRecipeApp
//
//  Created by Surbhi on 17/11/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class MyRecipeVC: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, ENSideMenuDelegate
{
    //MARK:- IBOutlets
    @IBOutlet weak var myRecipeCollection: UICollectionView!
    @IBOutlet weak var noCount: UILabel!

    //MARK:- Global Variables
    var myRecipeArray = [AnyObject]()
    let myActivityIndicator = UIActivityIndicatorView()
    
    //MARK:- Start
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getMyRecipes()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Side Menu
    
    @IBAction func toggleSideMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        backItem.tintColor = UIColor.lightGrayColor()
        navigationItem.backBarButtonItem = backItem
    }
    
    //MARK:- LOAD DATA
    func getMyRecipes()
    {
        if Reachability.isConnectedToNetwork()
        {
            Constants.appDelegate.startIndicator()
            
            let method = "users/my_recipes"
            let paramStr = "user_id=\(NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID)!)&favourite=0"
            
            Server.postRequestWithURL(Constants.BASEURL+method, paramString: paramStr, completionHandler: { (response) in
                
                if response[Constants.STATE] as! Int == 1
                {
                    self.myRecipeArray = response["data"] as! Array<AnyObject>
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        
                        self.myRecipeCollection.hidden = false
                        self.noCount.hidden = true
                        self.myRecipeCollection.reloadData()
                    }
                    Constants.appDelegate.stopIndicator()
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.myRecipeCollection.hidden = true
                        self.noCount.hidden = false
                    }
                    Constants.appDelegate.stopIndicator()
                    CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"",
                        "message":response["message"]!])
                }
            })
        }
        else
        {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
            dispatch_async(dispatch_get_main_queue()) {
                self.myRecipeCollection.hidden = true
                self.noCount.hidden = false
            }
        }
    }
    
    
    //MARK:- COllection View
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        if DeviceType.IS_IPAD{
            return CGSizeMake((collectionView.bounds.size.width - 20)/2, (collectionView.bounds.size.width - 20)/2)
        }
        return CGSizeMake((collectionView.bounds.size.width - 25)/2, (collectionView.bounds.size.width - 25)/2)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        if DeviceType.IS_IPAD{
            return 10
        }
        return 5
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        if DeviceType.IS_IPAD{
            return 10
        }
        return 5
        
    }

    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return myRecipeArray.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("MyRecipeCell", forIndexPath: indexPath)
        
        let rec_Image = cell.contentView.viewWithTag(1101) as! UIImageView
        rec_Image.image = nil
        rec_Image.contentMode = UIViewContentMode.ScaleAspectFill

        let rating = cell.contentView.viewWithTag(1102) as! TPFloatRatingView // "rating"
        let nameLabel = cell.contentView.viewWithTag(1103) as! UILabel // "name"
        let deleteButton = cell.contentView.viewWithTag(1104) as! UIButton // "RemoveRecipe"
        deleteButton.addTarget(self, action: #selector(RemoveRecipe), forControlEvents: UIControlEvents.TouchUpInside)

        if myRecipeArray.count > 0 {
            let dict = myRecipeArray[indexPath.item]
            
            // MEAL IMAGE
            let imgStr =  dict["image_1"] as! String
            
            myActivityIndicator.center = cell.contentView.center
            myActivityIndicator.hidesWhenStopped = true
            myActivityIndicator.activityIndicatorViewStyle = .Gray

            rec_Image.addSubview(myActivityIndicator)
            myActivityIndicator.startAnimating()
            Helper.loadImageFromUrlWithIndicator(imgStr, view: rec_Image, indic: myActivityIndicator)

            
            // MEAL NAME
            nameLabel.text = dict["name"] as? String
            
            // MEAL RATING
            rating.backgroundColor = UIColor.clearColor()
            rating.emptySelectedImage = UIImage.init(named: "starEmpty")
            rating.fullSelectedImage = UIImage.init(named: "starFull")
            rating.contentMode = UIViewContentMode.ScaleAspectFill
            rating.editable = false
            rating.halfRatings = true;
            rating.floatRatings = true;
            rating.maxRating = 5
            rating.minRating = 0
            
            if dict["rating"] != nil
            {
                let rate  = Float((dict["rating"] as? NSNumber)!)
                rating.rating = CGFloat.init(rate)
            }
            else
            {
                rating.rating = 0.0
            }
        }
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        let destViewController  = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("RecipeDetail") as! RecipeDetailView
        let dict = myRecipeArray[indexPath.item]
        
        destViewController.recipeIDSTR = dict["id"] as! String
        destViewController.myRecipeStr = "1"
        destViewController.iscomingFromRecipe = false
        
        if self.navigationController == nil {
            self.presentViewController(destViewController, animated: true, completion: nil)
        }
        else
        {
            self.navigationController?.pushViewController(destViewController, animated: true)
        }
    }
    
    //MARK:- REMOVE MODEL & CHANGE MODEL
    
    @IBAction func RemoveRecipe(sender: AnyObject)
    {
        let cell = ((sender.superview)!!.superview)!.superview as! UICollectionViewCell
        let indexp = myRecipeCollection.indexPathForCell(cell)
        
        let dict = myRecipeArray[indexp!.item]
        let recipeIDSTR = dict["id"] as! String
        
        
        let alert = UIAlertController.init(title: "Culineria", message: "Are you sure you want to delete your recipe?", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction.init(title: "YES", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
            self.DeleteMethod(recipeIDSTR)
        }))
        alert.addAction(UIAlertAction.init(title: "NO", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
            self.dismissViewControllerAnimated(true, completion: nil)
        }))
        self.presentViewController(alert, animated: true, completion: nil)

        
    }
    
    func DeleteMethod(idstr :String)
    {
        if Reachability.isConnectedToNetwork()
        {
            Constants.appDelegate.startIndicator()
            
            let method = "recipes/remove"
            let paramStr = "user_id=\(NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID)!)&recipe_id=\(idstr)"
            
            Server.postRequestWithURL(Constants.BASEURL+method, paramString: paramStr, completionHandler: { (response) in
                
                Constants.appDelegate.stopIndicator()

                if response[Constants.STATE] as! Int == 1
                {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.getMyRecipes()
                    }
                }
                else
                {
                    CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"",
                        "message":response["message"]!])
                }
            })
        }
        else
        {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }

    }
    
}
