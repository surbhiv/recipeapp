//
//  CategoriesRecipeVC.swift
//  MyRecipeApp
//
//  Created by Surbhi on 15/11/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class CategoriesRecipeVC: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, ENSideMenuDelegate, UITableViewDelegate, UITableViewDataSource {

    //MARK:- IBOutlets
    @IBOutlet weak var recipeCollection: UICollectionView!
    @IBOutlet weak var recipeCategoryLabel: UILabel!
    @IBOutlet weak var collectionHeader: UIView!
    @IBOutlet weak var hederImage: UIImageView!
    @IBOutlet weak var headerName: UILabel!
    @IBOutlet weak var headerVol: UILabel!
    @IBOutlet weak var collectionHeaderHeight: NSLayoutConstraint!
    
    @IBOutlet weak var levelbutton: UIButton!
    @IBOutlet weak var cuisineButton: UIButton!
    
    @IBOutlet weak var leveltable: UITableView!//recipeLevelCell // 100
    @IBOutlet weak var leveltableHeight: NSLayoutConstraint!
    
    @IBOutlet weak var cuisineTable: UITableView!//cuisineCell // 101
    @IBOutlet weak var cuisinetableHeight: NSLayoutConstraint!
    
    
    //MARK:- Global Variables
    var recipesArray = [AnyObject]()
    var catID = String()
    var CategoryName = String()
    var leverArray = [AnyObject]()
    var cuisineArray = [AnyObject]()
    var leveSelected : String = "0"
    var cuisineSelected : String = "0"
    let maxViewBeforeLogin = 9
    
    var MWO_topCAtID = Int()
    
    //MARK:- Start
    override func viewDidLoad() {
        super.viewDidLoad()
        recipeCategoryLabel.text = CategoryName.capitalizedString
        
        updateSelfView()
        getRecipeData()
        
        let spacing = levelbutton.frame.size.width/2
        levelbutton.imageEdgeInsets = UIEdgeInsetsMake(0, spacing, 0, 0);
        cuisineButton.imageEdgeInsets = UIEdgeInsetsMake(0, spacing, 0, 0);

        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    func updateSelfView()
    {
        if NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED) == "" || NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED) == nil
        {
            dispatch_async(dispatch_get_main_queue()) {
                self.collectionHeader.hidden = true
                self.collectionHeaderHeight.constant = 0.0
            }
        }
        else
        {
            let modelArray = NSUserDefaults.standardUserDefaults().objectForKey(Constants.MODEL) as! Array<AnyObject>
            var Dict = Dictionary<String,AnyObject>()
            for item in modelArray {
                let dc = item as! Dictionary<String,AnyObject>
                let idSTRing = dc["id"] as! String
                let topID = dc["top_category_id"] as! String

                if idSTRing ==  NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED) && topID == NSUserDefaults.standardUserDefaults().stringForKey(Constants.TOPModelSELCTED){
                    Dict = dc
                }
            }
            
            dispatch_async(dispatch_get_main_queue()) {
                self.headerName.text = Dict["name"] as? String
                self.collectionHeader.hidden = false
                
                //Adding SHadow
                self.collectionHeader.layer.shadowColor = Colors.ShadowColor.CGColor
                self.collectionHeader.layer.shadowOpacity = 1.0
                self.collectionHeader.layer.shadowOffset = CGSizeMake(0, 2)
                self.collectionHeader.layer.shadowRadius = 1.5
                self.collectionHeader.layer.masksToBounds = true
                self.collectionHeader.layer.borderWidth = 0.15
                self.collectionHeader.layer.borderColor = Colors.appThemeColor.CGColor
                

                let myActivityIndicator = UIActivityIndicatorView()
                myActivityIndicator.center = self.collectionHeader.center
                myActivityIndicator.hidesWhenStopped = true
                myActivityIndicator.activityIndicatorViewStyle = .Gray
                self.hederImage.addSubview(myActivityIndicator)
                myActivityIndicator.startAnimating()

                if DeviceType.IS_IPAD{
                    self.collectionHeaderHeight.constant = 200.0
                }
                else{
                    self.collectionHeaderHeight.constant = 95.0
                }

                let imgSTRing = Dict["image"] as! String
                Helper.loadImageFromUrlWithIndicator(imgSTRing, view: self.hederImage, indic: myActivityIndicator)
            }
        }
        
        leverArray = NSUserDefaults.standardUserDefaults().objectForKey(Constants.LEVEL) as! Array<AnyObject>
        cuisineArray = NSUserDefaults.standardUserDefaults().objectForKey(Constants.CUISINE) as! Array<AnyObject>
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Side Menu 
    
    @IBAction func toggleSideMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        backItem.tintColor = UIColor.lightGrayColor()
        navigationItem.backBarButtonItem = backItem
    }
    
    @IBAction func BackButtonPressed(sender: AnyObject) {
        if self.navigationController == nil {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        else {
            self.navigationController?.popViewControllerAnimated(true)
        }
    }

    //MARK:- LOAD DATA
    func getRecipeData()
    {
        var modelID = String()
        if NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED) == "" || NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED) == nil
        {
            modelID = ""
        }
        else
        {
            modelID = NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED)!
        }
        
        
        
        if Reachability.isConnectedToNetwork()
        {
            Constants.appDelegate.startIndicator()
            
            let method = "recipes"
            let paramStr = "category_id=\(catID)&model_id=\(modelID)&id=&user_id=&type=&meal_type_id=&level_id=\(leveSelected)&cuisine_id=\(cuisineSelected)&mwo_top_category_id=\(MWO_topCAtID)"
            
            Server.postRequestWithURL(Constants.BASEURL+method, paramString: paramStr, completionHandler: { (response) in

                if response[Constants.STATE] as! Int == 1
                {
                    self.recipesArray = response["data"] as! Array<AnyObject>
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        self.recipeCollection.hidden = false
                        self.recipeCollection.reloadData()
                        self.cuisineTable.hidden = true
                        self.cuisinetableHeight.constant = 0
                        self.leveltable.hidden = true
                        self.leveltableHeight.constant = 0
                    }
                    Constants.appDelegate.stopIndicator()

                }
                else
                {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.cuisineTable.hidden = true
                        self.cuisinetableHeight.constant = 0
                        self.leveltable.hidden = true
                        self.leveltableHeight.constant = 0
                        self.recipeCollection.hidden = true

                    }

                    Constants.appDelegate.stopIndicator()

                    CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"",
                        "message":response["message"]!])
                }
                
            })
        }
        else
        {
            dispatch_async(dispatch_get_main_queue()) {
                self.cuisineTable.hidden = true
                self.cuisinetableHeight.constant = 0
                self.leveltable.hidden = true
                self.leveltableHeight.constant = 0
                self.recipeCollection.hidden = true
            }

            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }

    
    //MARK:- COllection View

    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        if DeviceType.IS_IPAD{
            return CGSizeMake((collectionView.bounds.size.width - 20)/2, (collectionView.bounds.size.width - 20)/2)
        }
        return CGSizeMake((collectionView.bounds.size.width - 25)/2, (collectionView.bounds.size.width - 25)/2)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        if DeviceType.IS_IPAD{
            return 10
        }
        return 5
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        if DeviceType.IS_IPAD{
            return 10
        }
        return 5
        
    }

    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return recipesArray.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cat_RecipeCell", forIndexPath: indexPath)
        
        let vegTypeImage = cell.contentView.viewWithTag(1082) as! UIImageView
        let rating = cell.contentView.viewWithTag(1083) as! TPFloatRatingView // "rating"
        let nameLabel = cell.contentView.viewWithTag(1084) as! UILabel // "name"

        let rec_Image = cell.contentView.viewWithTag(1081) as! UIImageView
        rec_Image.image = nil
        rec_Image.contentMode = UIViewContentMode.ScaleAspectFill
        
        let myActivityIndicator = UIActivityIndicatorView()
        myActivityIndicator.center = cell.contentView.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.activityIndicatorViewStyle = .Gray
        rec_Image.addSubview(myActivityIndicator)
        myActivityIndicator.startAnimating()

        if recipesArray.count > 0 {
            let dict = recipesArray[indexPath.item]
            
            // MEAL IMAGE
            let imgArry = dict["images"] as! Array<AnyObject>
            if imgArry.count>0
            {
                let img =  imgArry.first as! Dictionary<String,AnyObject>
                let imgStr =  img["image"] as! String
                Helper.loadImageFromUrlWithIndicator(imgStr, view: rec_Image, indic: myActivityIndicator)
            }
            
            // MEAL NAME
            nameLabel.text = dict["name"] as? String
            
            
            // MEAL TYPE
            let mealtype =  dict["type"] as? String
            if mealtype!.lowercaseString == "veg"
            {
                vegTypeImage.highlighted = false
                vegTypeImage.image = UIImage.init(named: "veg.png")
            }
            else if mealtype!.lowercaseString == "non-veg"
            {
                vegTypeImage.highlighted = false
                vegTypeImage.image = UIImage.init(named: "veg_non.png")
            }
            else
            {
                vegTypeImage.highlighted = true
            }
            
            // MEAL RATING
            rating.backgroundColor = UIColor.clearColor()
            rating.emptySelectedImage = UIImage.init(named: "starEmpty")
            rating.fullSelectedImage = UIImage.init(named: "starFull")
            rating.contentMode = UIViewContentMode.ScaleAspectFill
            rating.editable = false
            rating.halfRatings = true;
            rating.floatRatings = true;
            rating.maxRating = 5
            rating.minRating = 0
            
            if dict["rating"] != nil
            {
                let rate  = Float((dict["rating"] as? NSNumber)!)
                rating.rating = CGFloat.init(rate)
            }
            else
            {
                rating.rating = 0.0
            }
        }
        
        return cell
    }

    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        let destViewController  = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("RecipeDetail") as! RecipeDetailView
        let dict = recipesArray[indexPath.item] as! Dictionary<String,AnyObject>
        let recipeSTR = dict["id"] as! String

        destViewController.recipeIDSTR = recipeSTR
        destViewController.iscomingFromRecipe = true

        let count = Helper.getOpenCount()
        
            if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == ""
            {
                if count <= maxViewBeforeLogin {
                    
                    Helper.incrementOpenCount()
                    
                    if self.navigationController == nil {
                        self.presentViewController(destViewController, animated: true, completion: nil)
                    }
                    else
                    {
                        self.navigationController?.pushViewController(destViewController, animated: true)
                    }
                    
                }
                else
                {
                
                let alert = UIAlertController.init(title: "Culineria", message: "Login / Register to view more recipes.", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction.init(title: "LOGIN", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                    
                    let destinationController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("LoginView") as! LoginView
                    Constants.appDelegate.isRecipeDetail = true
                    self.navigationController?.presentViewController(destinationController, animated: true, completion: nil)
                }))
                alert.addAction(UIAlertAction.init(title: "CANCEL", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                    self.dismissViewControllerAnimated(true, completion: nil)
                }))
                
                self.presentViewController(alert, animated: true, completion: nil)
                }
            }
            else
            {
                if self.navigationController == nil {
                    self.presentViewController(destViewController, animated: true, completion: nil)
                }
                else
                {
                    self.navigationController?.pushViewController(destViewController, animated: true)
                }
                Helper.resetCount()
            }

        
    }
    
    //MARK:- REMOVE MODEL & CHANGE MODEL
    
    @IBAction func RemoveModel(sender: AnyObject) {
        
        let mo = sideMenuController()?.sideMenu?.menuViewController as! MenuTableView
        mo.DeleTEModelSelection()
        getRecipeData()
        updateSelfView()
    }
    
    @IBAction func ChangeModelPressed(sender: AnyObject) {
        
        let mo = sideMenuController()?.sideMenu?.menuViewController as! MenuTableView
        mo.tableView(mo.tableView, didSelectRowAtIndexPath: NSIndexPath.init(forRow: 1, inSection: 0))
        mo.tableView.selectRowAtIndexPath(NSIndexPath(forRow: 1, inSection: 0), animated: false, scrollPosition: .Middle)
        mo.toggleSideMenuView()
    }

    
    //MARK:- Recipe LEVEL & CUISINE
    
    
    @IBAction func RecipeLevelPressed(sender: AnyObject) {
        
        if leveltable.hidden == true {
            
            if cuisineTable.hidden == false {
                cuisineTable.hidden = true
                cuisinetableHeight.constant = 0
            }
            
            UIView.animateWithDuration(5.0, delay: 0.0, usingSpringWithDamping: 6.0, initialSpringVelocity: 2.6, options: UIViewAnimationOptions.TransitionCurlDown, animations: {
                self.view.bringSubviewToFront(self.leveltable)
                self.leveltable.hidden = false
                if DeviceType.IS_IPAD{
                    self.leveltableHeight.constant = CGFloat(self.leverArray.count + 1) * 40.0
                }
                else{
                    self.leveltableHeight.constant = CGFloat(self.leverArray.count + 1) * 30.0
                }
                
                self.leveltable.reloadData()

                }, completion: nil)
        }
        else
        {
            UIView.animateWithDuration(2.0, delay: 0.0, usingSpringWithDamping: 2.6, initialSpringVelocity: 6.0, options: UIViewAnimationOptions.TransitionCurlUp, animations: {
                self.leveltable.hidden = true
                self.leveltableHeight.constant = 0
                }, completion: nil)
        }
    }
    
    @IBAction func RecipeCuisinePressed(sender: AnyObject) {
        if cuisineTable.hidden == true {
            
            if leveltable.hidden == false {
                leveltable.hidden = true
                leveltableHeight.constant = 0
            }
            
            UIView.animateWithDuration(5.0, delay: 0.0, usingSpringWithDamping: 6.0, initialSpringVelocity: 2.6, options: UIViewAnimationOptions.TransitionCurlDown, animations: {
                self.view.bringSubviewToFront(self.cuisineTable)
                self.cuisineTable.hidden = false
                
                if DeviceType.IS_IPAD{
                    self.cuisinetableHeight.constant = CGFloat(self.cuisineArray.count + 1) * 40.0
                }
                else{
                    self.cuisinetableHeight.constant = CGFloat(self.cuisineArray.count + 1) * 30.0
                }

                self.cuisineTable.reloadData()

                }, completion: nil)
        }
        else
        {
            UIView.animateWithDuration(2.0, delay: 0.0, usingSpringWithDamping: 2.6, initialSpringVelocity: 6.0, options: UIViewAnimationOptions.TransitionCurlUp, animations: {
                self.cuisineTable.hidden = true
                self.cuisinetableHeight.constant = 0
                }, completion: nil)
        }
    }
    
    
    //MARK:- TABLEVIEW
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == leveltable
        {
            return leverArray.count + 1
        }
        else
        {
            return cuisineArray.count + 1
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        self.leveltable.backgroundColor = Colors.appThemeColor
        self.cuisineTable.backgroundColor = Colors.appThemeColor
        
        var cell = UITableViewCell()
        if tableView == leveltable
        {
            cell = tableView.dequeueReusableCellWithIdentifier("recipeLevelCell", forIndexPath: indexPath)
            cell.backgroundColor = Colors.appThemeColor
            let lab = cell.contentView.viewWithTag(100) as! UILabel
            
            
            if indexPath.row == 0 {
                lab.text = "All Levels"
            }
            else
            {
                let dit = leverArray[indexPath.row - 1] as! Dictionary<String,AnyObject>
                let tit = dit["title"] as! String
                lab.text = tit
            }
        }
        else
        {
            cell = tableView.dequeueReusableCellWithIdentifier("cuisineCell", forIndexPath: indexPath)
            cell.backgroundColor = Colors.appThemeColor
            let lab = cell.contentView.viewWithTag(101) as! UILabel
            if indexPath.row == 0 {
                lab.text = "All Cuisines"
            }
            else
            {
                let dit = cuisineArray[indexPath.row - 1] as! Dictionary<String,AnyObject>
                let tit = dit["title"] as! String
                lab.text = tit
            }
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if tableView == leveltable
        {
            if indexPath.row == 0
            {
                leveSelected = "0"
                levelbutton.setTitle(" All Recipes", forState: .Normal)

            }
            else
            {
                let dit = leverArray[indexPath.row - 1] as! Dictionary<String,AnyObject>
                let ids = dit["id"] as! String
                leveSelected = ids
                let strTitle = " \(dit["title"] as! String)"
                levelbutton.setTitle(strTitle + " ", forState: .Normal)
            }
            getRecipeData()
        }
        else
        {
            if indexPath.row == 0
            {
                cuisineSelected = "0"
                cuisineButton.setTitle(" All Recipes", forState: .Normal)
            }
            else
            {
                let dit = cuisineArray[indexPath.row - 1] as! Dictionary<String,AnyObject>
                let ids = dit["id"] as! String
                cuisineSelected = ids
                let strTitle = " \(dit["title"] as! String)"

                cuisineButton.setTitle(strTitle + " ", forState: .Normal)
            }
            getRecipeData()
        }
    }
    
}
