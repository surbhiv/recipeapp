//
//  RateRecipeView.swift
//  MyRecipeApp
//
//  Created by Surbhi on 04/11/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class RateRecipeView: UIViewController, TPFloatRatingViewDelegate {

    @IBOutlet weak var rateView: TPFloatRatingView!
    @IBOutlet weak var rateButton: UIButton!
    @IBOutlet weak var CancelButton: UIButton!
    @IBOutlet weak var ratingView: UIView!
    
    var recipeID  = String()
    var myRecipe  = Bool()
    var isRecipeofDay  = Bool()
    
    // MARK:- START
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ratingView.layer.shadowColor = Colors.ShadowColor.CGColor
        ratingView.layer.cornerRadius = 5.0
        ratingView.clipsToBounds = true
        
        rateView.delegate = self
        rateView.backgroundColor = UIColor.clearColor()
        rateView.emptySelectedImage = UIImage.init(named: "ratingEmpty")
        rateView.fullSelectedImage = UIImage.init(named: "ratingFull")
        rateView.contentMode = UIViewContentMode.ScaleAspectFill
        rateView.editable = true
        rateView.halfRatings = false;
        rateView.floatRatings = false;
        rateView.maxRating = 5
        rateView.minRating = 1
        rateView.rating = 0
    }
    
    // MARK:- ACTIONS
    
    @IBAction func cancelPressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func RateButtonPressed(sender: AnyObject) {
        
        let method = "recipes/rating"
        var param = String()
        if (rateView.rating >= 1){
            param = "user_id=\(NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID)!)&recipe_id=\(recipeID)&rating=\(String(Int(rateView.rating)))"
            
        }
        else {
            param = "user_id=\(NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID)!)&recipe_id=\(recipeID)&rating="

        }
        
        if Reachability.isConnectedToNetwork() {
            
            Constants.appDelegate.startIndicator()
            Server.postRequestWithURL(Constants.BASEURL+method, paramString: param) { (response) in
                
                Constants.appDelegate.stopIndicator()
                if response[Constants.STATE] as! Int == 1
                {
                    dispatch_async(dispatch_get_main_queue()){
                        NSNotificationCenter.defaultCenter().postNotification(NSNotification.init(name: "RecipeRatingUpdated", object: nil))
                        
                        let alert = UIAlertController.init(title: "Culineria", message: response["message"]! as? String, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                            
                            self.dismissView()
                        }))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue()){
                        
                        let alert = UIAlertController.init(title: "Culineria", message: response["message"]! as? String, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                            
                            self.dismissView()
                        }))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        else {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    func dismissView()
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func FadeViewPressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    // MARK:- TPFloatRatingViewDelegate
    
    func floatRatingView(ratingView: TPFloatRatingView!, ratingDidChange rating: CGFloat) {
    }
    
    func floatRatingView(ratingView: TPFloatRatingView!, continuousRating rating: CGFloat) {
    }

}
