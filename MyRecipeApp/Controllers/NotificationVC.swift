//
//  NotificationVC.swift
//  MyRecipeApp
//
//  Created by Surbhi on 14/12/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class NotificationVC: UIViewController , UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var notiTable: UITableView!
    @IBOutlet weak var noCount: UILabel!
    
    var notificationArray = [AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.noCount.hidden = true
        self.getData()
        
        
        let oldcount = NSUserDefaults.standardUserDefaults().integerForKey( "NotificationCount")
        NSUserDefaults.standardUserDefaults().setInteger(oldcount, forKey: "notiCount")

        let mo = self.sideMenuController()?.sideMenu?.menuViewController as! MenuTableView
        mo.tableView.reloadData()

        
        
        self.automaticallyAdjustsScrollViewInsets = false
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func toggleSideMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    
    func getData() {
        if Reachability.isConnectedToNetwork() {
            SVProgressHUD.show()
            let url = "\(Constants.BASEURL)notifications"
            Server.getRequestWithURL(url, completionHandler: { (response) in
                if response[Constants.STATE] as! Int == 1 {

                    SVProgressHUD.dismiss()
                    self.notificationArray = response["data"] as! Array<AnyObject>
                    
                    if self.notificationArray.count == 0 {
                        self.notiTable.hidden = true
                        
                        dispatch_async(dispatch_get_main_queue())
                        {
                            self.noCount.hidden = false
                            self.view.bringSubviewToFront(self.noCount)
                        }
                    }
                    else
                    {
                        self.notiTable.hidden = false
                        self.noCount.hidden = true
                        self.notiTable.delegate = self
                        self.notiTable.dataSource = self
                        dispatch_async(dispatch_get_main_queue())
                        {
                            self.notiTable.reloadData()
                        }
                    }
                }
                else {
                    SVProgressHUD.dismiss()
                    self.notiTable.hidden = true
                    self.noCount.hidden = false
                    self.view.bringSubviewToFront(self.noCount)
                    CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"",
                        "message":response["message"]!])
                }
            })
        }
        else {
            
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
            self.notiTable.hidden = true
            self.noCount.hidden = false
            self.view.bringSubviewToFront(self.noCount)
        }
        
    }

    
    
    //MARK: - TABLEVIEW
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return notificationArray.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("NotificationCell")
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "NotificationCell")
        }
        
        cell?.layer.shadowColor = Colors.ShadowColor.CGColor
        cell?.layer.cornerRadius = 8.0
        cell?.clipsToBounds = true
        
        let heading = cell?.contentView.viewWithTag(1201) as! UILabel
        let timelabel = cell?.contentView.viewWithTag(1202) as! UILabel
        
        let dict = notificationArray[indexPath.section] as! Dictionary<String, AnyObject>
        
        heading.text = dict["message"] as? String
        heading.numberOfLines = 0
        timelabel.text = dict["created"] as? String
        
        
        notiTable.estimatedRowHeight = 50.0
        notiTable.rowHeight = UITableViewAutomaticDimension
        
        return cell!
    }


    

}
