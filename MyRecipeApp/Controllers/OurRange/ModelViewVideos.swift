//
//  ModelViewVideos.swift
//  MyRecipeApp
//
//  Created by Surbhi on 08/11/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class ModelViewVideos: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var videoTable: UITableView!
    @IBOutlet weak var noCount: UILabel!

    var videoArray = [AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if videoArray.count == 0 {
            videoTable.hidden = true
            noCount.hidden = false
        }
        else
        {
            videoTable.hidden = false
            noCount.hidden = true
            videoTable.delegate = self
            videoTable.dataSource = self
            videoTable.reloadData()
        }
        self.automaticallyAdjustsScrollViewInsets = false

    }

    
    //MARK: - TABLEVIEW
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return videoArray.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("VideoCell")
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "VideoCell")
        }
        
        cell?.layer.shadowColor = Colors.ShadowColor.CGColor
        cell?.clipsToBounds = true
        
        let heading = cell?.contentView.viewWithTag(1052) as! UILabel
        let img = cell?.contentView.viewWithTag(1051) as! UIImageView
        
        let dict = videoArray[indexPath.section] as! Dictionary<String, AnyObject>
        
        heading.text = dict["title"] as? String
        
        let imgStr = dict["video_thumb"] as? String
        
        let myActivityIndicator = UIActivityIndicatorView()
        myActivityIndicator.center = img.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.activityIndicatorViewStyle = .Gray
        img.addSubview(myActivityIndicator)
        myActivityIndicator.startAnimating()

        
        
        if imgStr != "" {
            Helper.loadImageFromUrlWithIndicator(imgStr!, view:  img, indic: myActivityIndicator)
        }
        
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let dict = videoArray[indexPath.section] as! Dictionary<String, AnyObject>
        let videoID = dict["video_id"] as! String
        
        let  destinationController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("YouTubeVideoPlayer") as! YouTubeVideoPlayer
        
        if videoID != ""
        {
            destinationController.videoID = dict["video_id"] as! String
            self.navigationController?.pushViewController(destinationController, animated: true)
        }
    }

}
