//
//  OurRangeMainView.swift
//  MyRecipeApp
//
//  Created by Surbhi on 08/11/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class OurRangeMainView: UIViewController, ENSideMenuDelegate, UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var headerScroll: UIScrollView!
    @IBOutlet weak var headerHeight: NSLayoutConstraint!
    @IBOutlet weak var detailScroll: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var headerLabel : UILabel!
    
    
    private var indicatorLabel : UILabel!
    private var ControllerArray = [AnyObject]()
    private var headerArray = []
    
    var rangeArray = [AnyObject]()
    var convectionArray = [AnyObject]()
    var grillArray = [AnyObject]()
    var soloArray = [AnyObject]()
    var builtInOvenArray = [AnyObject]()
    var builtInMicroArray = [AnyObject]()

    var convView = OurRangeSubView()
    var grillView = OurRangeSubView()
    var soloView = OurRangeSubView()

    
    
    @IBOutlet weak var topCategoryView: UIView!
    @IBOutlet weak var topCategoryTable: UITableView!
    var topModelArray = [AnyObject]()//["BUILT IN OVEN","BUILT IN MICROWAVE","COUNTERTOP MICROWAVE"]
    //[AnyObject]()
    
    var selectedTopCategory = Int()
    
    var viewLoaded : Bool = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.sideMenuController()?.sideMenu?.delegate = self
        self.navigationController?.navigationBarHidden = true
        topModelArray = NSUserDefaults.standardUserDefaults().objectForKey(Constants.TOPMODEL) as! Array<AnyObject>

        self.setUPView()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(setUPView), name: "ShowModelSelectionVw", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(updateModelSelectionTables), name: "ModelSelection", object: nil)
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func viewDidDisappear(animated: Bool) {
        viewLoaded = true
    }
    
    override func viewDidAppear(animated: Bool) {
        
        if viewLoaded == true {
            if selectedTopCategory == 0 {
                convView.modelTable.reloadData()
            }
            else if selectedTopCategory == 1{
                convView.modelTable.reloadData()
            }
            else {
                convView.modelTable.reloadData()
                grillView.modelTable.reloadData()
                soloView.modelTable.reloadData()
            }
        }
    }
    
    //MARK:- VIEW SETUP
    func setUPView()
    {
        dispatch_async(dispatch_get_main_queue()){
                self.topCategoryView.hidden = false
                self.view.bringSubviewToFront(self.topCategoryView)
                self.topCategoryTable.reloadData()
            }
    }
    
    //MARK:- SIDE MENU ACTIONS
    @IBAction func toggleSideMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    //MARK:- PREPARE SEGUE
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        backItem.tintColor = UIColor.lightGrayColor()
        navigationItem.backBarButtonItem = backItem
    }
    
    //MARK:- Arrays for Categories
    func getArraysForAllCategories(topCat: Int) {
        
        rangeArray = NSUserDefaults.standardUserDefaults().objectForKey(Constants.RANGE) as! Array
        if topCat == 1 {
            builtInOvenArray = rangeArray
        }
        else if topCat == 2 {
            builtInMicroArray = rangeArray
        }
        else if topCat == 3 {
            for item in  rangeArray {
                let dict = item as! Dictionary<String,AnyObject>
                let cateName = dict["category"] as! String
                
                if cateName == "Convection" {
                    convectionArray.append(item)
                }
                else if cateName == "Grill" {
                    grillArray.append(item)
                }
                else  {
                    soloArray.append(item)
                }
            }
        }
    }
    
    // MARK: - Label ScrollView
    func setHeaderScroll()  {
        var scrollContentCount = 0;
        var frame : CGRect = CGRectMake(0, 0, 0, 0)
        var labelx : CGFloat = 0.0
        let width  : CGFloat = ScreenSize.SCREEN_WIDTH / 3
        let firstLabelWidth : CGFloat = ScreenSize.SCREEN_WIDTH / 3
        
        if indicatorLabel == nil {
            indicatorLabel = UILabel.init(frame: CGRectMake(labelx, (DeviceType.IS_IPAD ? 47 : 38), firstLabelWidth, 3))
            indicatorLabel.backgroundColor = UIColor.whiteColor()
            indicatorLabel.text = "";
            headerScroll.addSubview(indicatorLabel)
        }
        indicatorLabel.frame = CGRectMake(labelx, (DeviceType.IS_IPAD ? 47 : 38), firstLabelWidth, 3)
        
        for arrayIndex in 0 ..< headerArray.count  {
            frame.origin.x = labelx
            // set scorllview properties
            frame.origin.y = 0;
            frame.size.width=width
            frame.size.height = (DeviceType.IS_IPAD ? 45 : 37)
            
            let accountLabelButton = UIButton.init(type: UIButtonType.Custom)
            accountLabelButton.addTarget(self, action:#selector(HeaderlabelSelected), forControlEvents: UIControlEvents.TouchUpInside)
            
            accountLabelButton.frame = frame;
            accountLabelButton.tag = arrayIndex;
            accountLabelButton.setAttributedTitle(NSAttributedString(string: headerArray[arrayIndex] as! String,  attributes: [NSFontAttributeName : UIFont.init(name: "Helvetica-Bold", size: 15.0)!, NSForegroundColorAttributeName : UIColor.whiteColor()]), forState: UIControlState.Normal)
            
            if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS {
                accountLabelButton.setAttributedTitle(NSAttributedString(string: headerArray[arrayIndex] as! String,  attributes: [NSForegroundColorAttributeName : Colors.Lighttextcolor,NSFontAttributeName : UIFont.init(name: "Helvetica-Bold", size: 14.0)!]), forState: UIControlState.Normal)
            }
            else if DeviceType.IS_IPAD{
                accountLabelButton.setAttributedTitle(NSAttributedString(string: headerArray[arrayIndex] as! String,  attributes: [NSForegroundColorAttributeName : Colors.Lighttextcolor,NSFontAttributeName : UIFont.init(name: "Helvetica-Bold", size: 18.0)!]), forState: UIControlState.Normal)
            }
            
            accountLabelButton.titleLabel?.textAlignment = NSTextAlignment.Center
            
            headerScroll.addSubview(accountLabelButton)
            
            if arrayIndex == 0 {
                accountLabelButton.alpha=1.0;
            }
            else {
                accountLabelButton.alpha=0.7;
            }
            
            labelx=labelx+width;
            scrollContentCount = scrollContentCount + 1;
        }
        
        // set scroll content size
        headerScroll.contentSize = CGSizeMake(frame.width, (DeviceType.IS_IPAD ? 50 : 40 ))//CGSizeMake(frame.origin.x+frame.size.width, 40);
        automaticallyAdjustsScrollViewInsets = false;
        detailScroll.delegate=self;
    }
    
    
    func HeaderlabelSelected(sender: UIButton)  {
        
        detailScroll.tag = sender.tag
        let xAxis : CGFloat = ScreenSize.SCREEN_WIDTH * CGFloat(sender.tag)
        detailScroll.setContentOffset(CGPointMake(xAxis, 0), animated: true)
        
        let buttonArray = NSMutableArray()
        
        for view in headerScroll.subviews {
            if view.isKindOfClass(UIButton) {
                let labelButton = view as! UIButton
                buttonArray.addObject(labelButton)
                
                if labelButton.tag == sender.tag {
                    labelButton.alpha = 1.0
                }
                else {
                    labelButton.alpha = 0.7
                }
            }
        }
        
        let labelButton = buttonArray.objectAtIndex(sender.tag)
        var frame : CGRect = labelButton.frame
        frame.origin.y = (DeviceType.IS_IPAD ? 47 : 38)
        frame.size.height = 3
        
        UIView.animateWithDuration(0.2, animations: {
            if sender.tag == 0 {
                self.indicatorLabel.frame =  CGRectMake(0,(DeviceType.IS_IPAD ? 47 : 38), ScreenSize.SCREEN_WIDTH/3.0, 3)
            }
            else {
                self.indicatorLabel.frame =  CGRectMake((ScreenSize.SCREEN_WIDTH/3.0) * CGFloat(sender.tag) ,(DeviceType.IS_IPAD ? 47 : 38), ScreenSize.SCREEN_WIDTH/3.0, 3)
            }
            
            self.headerScroll.scrollRectToVisible(self.indicatorLabel.frame, animated: false)
        }, completion: nil)
    }
    
    // MARK: -  Main Scrollview
    func setUpScrollView() {//
        
        for arrayIndex in headerArray {
            var detailStr = String()
            
            if selectedTopCategory == 0{
                let arrSTR = arrayIndex as? String
                let selectedSTR = topModelArray[0]["name"] as? String
                if (arrSTR?.lowercaseString == selectedSTR?.lowercaseString) {
                    convView = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("OurRangeSub") as! OurRangeSubView
                    convView.modelArray = builtInOvenArray
                    convView.TOPcategoryType = "1"
                    let dict = builtInOvenArray[0] as! Dictionary<String,AnyObject>

                    detailStr = dict["category_description"] as! String
                    convView.headerString = detailStr
                    if detailStr == "" {
                        convView.hideHederDetailLabel = true
                    }
                    
                    ControllerArray.insert(convView, atIndex: 0)
                }
            }
            else if selectedTopCategory == 1{
                let arrSTR = arrayIndex as? String
                let selectedSTR = topModelArray[1]["name"] as? String
                if (arrSTR?.lowercaseString == selectedSTR?.lowercaseString) {
                    convView = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("OurRangeSub") as! OurRangeSubView
                    convView.modelArray = builtInMicroArray
                    convView.TOPcategoryType = "2"
                    let dict = builtInMicroArray[0] as! Dictionary<String,AnyObject>

                    detailStr = dict["category_description"] as! String
                    convView.headerString = detailStr
                    if detailStr == "" {
                        convView.hideHederDetailLabel = true
                    }

                    ControllerArray.insert(convView, atIndex: 0)
                }
            }
            else if selectedTopCategory == 2{
                
                if (arrayIndex as! String == "CONVECTION") {
                    convView = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("OurRangeSub") as! OurRangeSubView
                    convView.modelArray = convectionArray
                    convView.TOPcategoryType = "3"
                    let dict = convectionArray[0] as! Dictionary<String,AnyObject>

                    detailStr = dict["category_description"] as! String
                    convView.headerString = detailStr
                    if detailStr == "" {
                        convView.hideHederDetailLabel = true
                    }

                    ControllerArray.insert(convView, atIndex: 0)
                }
                
                if (arrayIndex as! String == "GRILL")  {
                    grillView = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("OurRangeSub") as! OurRangeSubView
                    grillView.modelArray = grillArray
                    grillView.TOPcategoryType = "3"
                    let dict = grillArray[0] as! Dictionary<String,AnyObject>
                    detailStr = dict["category_description"] as! String
                    grillView.headerString = detailStr
                    if detailStr == "" {
                        grillView.hideHederDetailLabel = true
                    }

                    ControllerArray.insert(grillView, atIndex: 1)
                }
                
                if (arrayIndex as! String == "SOLO") {
                    soloView = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("OurRangeSub") as! OurRangeSubView
                    soloView.modelArray = soloArray
                    soloView.TOPcategoryType = "3"
                    let dict = soloArray[0] as! Dictionary<String,AnyObject>
                    detailStr = dict["category_description"] as! String
                    soloView.headerString = detailStr
                    if detailStr == "" {
                        soloView.hideHederDetailLabel = true
                    }

                    ControllerArray.insert(soloView, atIndex: 2)
                }
            }
            
        }
        setViewControllers(ControllerArray, animated: false)
    }
    
    //  Add and remove view controllers
    
    func setViewControllers(viewControllers : NSArray, animated : Bool) {
        if self.childViewControllers.count > 0 {
            for vC in self.childViewControllers {
                vC.willMoveToParentViewController(nil)
                vC.removeFromParentViewController()
            }
        }
        
        for vC in viewControllers {
            
            self.addChildViewController(vC as! UIViewController)
            vC.didMoveToParentViewController(self)
        }
        
        //TODO animations
        if ((detailScroll) != nil) {
            reloadPages()
        }
    }
    
    func reloadPages() {
        var scrollContentCount = 0;
        
        for arrayIndex in 0 ..< headerArray.count {
            // set scorllview properties
            var frame : CGRect = CGRectMake(0, 0, 0, 0)
            frame.origin.x = ScreenSize.SCREEN_WIDTH * CGFloat(arrayIndex)
            frame.origin.y = 0;
            frame.size = ScreenSize.SCREEN.size
            
            detailScroll.pagingEnabled = true
            let contentWidth : CGFloat = ScreenSize.SCREEN_WIDTH * CGFloat(scrollContentCount+1)
            widthConstraint.constant = contentWidth;
            
            let vC = self.childViewControllers[arrayIndex]
            addView(vC.view, contentView: contentView, frame: frame)
            
            scrollContentCount = scrollContentCount + 1;
            //            previousScrollIndex=0;
        }
        
        detailScroll.contentSize = CGSizeMake(widthConstraint.constant, detailScroll.frame.size.height)
        detailScroll.delegate = self
    }
    
    // adding views to scrollview
    func addView(view : UIView, contentView : UIView, frame : CGRect)  {
        view.frame = frame
        view.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(view)
        
        contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: contentView, attribute:  NSLayoutAttribute.Leading, multiplier: 1.0, constant: frame.origin.x))
        contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute:  NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: frame.size.width))
        contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: contentView, attribute:  NSLayoutAttribute.Top, multiplier: 1.0, constant:0))
        
        contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute:  NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: frame.size.height - (100 + self.headerHeight.constant)))
    }
    
    
    //MARK:- UPDATE TABLES Based on Model Selection
    func updateModelSelectionTables(noti: NSNotification) {
        if selectedTopCategory == 0 {
            convView.modelTable.reloadData()
        }
        else if selectedTopCategory == 1{
            convView.modelTable.reloadData()
        }
        else {
            convView.modelTable.reloadData()
            grillView.modelTable.reloadData()
            soloView.modelTable.reloadData()
        }
    }
    
    //MARK:- TABLEVIEW
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(topModelArray.count)
        return topModelArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cellIdentifier = String()
        if DeviceType.IS_IPAD {
            cellIdentifier = "TopCategoryCell_Ipad"
        }
        else{
            cellIdentifier = "TopCategoryCell"
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath)
        cell.backgroundColor = UIColor.clearColor()
        let title = cell.contentView.viewWithTag(9901) as! UILabel
        let img2 = cell.contentView.viewWithTag(9902) as! UIImageView
        
        img2.image = UIImage.init(named: "radio_whiteOFF")
        let STR = topModelArray[indexPath.row]["name"] as? String
        title.text = STR!.uppercaseString
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        let img2 = cell!.contentView.viewWithTag(9902) as! UIImageView
        img2.image = UIImage.init(named: "radio_whiteON")
        selectedTopCategory = indexPath.row
        
        if selectedTopCategory == 0 {
            
            let str = topModelArray[indexPath.row]["name"] as? String
            headerArray = [(str?.uppercaseString)!]
            headerHeight.constant = 0
            headerLabel.text = (str?.capitalizedString)
            
        }
        else if selectedTopCategory == 1 {
            let str = topModelArray[indexPath.row]["name"] as? String
            headerArray = [(str?.uppercaseString)!]
            headerHeight.constant = 0
            headerLabel.text = (str?.capitalizedString)

        }
        else if selectedTopCategory == 2 {
            let str = topModelArray[indexPath.row]["name"] as? String

            headerArray = ["CONVECTION","GRILL","SOLO"]
            headerHeight.constant = (DeviceType.IS_IPAD ? 50 : 40)
            headerLabel.text = (str?.capitalizedString)

        }
        
        self.GetARRAYforTopCatSelected(selectedTopCategory + 1)
       
    }
    
    
    //MARK:- GET MICROWAVE FOT TOP CAT SELECTED
    func GetARRAYforTopCatSelected(topCat : Int){
        if Reachability.isConnectedToNetwork() {
            Constants.appDelegate.startIndicator()
            let method = "microwaves?top_category_id=\(topCat)"
            
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                Constants.appDelegate.stopIndicator()
                
                if response[Constants.STATE] as! Int == 1 {
                    NSUserDefaults.standardUserDefaults().setObject(response["data"], forKey:Constants.RANGE)
                    var arr = [AnyObject]()
                    
                    for item in response["data"] as! Array<AnyObject>
                    {
                        let dict = item as! Dictionary<String, AnyObject>
                        let newDIct = NSMutableDictionary()
                        
                        let idStr = dict["id"] as! String
                        let nameStr = dict["title"] as! String
                        let imageStr = dict["image"] as! String
                        let volStr = dict["capacity"] as! String
                        let topCatID = dict["top_category_id"] as! String

                        newDIct.setObject(idStr, forKey: "id")
                        newDIct.setObject(nameStr, forKey: "name")
                        newDIct.setObject(imageStr, forKey: "image")
                        newDIct.setObject(volStr, forKey: "capacity")
                        newDIct.setObject(topCatID, forKey: "top_category_id")

                        arr.append(newDIct)
                    }
                    
                    NSUserDefaults.standardUserDefaults().setObject(arr, forKey: Constants.MODEL)
                    
                    self.getArraysForAllCategories(topCat)
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        // Do stuff to UI
                        self.topCategoryView.hidden = true
                        
                        if self.headerArray.count > 1{
                            self.setHeaderScroll()
                        }
                        self.setUpScrollView()
                    }
                }
                else{
                    
                }
            })
        }
        else {
                CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
}

