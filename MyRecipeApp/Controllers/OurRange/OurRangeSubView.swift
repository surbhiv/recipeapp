//
//  OurRangeSubView.swift
//  MyRecipeApp
//
//  Created by Surbhi on 08/11/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class OurRangeSubView: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var CategoryDetailLabel: UILabel!
    @IBOutlet weak var modelTable: UITableView!
    @IBOutlet weak var detailHeight: NSLayoutConstraint!

    
    var categoryType = String()
    var TOPcategoryType = String()
    
    var convArray = [AnyObject]()
    var grillArray = [AnyObject]()
    var soloArray = [AnyObject]()
    var modelArray = [AnyObject]()
    
    var headerString = String()
    var hideHederDetailLabel = Bool()
    
    let cellReuseIdentifier = "OurRangeViewCell"
    var isSelected :Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CategoryDetailLabel.text = headerString
        
        if hideHederDetailLabel == true {
            detailHeight.constant = 0
        }
        
        modelTable.hidden = false
        modelTable.delegate = self
        modelTable.dataSource = self
        modelTable.reloadData()
        
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    //MARK:- Tableview
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return modelArray.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell : OurRangeViewCell = modelTable.dequeueReusableCellWithIdentifier(cellReuseIdentifier, forIndexPath: indexPath) as! OurRangeViewCell
        cell.contentView.layer.shadowColor = Colors.ShadowColor.CGColor
        cell.contentView.clipsToBounds = true
        
        var dict  =  Dictionary<String, AnyObject>()
        
        dict = modelArray[indexPath.section] as! Dictionary<String, AnyObject>

        
        cell.modelName.text = dict["title"] as? String
        cell.modelVolume.text = dict["capacity"] as? String
        let amount = dict["price"] as? String
        cell.modelPrice.text = Constants.RUPEE+amount!
        
        cell.modelImage.image = nil
        
        
        let myActivityIndicator = UIActivityIndicatorView()
        myActivityIndicator.center = cell.modelImage.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.activityIndicatorViewStyle = .Gray
        cell.modelImage.addSubview(myActivityIndicator)
        myActivityIndicator.startAnimating()

        let imgURL: String = (dict["image"] as? String)!
        Helper.loadImageFromUrlWithIndicator(imgURL, view:  cell.modelImage, indic: myActivityIndicator)

        cell.selectionButton.addTarget(self, action: #selector(selectionButtonPressed), forControlEvents: UIControlEvents.TouchUpInside)
        
        var fon = UIFont()
        if DeviceType.IS_IPAD {
            fon = UIFont.init(name: "Helvetica-Light", size: 16.0)!
        }
        else{
            fon = UIFont.init(name: "Helvetica-Light", size: 13.0)!
        }
        
        
        let attrs = [ NSFontAttributeName : fon,
            NSForegroundColorAttributeName : Colors.Apptextcolor,
            NSUnderlineStyleAttributeName : 1]
        
        let  attributedString1 = NSMutableAttributedString(string:"View Details", attributes:attrs)
        cell.viewDetailButton.setAttributedTitle(attributedString1, forState: .Normal)
        cell.viewDetailButton.addTarget(self, action:#selector(OpenDetailView), forControlEvents: UIControlEvents.TouchUpInside)
        
        
        let  attributedString2 = NSMutableAttributedString(string:"View Recipe", attributes:attrs)
        cell.viewrecipeButton.setAttributedTitle(attributedString2, forState: .Normal)
        cell.viewrecipeButton.addTarget(self, action: #selector(selectionButtonPressed), forControlEvents: UIControlEvents.TouchUpInside)
        
        if NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED) == "" || NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED) == nil
        {
            cell.selectionButton.selected = false
            isSelected = false
        }
        else
        {
            let idStr = dict["id"] as! String
            
            if idStr == NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED) && TOPcategoryType == NSUserDefaults.standardUserDefaults().stringForKey(Constants.TOPModelSELCTED)
            {
                cell.selectionButton.selected = true
                isSelected = true
            }
            else
            {
                cell.selectionButton.selected = false
            }
        }
        
        return cell
    }
    
    //MARK:- SELECTION BUTTON PRESSED  
    @IBAction func selectionButtonPressed(sender : AnyObject)
    {
        let cell = (sender.superview)!!.superview as! UITableViewCell
        let indexp = modelTable.indexPathForCell(cell)
        var dict  =  Dictionary<String, AnyObject>()
        
        
        dict = modelArray[indexp!.section] as! Dictionary<String, AnyObject>

        let idStr = dict["id"] as! String
        
        
        if NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED) == "" || NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED) == nil
        {
            NSUserDefaults.standardUserDefaults().setObject(idStr, forKey: Constants.ModelSELCTED)
            NSUserDefaults.standardUserDefaults().setObject(TOPcategoryType, forKey: Constants.TOPModelSELCTED)
            NSNotificationCenter.defaultCenter().postNotificationName("ModelSelection", object: nil)
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setObject(idStr, forKey: Constants.ModelSELCTED)
            NSUserDefaults.standardUserDefaults().setObject(TOPcategoryType, forKey: Constants.TOPModelSELCTED)
            NSNotificationCenter.defaultCenter().postNotificationName("ModelSelection", object: nil)
        }

        let mo = sideMenuController()?.sideMenu?.menuViewController as! MenuTableView
        mo.UPDATEHeaderView(2)

        mo.tableView(mo.tableView, didSelectRowAtIndexPath: NSIndexPath.init(forRow: 2, inSection: 0))
        mo.tableView.selectRowAtIndexPath(NSIndexPath(forRow: 2, inSection: 0), animated: false, scrollPosition: .Middle)
        mo.toggleSideMenuView()

    }
    
    //MARK:- OPEN DETAIL VIEW
    func OpenDetailView(sender: AnyObject)
    {
        var indexPath: NSIndexPath!
        
        if let button = sender as? UIButton {
            if let superview = button.superview {
                if let cell = superview.superview as? OurRangeViewCell {
                    indexPath = modelTable.indexPathForCell(cell)
                }
            }
        }
        var dict  =  Dictionary<String, AnyObject>()
        dict = modelArray[indexPath.section] as! Dictionary<String, AnyObject>

        let destinationController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("ModelDetailView") as! ModelDetailView
        destinationController.modelDetails = dict
        
        self.navigationController?.pushViewController(destinationController, animated: true)
    }
    
    func OpenRecipeView(sender: AnyObject)
    {
        var indexPath: NSIndexPath!
        
        if let button = sender as? UIButton {
            if let superview = button.superview {
                if let cell = superview.superview as? OurRangeViewCell {
                    indexPath = modelTable.indexPathForCell(cell)
                }
            }
        }
        var dict  =  Dictionary<String, AnyObject>()
        dict = modelArray[indexPath.section] as! Dictionary<String, AnyObject>

        let destinationController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("ModelDetailView") as! ModelDetailView
        destinationController.modelDetails = dict
        self.navigationController?.pushViewController(destinationController, animated: true)
    }

}
