//
//  ModelDetailView.swift
//  MyRecipeApp
//
//  Created by Surbhi on 08/11/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit
import SafariServices
import Firebase


class ModelDetailView: UIViewController, UIScrollViewDelegate, SFSafariViewControllerDelegate {

    
    @IBOutlet weak var headerScroll: UIScrollView!
    @IBOutlet weak var detailScroll: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var modelImageView: UIImageView!
    @IBOutlet weak var modelTitleLabel: UILabel!
    @IBOutlet weak var modelPriceLabel: UILabel!
    
    
    var indicatorLabel : UILabel!
    var ControllerArray = [AnyObject]()
    var headerArray = ["DESCRIPTION","VIDEOS","PANEL"]
    
    
    var modelDetails = Dictionary<String,AnyObject>()

    var desrcriptionArray = [AnyObject]()
    var videosArray = [AnyObject]()
    
    var descView = ModelViewDescription()
    var videoView = ModelViewVideos()
    var knowView = ModelViewKnow()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.navigationBarHidden = true
        
        FIRAnalytics.logEventWithName(kFIREventSelectContent, parameters: [
            kFIRParameterItemID:"3",
            kFIRParameterItemName:"Microwave",
            kFIRParameterContentType:"Microwave Detail Page"
            ])
//        let panels = (modelDetails["panels"])!
        
        if let y = modelDetails["panels"] as? String {
            print(y)
            self.headerArray = ["DESCRIPTION","VIDEOS"]
        }
        else
        {
            self.headerArray = ["DESCRIPTION","VIDEOS","PANEL"]
        }
        
        widthConstraint.constant = ScreenSize.SCREEN_WIDTH
        
        setUpView()
    }

    
    //MARK:- BUTTON Actions
    
    @IBAction func BackButtonPressed(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    @IBAction func BuyNowPressed(sender: AnyObject) {
        
        let urlString = modelDetails["web_url"] as! String
        
        if urlString != "" {
            
            
            FIRAnalytics.logEventWithName(kFIREventSelectContent, parameters: [
                kFIRParameterItemID:"4",
                kFIRParameterItemName:"Buy",
                kFIRParameterContentType:"go To Buy From App"
                ])

            
            let svc = SFSafariViewController(URL: NSURL(string: urlString)!)
            svc.delegate = self
            self.presentViewController(svc, animated: true, completion: nil)
        }
        else
        {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"", "message":"Some Error Occurred"])
        }
    }
    
    func safariViewControllerDidFinish(controller: SFSafariViewController)
    {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK:- SetUpView
    func setUpView()
    {
        let myActivityIndicator = UIActivityIndicatorView()
        myActivityIndicator.center = self.headerView.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.activityIndicatorViewStyle = .Gray
        self.modelImageView.addSubview(myActivityIndicator)
        myActivityIndicator.startAnimating()

        let imgStr = modelDetails["image"] as? String
        Helper.loadImageFromUrlWithIndicator(imgStr!, view:  modelImageView, indic: myActivityIndicator)

        modelTitleLabel.text = modelDetails["title"] as? String
        let amount = modelDetails["price"] as! String
        modelPriceLabel.text = Constants.RUPEE+amount
        
        dispatch_async(dispatch_get_main_queue()) {
            // Do stuff to UI
            self.setHeaderScroll()
            self.setUpScrollView()
        }
        
        NSNotificationCenter.defaultCenter().postNotificationName("RatingViewCanBeShowm", object: UIApplication.sharedApplication())
    }

    // MARK: - Label ScrollView
    
    func setHeaderScroll()
    {
        var scrollContentCount = 0;
        var frame : CGRect = CGRectMake(0, 0, 0, 0)
        var labelx : CGFloat = 0.0
        let count = CGFloat(headerArray.count)
        
        let width  : CGFloat = ScreenSize.SCREEN_WIDTH / count
        let firstLabelWidth : CGFloat = ScreenSize.SCREEN_WIDTH / count
        
        indicatorLabel = UILabel.init(frame: CGRectMake(labelx, 47, firstLabelWidth, 3))
        indicatorLabel.backgroundColor = UIColor.whiteColor()
        indicatorLabel.text = "";
        headerScroll.addSubview(indicatorLabel)
        
        for arrayIndex in 0 ..< headerArray.count
        {
            frame.origin.x = labelx
            // set scorllview properties
            frame.origin.y = 0;
            frame.size.width=width;
            frame.size.height=45;
            
            let accountLabelButton = UIButton.init(type: UIButtonType.Custom)
            accountLabelButton.addTarget(self, action:#selector(HeaderlabelSelected), forControlEvents: UIControlEvents.TouchUpInside)
            
            accountLabelButton.frame = frame;
            accountLabelButton.tag = arrayIndex;
            
            accountLabelButton.titleLabel?.numberOfLines = 0
            accountLabelButton.setAttributedTitle(NSAttributedString(string: headerArray[arrayIndex],  attributes: [NSFontAttributeName : UIFont.init(name: "Helvetica-Bold", size: 15.0)!, NSForegroundColorAttributeName : UIColor.whiteColor()]), forState: UIControlState.Normal)
            
            if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS {
                accountLabelButton.setAttributedTitle(NSAttributedString(string: headerArray[arrayIndex],  attributes: [NSForegroundColorAttributeName : Colors.Lighttextcolor,NSFontAttributeName : UIFont.init(name: "Helvetica-Bold", size: 13.5)!]), forState: UIControlState.Normal)
            }
            if DeviceType.IS_IPAD {
                accountLabelButton.setAttributedTitle(NSAttributedString(string: headerArray[arrayIndex],  attributes: [NSForegroundColorAttributeName : Colors.Lighttextcolor,NSFontAttributeName : UIFont.init(name: "Helvetica-Bold", size: 18.0)!]), forState: UIControlState.Normal)
            }
            accountLabelButton.titleLabel?.textAlignment = NSTextAlignment.Center
            
            headerScroll.addSubview(accountLabelButton)
            
            if arrayIndex == 0
            {
                accountLabelButton.alpha=1.0;
            }
            else
            {
                accountLabelButton.alpha=0.7;
            }
            
            labelx=labelx+width;
            scrollContentCount = scrollContentCount + 1;
        }
        
        // set scroll content size
        headerScroll.contentSize = CGSizeMake(frame.width/count, 50)//CGSizeMake(frame.origin.x+frame.size.width, 40);
        automaticallyAdjustsScrollViewInsets = false;
        detailScroll.delegate=self;
    }

    func HeaderlabelSelected(sender: UIButton)
    {
        detailScroll.tag = sender.tag
        let count = CGFloat(headerArray.count)

        let xAxis : CGFloat = ScreenSize.SCREEN_WIDTH * CGFloat(sender.tag)
        detailScroll.setContentOffset(CGPointMake(xAxis, 0), animated: true)
        
        let buttonArray = NSMutableArray()
        
        for view in headerScroll.subviews
        {
            if view.isKindOfClass(UIButton) {
                let labelButton = view as! UIButton
                buttonArray.addObject(labelButton)
                
                if labelButton.tag == sender.tag {
                    labelButton.alpha = 1.0
                }
                else
                {
                    labelButton.alpha = 0.7
                }
            }
        }
        
        let labelButton = buttonArray.objectAtIndex(sender.tag)
        var frame : CGRect = labelButton.frame
        frame.origin.y = 47
        frame.size.height = 3
        
        UIView.animateWithDuration(0.2, animations: {
            if sender.tag == 0
            {
                self.indicatorLabel.frame =  CGRectMake(0,47, ScreenSize.SCREEN_WIDTH/count, 3)
            }
            else
            {
                self.indicatorLabel.frame =  CGRectMake((ScreenSize.SCREEN_WIDTH/count) * CGFloat(sender.tag) ,47, ScreenSize.SCREEN_WIDTH/count, 3)
            }
            
            self.headerScroll.scrollRectToVisible(self.indicatorLabel.frame, animated: false)
            
            }, completion: nil)
    }

    // MARK: -  Main Scrollview
    func setUpScrollView()
    {
        for arrayIndex in headerArray
        {
            if (arrayIndex == "DESCRIPTION")
            {
                descView = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("ModelViewDescription") as! ModelViewDescription
                
                if (modelDetails["features"])?.isKindOfClass(NSString.classForCoder()) == true {
                    let arr = []
                    descView.descriptionArray = arr as [AnyObject]
                }
                else{
                    let arr = modelDetails["features"] as! Array<AnyObject>
                    descView.descriptionArray = arr
                }
                
                ControllerArray.insert(descView, atIndex: 0)
            }
            
            if (arrayIndex == "VIDEOS")
            {
                videoView = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("ModelViewVideos") as! ModelViewVideos

                if let y = modelDetails["videos"] as? String {
                    print(y)
                }
                else
                {
                    let arr = modelDetails["videos"]! as! Array<AnyObject>
                    videoView.videoArray = arr
                }
                ControllerArray.insert(videoView, atIndex: 1)
            }
            
            if (arrayIndex == "PANEL")
            {
                knowView = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("ModelViewKnow") as! ModelViewKnow
                
                let panels = (modelDetails["panels"])!
                
                if let y = modelDetails["panels"] as? String {
                    print(y)
                }
                else
                {
                    print(panels)
                    let arr =  panels as! Array<AnyObject>
                    knowView.knowoDict = arr[0] as! Dictionary<String,AnyObject>
                }
                
                
                ControllerArray.insert(knowView, atIndex: 2)
            }
            
            
//            knowView
        }
        setViewControllers(ControllerArray, animated: false)
    }

    //  Add and remove view controllers
    
    func setViewControllers(viewControllers : NSArray, animated : Bool)
    {
        if self.childViewControllers.count > 0
        {
            for vC in self.childViewControllers
            {
                vC.willMoveToParentViewController(nil)
                vC.removeFromParentViewController()
            }
        }
        
        for vC in viewControllers
        {
            
            self.addChildViewController(vC as! UIViewController)
            vC.didMoveToParentViewController(self)
        }
        //TODO animations
        if ((detailScroll) != nil)
        {
            reloadPages()
        }
    }

    func reloadPages()
    {
        var scrollContentCount = 0;
        
        for arrayIndex in 0 ..< headerArray.count
        {
            // set scorllview properties
            var frame : CGRect = CGRectMake(0, 0, 0, 0)
            frame.origin.x = ScreenSize.SCREEN_WIDTH * CGFloat(arrayIndex)
            frame.origin.y = 0;
            frame.size = ScreenSize.SCREEN.size
            
            detailScroll.pagingEnabled = true
            let contentWidth : CGFloat = ScreenSize.SCREEN_WIDTH * CGFloat(scrollContentCount+1)
            widthConstraint.constant = contentWidth;
            
            let vC = self.childViewControllers[arrayIndex]
            addView(vC.view, contentView: contentView, frame: frame)
            
            scrollContentCount = scrollContentCount + 1;
            //            previousScrollIndex=0;
        }
        
        detailScroll.contentSize = CGSizeMake(widthConstraint.constant, detailScroll.frame.size.height)
        detailScroll.delegate = self
    }

    // adding views to scrollview
    func addView(view : UIView, contentView : UIView, frame : CGRect)
    {
        view.frame = frame
        view.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(view)
        
        contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: contentView, attribute:  NSLayoutAttribute.Leading, multiplier: 1.0, constant: frame.origin.x))
        contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute:  NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: frame.size.width))
        contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: contentView, attribute:  NSLayoutAttribute.Top, multiplier: 1.0, constant:0))
        
        contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute:  NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant:  detailScroll.frame.size.height))//frame.size.height - 250
    }

}
