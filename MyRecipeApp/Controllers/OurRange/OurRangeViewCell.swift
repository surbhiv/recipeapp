//
//  OurRangeViewCell.swift
//  MyRecipeApp
//
//  Created by Surbhi on 08/11/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class OurRangeViewCell: UITableViewCell {

    @IBOutlet weak var modelImage: UIImageView!
    @IBOutlet weak var modelName: UILabel!
    @IBOutlet weak var modelVolume: UILabel!
    @IBOutlet weak var modelPrice: UILabel!
    
    @IBOutlet weak var viewDetailButton: UIButton!
    @IBOutlet weak var viewrecipeButton: UIButton!
    @IBOutlet weak var selectionButton: UIButton!
    
}
