//
//  ModelViewDescription.swift
//  MyRecipeApp
//
//  Created by Surbhi on 08/11/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class ModelViewDescription: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var descriptionTable: UITableView!
    @IBOutlet weak var noCount: UILabel!

    var descriptionArray = [AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if descriptionArray.count == 0 {
            descriptionTable.hidden = true
            noCount.hidden = false
        }
        else
        {
            descriptionTable.hidden = false
            noCount.hidden = true
            descriptionTable.delegate = self
            descriptionTable.dataSource = self
            descriptionTable.reloadData()
        }
        
        self.automaticallyAdjustsScrollViewInsets = false

    }
    
    //MARK: - TABLEVIEW
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return descriptionArray.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("DescriptionCell")
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "DescriptionCell")
        }
        
        cell?.layer.shadowColor = Colors.ShadowColor.CGColor
        cell?.clipsToBounds = true
        
        let heading = cell?.contentView.viewWithTag(1041) as! UILabel
        let description = cell?.contentView.viewWithTag(1043) as! UILabel
        let img = cell?.contentView.viewWithTag(1042) as! UIImageView

        let dict = descriptionArray[indexPath.section] as! Dictionary<String, AnyObject>
        
        heading.text = dict["title"] as? String
        description.text = dict["short_description"] as? String

        let imgStr = dict["mnemonic"] as? String
        
        
        let myActivityIndicator = UIActivityIndicatorView()
        myActivityIndicator.center = img.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.activityIndicatorViewStyle = .Gray
        img.addSubview(myActivityIndicator)
        myActivityIndicator.startAnimating()

        
        if imgStr != "" {
            Helper.loadImageFromUrlWithIndicator(imgStr!, view:  img, indic: myActivityIndicator)

        }
        
        descriptionTable.estimatedRowHeight = 175.0
        descriptionTable.rowHeight = UITableViewAutomaticDimension
        
        return cell!
    }

}
