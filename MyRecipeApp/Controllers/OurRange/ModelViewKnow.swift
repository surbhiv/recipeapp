//
//  ModelViewKnow.swift
//  MyRecipeApp
//
//  Created by Surbhi on 11/01/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class ModelViewKnow: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var knowTable: UITableView!
    @IBOutlet weak var noCount: UILabel!

    var knowoArray = [AnyObject]()
    var knowoDict = Dictionary<String, AnyObject>()

    override func viewDidLoad() {
        super.viewDidLoad()

        
        if knowoDict.count == 0 {
            knowTable.hidden = true
            noCount.hidden = false
        }
        else
        {
            if let y = knowoDict["panels"] as? String {
               print(y)
            }
            else
            {
                let arr = knowoDict["panel_icons"] as! Array<AnyObject>
                self.knowoArray = arr
            }
            
            knowTable.hidden = false
            noCount.hidden = true
            knowTable.delegate = self
            knowTable.dataSource = self
            knowTable.reloadData()
        }
        self.automaticallyAdjustsScrollViewInsets = false

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK: - TABLEVIEW
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        else if section == 1{
            
            let titleSTr = knowoDict["title"] as? String
            if titleSTr == "" || titleSTr == nil {
                return 0
            }
            return 1
        }
        else{
            return knowoArray.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            
            var cell = tableView.dequeueReusableCellWithIdentifier("DescriptionCell_1")
            
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "DescriptionCell_1")
            }
            
            cell?.layer.shadowColor = Colors.ShadowColor.CGColor
            cell?.clipsToBounds = true
            
            let img = cell?.contentView.viewWithTag(4011) as! UIImageView
            img.backgroundColor = UIColor.whiteColor()
            let imgURL = knowoDict["mnemonic"] as? String
            if imgURL != "" {
                Helper.loadImageFromUrl(imgURL!, view: img, width: 320)
            }
            
            return cell!
        }
        else if indexPath.section == 1{
            
            var cell = tableView.dequeueReusableCellWithIdentifier("DescriptionCell_2")
            
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "DescriptionCell_2")
            }
            
            cell?.layer.shadowColor = Colors.ShadowColor.CGColor
            cell?.clipsToBounds = true
            
            let heading = cell?.contentView.viewWithTag(4012) as! UILabel
            let desc = cell?.contentView.viewWithTag(4013) as! UILabel
            
            let titleSTR  = knowoDict["title"] as? String
            let descStr = knowoDict["short_description"] as? String

            heading.text = titleSTR!
            desc.text = descStr!
            
            return cell!
        }
        else
        {
            
            var cell = tableView.dequeueReusableCellWithIdentifier("DescriptionCell_3")
            
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "DescriptionCell_3")
            }
            
            cell?.layer.shadowColor = Colors.ShadowColor.CGColor
            cell?.clipsToBounds = true
            
            let heading = cell?.contentView.viewWithTag(4015) as! UILabel
            
            let img = cell?.contentView.viewWithTag(4014) as! UIImageView
            img.backgroundColor = UIColor.whiteColor()
            let dict = knowoArray[indexPath.row] as! Dictionary<String, AnyObject>

            heading.text = dict["icon_description"] as? String
            
            let imgStr = dict["icon_image"] as? String

            if imgStr != "" {
                Helper.loadImageFromUrl(imgStr!, view: img, width: 40)
            }
            
            return cell!
        }
    }

}
