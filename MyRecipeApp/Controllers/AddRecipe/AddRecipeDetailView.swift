//
//  AddRecipeDetailView.swift
//  MyRecipeApp
//
//  Created by Surbhi on 18/11/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class AddRecipeDetailView: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource
{

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var nameLine: UILabel!
    @IBOutlet weak var nameLineHeight: NSLayoutConstraint!
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var timeText: UITextField!
    @IBOutlet weak var timeLine: UILabel!
    @IBOutlet weak var timeLineHeight: NSLayoutConstraint!
    
    @IBOutlet weak var servingLabel: UILabel!
    @IBOutlet weak var servingText: UITextField!
    @IBOutlet weak var servingLine: UILabel!
    @IBOutlet weak var servingLineHeight: NSLayoutConstraint!
    
    @IBOutlet weak var selectLevelButton: UIButton!
    @IBOutlet weak var selectMicrowaveButton: UIButton!
    var microwaveID = [AnyObject]()

    @IBOutlet weak var vegButton: UIButton!
    @IBOutlet weak var nonVegButton: UIButton!
    
    @IBOutlet weak var continueToIngedient : UIButton!
    
    @IBOutlet weak var levelTable: UITableView!
    @IBOutlet weak var levelTableHeight: NSLayoutConstraint!
    
    var leverArray = [AnyObject]()
    var leveSelected : String = "0"
    var microwaveString : String = "0"
    var mealtypeString : String = ""
    var mwo_TopModelCatId = Int()

    let pref_Font : CGFloat = (DeviceType.IS_IPAD ? 16.5 : 13.5)
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        leverArray = NSUserDefaults.standardUserDefaults().objectForKey(Constants.LEVEL) as! Array<AnyObject>
        levelTable.reloadData()
        
        nameText.attributedPlaceholder = NSAttributedString(string: "Recipe Name",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: "Helvetica", size: pref_Font)!])
        timeText.attributedPlaceholder = NSAttributedString(string: "Cooking Time in Minutes",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName :  UIFont(name: "Helvetica", size: pref_Font)!])
        servingText.attributedPlaceholder = NSAttributedString(string: "Number of People Served",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName :  UIFont(name: "Helvetica", size: pref_Font)!])
        
        
        if nameText.text == "" {
            nameLabel.hidden = true
        }
        if timeText.text == ""{
            timeLabel.hidden = true
        }
        if servingText.text == ""{
            servingLabel.hidden = true
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func VEgButtonPressed(sender: AnyObject) {
        resgnTextFields()
        
        vegButton.selected = true
        nonVegButton.selected = false
    }
    
    @IBAction func NONVegButtonPressed(sender: AnyObject) {
        resgnTextFields()
        
        vegButton.selected = false
        nonVegButton.selected = true
    }

    //MARK: - RESIGN Textfield
    func resgnTextFields()
    {
        nameText.resignFirstResponder()
        servingText.resignFirstResponder()
        timeText.resignFirstResponder()
    }
    
    @IBAction func LevelPressed(sender:AnyObject) {
        resgnTextFields()
        if levelTable.hidden == true {
            
            UIView.animateWithDuration(5.0, delay: 0.0, usingSpringWithDamping: 6.0, initialSpringVelocity: 2.6, options: UIViewAnimationOptions.TransitionCurlDown, animations: {
                self.view.bringSubviewToFront(self.levelTable)
                self.levelTable.hidden = false
                self.levelTableHeight.constant = CGFloat(self.leverArray.count) * 32.0
                
                if DeviceType.IS_IPAD{
                    self.levelTableHeight.constant = CGFloat(self.leverArray.count) * 45.0
                }
                
                self.view.bringSubviewToFront(self.levelTable)

                }, completion: nil)
        }
        else
        {
            UIView.animateWithDuration(2.0, delay: 0.0, usingSpringWithDamping: 2.6, initialSpringVelocity: 6.0, options: UIViewAnimationOptions.TransitionCurlUp, animations: {
                self.levelTable.hidden = true
                self.levelTableHeight.constant = 0
                
                self.view.sendSubviewToBack(self.levelTable)

                }, completion: nil)
        }
    }
    
    @IBAction func MicrowavePressed(sender:AnyObject) {
        resgnTextFields()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(modelsSelectedMethod), name: "ModelsSelectedNoti", object: nil)
        
        let modelVC = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("modelSelectionVC") as! modelSelectionVC
        modelVC.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext
        self.presentViewController(modelVC, animated: true, completion: nil)
    }

    func modelsSelectedMethod(noti : NSNotification)
    {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ModelsSelectedNoti", object: nil)
        microwaveString = String()
        microwaveID = [AnyObject]()
        
        let arr = noti.userInfo!["infoData"] as! Array<AnyObject>
        let isAllModelSelecetd = noti.userInfo!["AllModelSelectedKey"] as! Bool
        
        if isAllModelSelecetd == true {
            let title: String = "All Models"//dc1["name"] as! String
            selectMicrowaveButton.setTitle(title, forState: .Normal)
        }
        else{
            let dc1 = arr[0] as! Dictionary<String,AnyObject>
            let title: String = dc1["name"] as! String
            selectMicrowaveButton.setTitle(title, forState: .Normal)
        }
        
        
        for item in arr
        {
            let dc = item as! Dictionary<String,AnyObject>
            let idstr = dc["id"] as! String
            microwaveID.append(idstr)

//            let topidstr = dc["top_category_id"] as! String
//            let dict = [idstr : topidstr]
//            microwaveID.append(dict)

        }
        
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(microwaveID, options: .PrettyPrinted)
            let string = NSString(data: jsonData, encoding: NSUTF8StringEncoding)
            microwaveString = String.init(string!)
            // use jsonData                                      
        } catch {
            // report error
        }
        
    }
    
    //MARK:- TABLEVIEW LEVEL
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return leverArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        var cell = UITableViewCell()
        cell = tableView.dequeueReusableCellWithIdentifier("recipeLevelCell", forIndexPath: indexPath)
        cell.backgroundColor = Colors.appThemeColor
        let lab = cell.contentView.viewWithTag(100) as! UILabel
        
        let dit = leverArray[indexPath.row] as! Dictionary<String,AnyObject>
        let tit = dit["title"] as! String
        lab.text = tit
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let dit = leverArray[indexPath.row] as! Dictionary<String,AnyObject>
        let ids = dit["id"] as! String
        let tit = dit["title"] as! String
        leveSelected = ids
        selectLevelButton.setTitle(tit, forState: .Normal)
        
        levelTable.hidden = true
        self.levelTableHeight.constant = 0
        self.view.sendSubviewToBack(self.levelTable)

    }
    
    //MARK:- TEXTView
    // MARK: TEXTFIELD DELEGATE
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if textField == nameText
        {
            ACTIVATETextField(nameText, textLabel: nameLabel, line: nameLine, height: nameLineHeight)
        }
        else if textField == timeText
        {
            ACTIVATETextField(timeText, textLabel: timeLabel, line: timeLine, height: timeLineHeight)
        }
        else if textField == servingText
        {
            ACTIVATETextField(servingText, textLabel: servingLabel, line: servingLine, height: servingLineHeight)
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        if textField == nameText
        {
            DEACTIVATETextField(nameText, textLabel: nameLabel, line: nameLine, height: nameLineHeight)
        }
        else if textField == timeText
        {
            DEACTIVATETextField(timeText, textLabel: timeLabel, line: timeLine, height: timeLineHeight)
        }
        else if textField == servingText
        {
            DEACTIVATETextField(servingText, textLabel: servingLabel, line: servingLine, height: servingLineHeight)
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == timeText
        {
            if (range.location > 1)
            {
                return false
            }
        }
        
        if textField == servingText
        {
            if (range.location > 2)
            {
                return false
            }
        }
        return true
    }
    
    
    func ACTIVATETextField(textf : UITextField,textLabel : UILabel , line: UILabel, height : NSLayoutConstraint)
    {
        dispatch_async(dispatch_get_main_queue())
        {
            UIView.animateWithDuration(1.0, delay: 0, options: UIViewAnimationOptions.TransitionFlipFromBottom, animations: {
                
                textLabel.hidden = false
                textf.attributedPlaceholder = NSAttributedString(string: "")
                Helper.activatetextField(line,height: height)
                
                }, completion: { (true) in
                    textLabel.hidden = false
            })
        }
    }
    
    
    func DEACTIVATETextField(textf : UITextField,textLabel : UILabel , line: UILabel, height : NSLayoutConstraint)
    {
        if textf.text == ""
        {
            dispatch_async(dispatch_get_main_queue())
            {
                UIView.animateWithDuration(1.0, animations: {
                    textLabel.hidden = true
                    textf.attributedPlaceholder = NSAttributedString(string: textLabel.text! ,  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName :  UIFont.init(name: "Helvetica", size: self.pref_Font)!])
                    }, completion: nil)
            }
        }
        dispatch_async(dispatch_get_main_queue())
        {
            Helper.deActivatetextField(line,height: height)
        }
    }

}
