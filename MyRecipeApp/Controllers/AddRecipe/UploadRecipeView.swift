//
//  UploadRecipeView.swift
//  MyRecipeApp
//
//  Created by Surbhi on 18/11/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

extension UIImage {
    var uncompressedPNGData: NSData?      { return UIImagePNGRepresentation(self)        }
    var highestQualityJPEGNSData: NSData? { return UIImageJPEGRepresentation(self, 1.0)  }
    var highQualityJPEGNSData: NSData?    { return UIImageJPEGRepresentation(self, 0.75) }
    var mediumQualityJPEGNSData: NSData?  { return UIImageJPEGRepresentation(self, 0.5)  }
    var lowQualityJPEGNSData: NSData?     { return UIImageJPEGRepresentation(self, 0.25) }
    var lowestQualityJPEGNSData:NSData?   { return UIImageJPEGRepresentation(self, 0.0)  }
}

class UploadRecipeView: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    
    @IBOutlet weak var notesLabel: UILabel!
    @IBOutlet weak var notestext: UITextField!
    @IBOutlet weak var notesLine: UILabel!
    @IBOutlet weak var notesLineHeight: NSLayoutConstraint!
    
    @IBOutlet weak var imageSelected: UIImageView!
    @IBOutlet weak var imageSelectedHeight: NSLayoutConstraint!
    @IBOutlet weak var imageSelectedWidth: NSLayoutConstraint!
    
    @IBOutlet weak var uploadRecipeImageBtn: UIButton!

    @IBOutlet weak var acceptCheckBoxButton: UIButton!
    @IBOutlet weak var shareWIthPublicButton: UIButton!
    @IBOutlet weak var termsButton: UIButton!
    @IBOutlet weak var continueUploadingButton: UIButton!
    @IBOutlet weak var scrollVw: UIScrollView!
    @IBOutlet weak var contentView: UIView!

    @IBOutlet weak var imageSizePop: UILabel!
    
    @IBOutlet weak var contentViewHeight: NSLayoutConstraint!
    let imagePicker = UIImagePickerController()
    var imageDataIMG = UIImage ()
    var imageData : NSData? = nil
    var isTermsAccepted : Bool = false
    var ispublicShare : Bool = false

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        notestext.attributedPlaceholder = NSAttributedString(string: "Enter notes here",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: "Helvetica", size: (DeviceType.IS_IPAD ? 17.0 : 13.5))!])
        
        
        if notestext.text == "" {
            notesLabel.hidden = true
        }

        imageSelectedHeight.constant = 0
        
        let attrs = [ NSFontAttributeName : UIFont(name: "Helvetica", size: (DeviceType.IS_IPAD ? 17.0 : 13.5))!,
                      NSForegroundColorAttributeName : Colors.Apptextcolor,
                      NSUnderlineStyleAttributeName : 1]
        
        let  attributedString1 = NSMutableAttributedString(string:"I accept terms & conditions", attributes:attrs)
        termsButton.setAttributedTitle(attributedString1, forState: .Normal)

        uploadRecipeImageBtn.layer.borderWidth = 1.5
        uploadRecipeImageBtn.layer.borderColor = Colors.appThemeColor.CGColor
        uploadRecipeImageBtn.layer.cornerRadius = 4.0
        uploadRecipeImageBtn.clipsToBounds = true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    //MARK:- SELECTING RECIPE IMAGE
    @IBAction func uploadImagePressed(sender: AnyObject) {
        
        notestext.resignFirstResponder()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .PhotoLibrary
        presentViewController(imagePicker, animated: true, completion: nil)

    }
    
    // IMAGE PICKER Delegates
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            let imgDat2 = UIImagePNGRepresentation(pickedImage)! as NSData
            let imageSize2: Int = imgDat2.length
            print("ORIGINAL size of image in KB: %f ", imageSize2/1024)

            
            let resizedImage = Helper.resizeImage(pickedImage, newWidth: 500)
            imageDataIMG = resizedImage

            let imgDat = UIImagePNGRepresentation(imageDataIMG)! as NSData
            let imageSize: Int = imgDat.length
            print("NEW size of image in KB: %f ", imageSize/1024)

            
            imageSelected.contentMode = .ScaleAspectFit
            let sze : CGSize = pickedImage.size
            let ratio = imageSelectedWidth.constant / sze.width
            
            imageSelectedHeight.constant = sze.height * ratio
            contentViewHeight.constant = 360 + ( sze.height * ratio )
            
            if DeviceType.IS_IPAD {
                contentViewHeight.constant = 500 + ( sze.height * ratio )
            }
            
            scrollVw.contentSize = CGSizeMake(ScreenSize.SCREEN_WIDTH, contentViewHeight.constant)
            
            imageSelected.image = pickedImage
            imageData = imgDat
        }
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }

    //MARK:- CHECKBOX TERMS & CONDITION
    @IBAction func ConditionsCheckBoxPressed(sender: AnyObject) {
        
        if acceptCheckBoxButton.selected {
            acceptCheckBoxButton.selected = false
            isTermsAccepted = false
        }
        else{
            acceptCheckBoxButton.selected = true
            isTermsAccepted = true
        }
    }
    
    @IBAction func OpenTermsandConditionView(sender: AnyObject) {
        
        let dest = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("TermsConditionVC") as! TermsConditionVC
        dest.isComingFromSideMenu = false
        self.presentViewController(dest, animated: true, completion: nil)
    }
    
    @IBAction func ShareWithPublicPressed(sender: AnyObject) {
        if shareWIthPublicButton.selected {
            shareWIthPublicButton.selected = false
            ispublicShare = false
        }
        else{
            shareWIthPublicButton.selected = true
            ispublicShare = true
        }
    }
    
    
    // MARK: TEXTFIELD DELEGATE
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if textField == notestext
        {
            ACTIVATETextField(notestext, textLabel: notesLabel, line: notesLine, height: notesLineHeight)
        }
        
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        if textField == notestext
        {
            DEACTIVATETextField(notestext, textLabel: notesLabel, line: notesLine, height: notesLineHeight)
        }
    }
    
    
    func ACTIVATETextField(textf : UITextField,textLabel : UILabel , line: UILabel, height : NSLayoutConstraint)
    {
        dispatch_async(dispatch_get_main_queue())
        {
            UIView.animateWithDuration(1.0, delay: 0, options: UIViewAnimationOptions.TransitionFlipFromBottom, animations: {
                
                textLabel.hidden = false
                textf.attributedPlaceholder = NSAttributedString(string: "")
                Helper.activatetextField(line,height: height)
                
                }, completion: { (true) in
                    textLabel.hidden = false
            })
        }
    }
    
    
    func DEACTIVATETextField(textf : UITextField,textLabel : UILabel , line: UILabel, height : NSLayoutConstraint)
    {
        if textf.text == ""
        {
            dispatch_async(dispatch_get_main_queue())
            {
                UIView.animateWithDuration(1.0, animations: {
                    textLabel.hidden = true
                    textf.attributedPlaceholder = NSAttributedString(string: textLabel.text! ,  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: "Helvetica", size: (DeviceType.IS_IPAD ? 17.0 : 13.5))!])
                    }, completion: nil)
            }
        }
        dispatch_async(dispatch_get_main_queue())
        {
            Helper.deActivatetextField(line,height: height)
        }
    }

    
    
}
