//
//  InstructionCell.swift
//  MyRecipeApp
//
//  Created by Surbhi on 19/11/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class InstructionCell: UITableViewCell {

    
    @IBOutlet weak var stepLabel: UILabel!
    @IBOutlet weak var methodText: UITextField!
    @IBOutlet weak var methodLine: UILabel!
    @IBOutlet weak var methodLineHeight: NSLayoutConstraint!
    
    
}
