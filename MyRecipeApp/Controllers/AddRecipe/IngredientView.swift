//
//  IngredientView.swift
//  MyRecipeApp
//
//  Created by Surbhi on 18/11/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit


class IngredientView: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource
{
    
    var unitPicker = UIPickerView()
    @IBOutlet weak var ingredientTable: UITableView!
    @IBOutlet weak var continueToInstructions : UIButton!
    @IBOutlet weak var moreIngredients: UIButton!
    
    var ingredientArray = [AnyObject]()
    var count : Int = 3
    var unitsArray = [AnyObject]()
    var selectedCell = NSIndexPath()
    
    //MARK:- VIEW Did Load
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let attrs = [ NSFontAttributeName : UIFont(name: "Helvetica", size: (DeviceType.IS_IPAD ? 17.0 : 15.0))!,
                      NSForegroundColorAttributeName : Colors.Apptextcolor,
                      NSUnderlineStyleAttributeName : 1]
        let  attributedString1 = NSMutableAttributedString(string:"Add More Ingredients", attributes:attrs)
        moreIngredients.setAttributedTitle(attributedString1, forState: .Normal)

        unitsArray = NSUserDefaults.standardUserDefaults().objectForKey(Constants.UNITS) as! Array<AnyObject>

        if DeviceType.IS_IPHONE_4_OR_LESS {
            unitPicker = UIPickerView.init(frame: CGRectMake(0, ScreenSize.SCREEN_HEIGHT - 150, ScreenSize.SCREEN_WIDTH, 150))
        }
        else if DeviceType.IS_IPHONE_5 {
            unitPicker = UIPickerView.init(frame: CGRectMake(0, ScreenSize.SCREEN_HEIGHT - 175, ScreenSize.SCREEN_WIDTH, 175))
        }
        else {
            unitPicker = UIPickerView.init(frame: CGRectMake(0, ScreenSize.SCREEN_HEIGHT - 200, ScreenSize.SCREEN_WIDTH, 200))
        }
        
        unitPicker.dataSource = self
        unitPicker.delegate = self
        unitPicker.backgroundColor = Colors.appThemeColor
        unitPicker.tintColor = UIColor.whiteColor()
        
    }
    
    //MARK:- ADD More Pressed
    @IBAction func addMorePressed(sender : AnyObject)
    {
        count += 1
        ingredientTable.reloadData()
    }
    
    // MARK:- TABLEVIEW
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if DeviceType.IS_IPAD {
            return 135
        }
        return 100
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if tableView == ingredientTable {
        
            return count
        }
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        let cell = ingredientTable.dequeueReusableCellWithIdentifier("IngredientCell", forIndexPath: indexPath) as! IngredientCell
        cell.contentView.layer.cornerRadius = 10.0
        cell.contentView.clipsToBounds = true
        
        if ingredientArray.count > 0 && indexPath.section < ingredientArray.count
        {
            let dict = ingredientArray[indexPath.section] as! Dictionary<String,AnyObject>
            cell.ingredientText.text = dict["name"] as? String;
            cell.selectUnitText.text = dict["unit"] as? String
            cell.quantityText.text = dict["quantity"] as? String;
        }
        
        cell.selectUnitText.inputView = unitPicker
        cell.selectUnitText.delegate = self
        

        
        cell.selectUnitText.attributedPlaceholder = NSAttributedString(string: "Select Unit",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: "HelveticaNeue", size: (DeviceType.IS_IPAD ? 17.0 : 15))!])
        
        cell.ingredientText.attributedPlaceholder = NSAttributedString(string: "Ingredient",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: "HelveticaNeue", size: (DeviceType.IS_IPAD ? 17.0 : 13.5))!])
        cell.quantityText.attributedPlaceholder = NSAttributedString(string: "Quantity",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName :  UIFont(name: "HelveticaNeue", size: (DeviceType.IS_IPAD ? 17.0 : 13.5))!])
        
        
        if cell.ingredientText.text == "" {
            cell.ingredentLabel.hidden = true
        }
        if cell.quantityText.text == ""{
            cell.quantityLabel.hidden = true
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            
            if count > 1{
                count -= 1
                
                if ingredientArray.count > 1 {
                    ingredientArray.removeAtIndex(indexPath.section)
                }
                
                ingredientTable.beginUpdates()
                let indexSet = NSMutableIndexSet()
                indexSet.addIndex(indexPath.section)
                ingredientTable.deleteSections(indexSet, withRowAnimation: UITableViewRowAnimation.Automatic)
                ingredientTable.reloadData()
                ingredientTable.endUpdates()

            }
        }
    }
    
    // MARK:- TextField
    func textFieldDidBeginEditing(textField: UITextField) {
        
        let cell = (textField.superview)!.superview as! IngredientCell
        let indexPa = ingredientTable.indexPathForCell(cell)

        if textField == cell.ingredientText {
            self.ACTIVATETextField(cell.ingredientText, textLabel: cell.ingredentLabel, line: cell.ingredientLine, height: cell.ingredientLineHeight)
        }
        if textField == cell.quantityText {
            self.ACTIVATETextField(cell.quantityText, textLabel: cell.quantityLabel, line: cell.quantityLine, height: cell.quantityLineHeight)
        }
        
        if textField == cell.selectUnitText {
            cell.ingredientText.resignFirstResponder()
            cell.quantityText.resignFirstResponder()
            ingredientTable.scrollToRowAtIndexPath(ingredientTable.indexPathForCell(cell)!, atScrollPosition: .Top, animated: true)
            selectedCell = indexPa!
            unitPicker.hidden = false
            self.view.bringSubviewToFront(unitPicker)
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        let cell = (textField.superview)!.superview as! IngredientCell
        let indexPa = ingredientTable.indexPathForCell(cell)
        let index = indexPa?.section
        
        if textField == cell.ingredientText {
            self.DEACTIVATETextField(cell.ingredientText, textLabel: cell.ingredentLabel, line: cell.ingredientLine, height: cell.ingredientLineHeight)
        }
        if textField == cell.quantityText {
            self.DEACTIVATETextField(cell.quantityText, textLabel: cell.quantityLabel, line: cell.quantityLine, height: cell.quantityLineHeight)
        }
        
        self.updateCellInformation(cell ,indexp: indexPa!,index: index!)
    }
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        let cell = (textField.superview)!.superview as! IngredientCell
        if textField == cell.quantityText
        {
            if (range.location > 1)
            {
                return false
            }
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        let cell = (textField.superview)!.superview as! IngredientCell

        if textField == cell.selectUnitText {
            unitPicker.hidden = true
        }
        return true
    }


    func ACTIVATETextField(textf : UITextField,textLabel : UILabel , line: UILabel, height : NSLayoutConstraint)
    {
        dispatch_async(dispatch_get_main_queue())
        {
            UIView.animateWithDuration(1.0, delay: 0, options: UIViewAnimationOptions.TransitionFlipFromBottom, animations: {
                
                textLabel.hidden = false
                textf.attributedPlaceholder = NSAttributedString(string: "")
                Helper.activatetextField(line,height: height)
                
                }, completion: { (true) in
                    textLabel.hidden = false
            })
        }
    }
    
    
    func DEACTIVATETextField(textf : UITextField,textLabel : UILabel , line: UILabel, height : NSLayoutConstraint)
    {
        if textf.text == ""
        {
            dispatch_async(dispatch_get_main_queue())
            {
                UIView.animateWithDuration(1.0, animations: {
                    textLabel.hidden = true
                    
                    textf.attributedPlaceholder = NSAttributedString(string: textLabel.text! ,  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName :UIFont(name: "Helvetica", size: (DeviceType.IS_IPAD ? 17.0 : 15))!])
                    }, completion: nil)
            }
        }
        dispatch_async(dispatch_get_main_queue())
        {
            Helper.deActivatetextField(line,height: height)
        }
    }
    
    //MARK:- Update Cell Info
    func updateCellInformation(cell : IngredientCell, indexp : NSIndexPath, index : Int)
    {
        let newDict = NSMutableDictionary ()
        newDict.setObject(cell.ingredientText.text!, forKey: "name")
        newDict.setObject(cell.selectUnitText.text!, forKey: "unit")
        
        for  item  in unitsArray {
            let dict = item as! Dictionary<String,AnyObject>
            if dict["title"] as! String ==  cell.selectUnitText.text!{
                newDict.setObject(dict["id"] as! String, forKey: "unitTypeId")
            }
        }
        
        newDict.setObject(cell.quantityText.text!, forKey: "quantity")

        if ingredientArray.count > index {
           
            ingredientArray.removeAtIndex(index)
            ingredientArray.insert(newDict, atIndex: index)
        }
        else
        {
            ingredientArray.append(newDict)
        }
        
    }
    

    //MARK:- PICKERVIEW
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return unitsArray.count + 1
    }
    
    func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 27
    }
    
    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        if row == 0 {
            let str = NSAttributedString(string: "Select Unit",  attributes: [NSForegroundColorAttributeName : UIColor.whiteColor(), NSFontAttributeName : UIFont(name: "Helvetica", size: (DeviceType.IS_IPAD ? 17.0 : 14))!])
            return str
        }
        else
        {
            let dict = unitsArray[row - 1] as! Dictionary<String,AnyObject>
            let str = dict["title"] as! String
            
            let Attrstr = NSAttributedString(string: str.lowercaseString,  attributes: [NSForegroundColorAttributeName : UIColor.whiteColor(), NSFontAttributeName : UIFont(name: "Helvetica", size: (DeviceType.IS_IPAD ? 17.0 : 14))!])
            return Attrstr
        }
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let cell = self.ingredientTable.cellForRowAtIndexPath(selectedCell) as! IngredientCell
        
        if row == 0
        {
            cell.selectUnitText.attributedPlaceholder = NSAttributedString(string: "Select Unit",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: "Helvetica", size: (DeviceType.IS_IPAD ? 18.0 : 15))!])

            cell.selectUnitText.text = ""
        }
        else
        {
            let dict = unitsArray[row - 1] as! Dictionary<String,AnyObject>
            let str = dict["title"] as! String
            cell.selectUnitText.text = str.lowercaseString
        }
    }
    
    
}
