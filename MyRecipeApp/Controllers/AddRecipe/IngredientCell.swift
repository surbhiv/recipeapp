//
//  IngredientCell.swift
//  MyRecipeApp
//
//  Created by Surbhi on 19/11/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class IngredientCell: UITableViewCell {

    @IBOutlet weak var ingredentLabel: UILabel!
    @IBOutlet weak var ingredientText: UITextField!
    @IBOutlet weak var ingredientLine: UILabel!
    @IBOutlet weak var ingredientLineHeight: NSLayoutConstraint!
    
    @IBOutlet weak var selectUnitText: UITextField!
    
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var quantityText: UITextField!
    @IBOutlet weak var quantityLine: UILabel!
    @IBOutlet weak var quantityLineHeight: NSLayoutConstraint!
    
}
