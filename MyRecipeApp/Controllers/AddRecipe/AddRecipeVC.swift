//
//  AddRecipeVC.swift
//  MyRecipeApp
//
//  Created by Surbhi on 18/11/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit
import Firebase

class AddRecipeVC: UIViewController, ENSideMenuDelegate, UIScrollViewDelegate {

    @IBOutlet weak var screen1Label: UILabel!
    @IBOutlet weak var screen2Label: UILabel!
    @IBOutlet weak var screen3Label: UILabel!
    @IBOutlet weak var screen4Label: UILabel!
    
    @IBOutlet weak var hedingLabel: UILabel!
    @IBOutlet weak var detailScroll: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var contentViewWidth: NSLayoutConstraint!
    
    var ControllerArray = [AnyObject]()
    var headerArray = ["BASIC","INGREDIENT","INSTRUCTION","FINAL"]
    
    var basicDetailView = AddRecipeDetailView()
    var ingredient = IngredientView()
    var instruction = InstructionView()
    var final = UploadRecipeView()
    
    var finalDict = Dictionary<String,AnyObject>()
    var AddRecipeDict = Dictionary<String,AnyObject>()
    var recipeName = String()
    var cookingTime = String()
    var servedpeople = String()
    var mealtype = String()
    var level = String()
    var microwve = String()
    var ingredientJson = String()
    var instructionJson = String()
    var mwo_TopModelCatId = Int()

    //MARK:- START
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.sideMenuController()?.sideMenu?.delegate = self
        self.navigationController?.navigationBarHidden = true
        
        if Constants.appDelegate.justLoggedIn == true {
            let dict = NSUserDefaults.standardUserDefaults().objectForKey(Constants.PROFILE) as? NSDictionary
            let nameStr = dict![Constants.NAME] as! String
            
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"Culineria",
                "message":"Hi \(nameStr), Welcome to Culineria!\nWe look forward to our culinary journey together."])
            Constants.appDelegate.justLoggedIn = false
        }
        
        dispatch_async(dispatch_get_main_queue()) {
            self.setUpScrollView()
            self.screen1Label.backgroundColor = Colors.appNavigationColor
            self.hedingLabel.text = "Fill details about your recipe"
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK:- SIDE MENU ACTIONS
    @IBAction func toggleSideMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        backItem.tintColor = UIColor.lightGrayColor()
        navigationItem.backBarButtonItem = backItem
    }
    
    
    // MARK: -  Main Scrollview
    func setUpScrollView()
    {
        for arrayIndex in headerArray
        {
            if (arrayIndex == "BASIC")
            {
                basicDetailView = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("AddRecipeDetailView") as! AddRecipeDetailView
                ControllerArray.insert(basicDetailView, atIndex: 0)
            }
            
            if (arrayIndex == "INGREDIENT")
            {
                ingredient = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("IngredientView") as! IngredientView
                ControllerArray.insert(ingredient, atIndex: 1)
            }
            
            if (arrayIndex == "INSTRUCTION")
            {
                instruction = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("InstructionView") as! InstructionView
                ControllerArray.insert(instruction, atIndex: 2)
            }
            
            if (arrayIndex == "FINAL")
            {
                final = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("UploadRecipeView") as! UploadRecipeView
                
                ControllerArray.insert(final, atIndex: 3)
            }
        }
        setViewControllers(ControllerArray, animated: false)
    }
    
    //  Add and remove view controllers
    
    func setViewControllers(viewControllers : NSArray, animated : Bool)
    {
        if self.childViewControllers.count > 0
        {
            for vC in self.childViewControllers
            {
                vC.willMoveToParentViewController(nil)
                vC.removeFromParentViewController()
            }
        }
        
        for vC in viewControllers
        {
            
            self.addChildViewController(vC as! UIViewController)
            vC.didMoveToParentViewController(self)
        }
        //TODO animations
        if ((detailScroll) != nil)
        {
            reloadPages()
        }
    }
    
    func reloadPages()
    {
        var scrollContentCount = 0;
        
        for arrayIndex in 0 ..< headerArray.count
        {
            // set scorllview properties
            var frame : CGRect = CGRectMake(0, 0, 0, 0)
            frame.origin.x = ScreenSize.SCREEN_WIDTH * CGFloat(arrayIndex)
            frame.origin.y = 0;
            frame.size = ScreenSize.SCREEN.size
            
            detailScroll.pagingEnabled = true
            let contentWidth : CGFloat = ScreenSize.SCREEN_WIDTH * CGFloat(scrollContentCount+1)
            contentViewWidth.constant = contentWidth;
            
            let vC = self.childViewControllers[arrayIndex]
            
            addView(vC.view, contentView: contentView, frame: frame)
            
            if arrayIndex == 0
            {
                basicDetailView.continueToIngedient!.tag = 0
                basicDetailView.continueToIngedient!.addTarget(self, action: #selector(HeaderlabelSelected), forControlEvents: .TouchUpInside)
            }
            
            scrollContentCount = scrollContentCount + 1;
            //            previousScrollIndex=0;
        }
        
        detailScroll.contentSize = CGSizeMake(contentViewWidth.constant, detailScroll.frame.size.height)
        detailScroll.delegate = self
    }

    
    
    // adding views to scrollview
    func addView(view : UIView, contentView : UIView, frame : CGRect)
    {
        view.frame = frame
        view.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(view)
        
        contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: contentView, attribute:  NSLayoutAttribute.Leading, multiplier: 1.0, constant: frame.origin.x))
        contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute:  NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: frame.size.width))
        contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: contentView, attribute:  NSLayoutAttribute.Top, multiplier: 1.0, constant:0))
        
        var heightForSubview : CGFloat = ScreenSize.SCREEN_HEIGHT - 157.0
        if DeviceType.IS_IPAD {
            heightForSubview = ScreenSize.SCREEN_HEIGHT - 192.0
        }
        
        contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute:  NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: heightForSubview ))
        
        
    }
    
    
    

    // for every continue Pressed
    func HeaderlabelSelected(sender : AnyObject)
    {
        if sender.tag == 0
        {
            if basicDetailView.nameText.text! == "" {
                CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"Culineria","message":"Please enter recipe name"])
            }
            else if basicDetailView.timeText.text == "" {
                CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"Culineria","message":"Please enter cooking time"])
            }
            else if basicDetailView.timeText.text == "00" || basicDetailView.timeText.text == "0"{
                CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"Culineria","message":"Please enter valid cooking time"])
            }
            else if basicDetailView.servingText.text == ""{
                CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"Culineria","message":"Please enter number of people served"])
            }
            else if basicDetailView.servingText.text == "00" || basicDetailView.servingText.text == "0" || basicDetailView.servingText.text == "000"{
                CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"Culineria","message":"Please enter valid number of people served"])
            }
            else if basicDetailView.selectLevelButton.titleForState(.Normal) == "" || basicDetailView.selectLevelButton.titleForState(.Normal) == "Select Level"{
                CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"Culineria","message":"Please select recipe level"])
            }
            else if basicDetailView.selectMicrowaveButton.titleForState(.Normal) == "" || basicDetailView.selectMicrowaveButton.titleForState(.Normal) == "Select Microwave"{
                CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"Culineria","message":"Please select microwave model"])
            }
            else
            {
                recipeName = basicDetailView.nameText.text!
                cookingTime = basicDetailView.timeText.text!
                servedpeople = basicDetailView.servingText.text!
                if basicDetailView.nonVegButton.selected == true {
                    mealtype = "Non-Veg"
                }
                else
                {
                    mealtype = "Veg"
                }
                
                level = basicDetailView.leveSelected
                microwve = basicDetailView.microwaveString
//                mwo_TopModelCatId = basicDetailView.
                detailScroll.tag = sender.tag + 1
                let xAxis : CGFloat = ScreenSize.SCREEN_WIDTH * CGFloat(sender.tag + 1)
                detailScroll.setContentOffset(CGPointMake(xAxis, 0), animated: true)
                
                screen1Label.backgroundColor = UIColor.whiteColor()
                screen2Label.backgroundColor = Colors.appNavigationColor
                screen3Label.backgroundColor = UIColor.whiteColor()
                screen4Label.backgroundColor = UIColor.whiteColor()
                self.hedingLabel.text = "Fill Ingredients"
                ingredient.continueToInstructions.tag = 1
                ingredient.continueToInstructions.addTarget(self, action: #selector(HeaderlabelSelected), forControlEvents: .TouchUpInside)
            }
        }
        if sender.tag == 1
        {
            if ingredient.ingredientArray.count == 0 {
                CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"Culineria","message":"Please add atleast one ingredient"])
            }
            else
            {
                var complete : Bool = true
                for item in ingredient.ingredientArray
                {
                    let dict = item as! Dictionary<String,AnyObject>
                    
                    if dict["name"] as? String == "" || dict["name"] as? String == nil {
                        CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"Culineria","message":"Complete details"])
                        complete = false
                        break
                    }
                    else if dict["unit"] as? String == "" || dict["unit"] as? String == nil || dict["unit"] as? String == "Select Unit" {
                        CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"Culineria","message":"Complete details"])
                        complete = false
                        break

                    }
                    else if dict["quantity"] as? String == "" || dict["quantity"] as? String == nil {
                        CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"Culineria","message":"Complete details"])
                        complete = false
                        break

                    }
                }
                
                
                if complete == true
                {
                    ingredientJson = String.init(self.convertArrayToJsonString(ingredient.ingredientArray))
                    detailScroll.tag = sender.tag + 1
                    let xAxis : CGFloat = ScreenSize.SCREEN_WIDTH * CGFloat(sender.tag + 1)
                    detailScroll.setContentOffset(CGPointMake(xAxis, 0), animated: true)
                    
                    screen1Label.backgroundColor = UIColor.whiteColor()
                    screen2Label.backgroundColor = UIColor.whiteColor()
                    screen3Label.backgroundColor = Colors.appNavigationColor
                    screen4Label.backgroundColor = UIColor.whiteColor()
                    self.hedingLabel.text = "Fill Instructions"
                    
                    instruction.continueToUpload.tag = 2
                    instruction.continueToUpload.addTarget(self, action: #selector(HeaderlabelSelected), forControlEvents: .TouchUpInside)
                }
            }
        }
        if sender.tag == 2
        {
            if instruction.instructionArray.count == 0 {
                CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"Culineria","message":"Please add atleast one method"])
            }
            else
            {
                instructionJson = String.init(self.convertArrayToJsonString(instruction.instructionDict))

                detailScroll.tag = sender.tag + 1
                let xAxis : CGFloat = ScreenSize.SCREEN_WIDTH * CGFloat(sender.tag + 1)
                detailScroll.setContentOffset(CGPointMake(xAxis, 0), animated: true)
                
                screen1Label.backgroundColor = UIColor.whiteColor()
                screen2Label.backgroundColor = UIColor.whiteColor()
                screen3Label.backgroundColor = UIColor.whiteColor()
                screen4Label.backgroundColor = Colors.appNavigationColor
                self.hedingLabel.text = "Fill recipe notes"
                
                final.continueUploadingButton.tag = 3
                final.continueUploadingButton.addTarget(self, action: #selector(HeaderlabelSelected), forControlEvents: .TouchUpInside)
            }
        }
        if sender.tag == 3
        {
            
            if final.imageSelected.image == nil  || final.imageSelected.image == "" {
                
                CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"Culineria","message":"Please upload recipe image"])
            }
            else if final.isTermsAccepted == false {
                
                CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"Culineria","message":"Please accept terms & conditions"])
            }
            else {
                
                let imgDat = UIImagePNGRepresentation(final.imageDataIMG)! as NSData
                let imageSize: Int = imgDat.length
                
                if imageSize/1024 > 1000 {
                    // show alert
                    CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"Culineria","message":"Image size is larger than allowed. Please select another image!"])
                }
                else
                {
                    let imgData = final.imageData
                    let imageData = imgData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.EncodingEndLineWithCarriageReturn)
                    var is_public = String()
                    
                    if final.ispublicShare == false
                    {
                        is_public = "0"
                    }
                    else
                    {
                        is_public = "1"
                    }
                    
                    let notesSTR = final.notestext.text!
                    
                    let param = "is_public=\(is_public)&method=\(instructionJson)&ingredients=\(ingredientJson)&notes=\(notesSTR)&created_by=\(NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID)!)&cooking_time=\(cookingTime)&servings=\(servedpeople)&type=\(mealtype)&level_id=\(level)&models=\(microwve)&name=\(recipeName)&image_1=\(imageData)"//&mwo_top_category_id=\()
                    self.CallMethodToUploadRecipe(param)

                }
            }
        }
    }
    
    //MARK:- API METHOD
    func CallMethodToUploadRecipe(parameter: String)
    {
        if Reachability.isConnectedToNetwork()
        {
            let method = "recipes/create"
            Constants.appDelegate.startIndicator()

            Server.postRequestWithURL(Constants.BASEURL+method, paramString: parameter) { (response) in

                Constants.appDelegate.stopIndicator()
                if response.count == 0{
                    dispatch_async(dispatch_get_main_queue()){
                        let mo = self.sideMenuController()?.sideMenu?.menuViewController as! MenuTableView
                        mo.tableView(mo.tableView, didSelectRowAtIndexPath: NSIndexPath.init(forRow: 5, inSection: 0))
                        mo.toggleSideMenuView()
                        mo.tableView.selectRowAtIndexPath(NSIndexPath(forRow: 5, inSection: 0), animated: false, scrollPosition: .Middle)
                    }
                }
                else{
                    Constants.appDelegate.stopIndicator()
                    if response[Constants.STATE] as! Int == 1
                    {
                        self.callAddRecipeAnalyticMethod()
                        
                        dispatch_async(dispatch_get_main_queue())
                        {
                            let alert = UIAlertController.init(title: "Culineria", message: response["message"]! as? String, preferredStyle: UIAlertControllerStyle.Alert)
                            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                                
                                let mo = self.sideMenuController()?.sideMenu?.menuViewController as! MenuTableView
                                mo.tableView(mo.tableView, didSelectRowAtIndexPath: NSIndexPath.init(forRow: 5, inSection: 0))
                                mo.toggleSideMenuView()
                                mo.tableView.selectRowAtIndexPath(NSIndexPath(forRow: 5, inSection: 0), animated: false, scrollPosition: .Middle)
                            }))
                            self.presentViewController(alert, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        let mess = response["message"] as! String
                        CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"", "message":mess])
                    }

                }
            }
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }

    }
    
    //MARK:- ARRAY TO JSON CONVERSION
    func convertArrayToJsonString(arr : AnyObject) -> NSString
    {
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(arr, options: .PrettyPrinted)
            let string = NSString(data: jsonData, encoding: NSUTF8StringEncoding)
            return string!
            // use jsonData
        } catch {
            // report error
            
            return ""
        }
    }

    //MARK:- ANALYTICS
    func callAddRecipeAnalyticMethod()
    {
        FIRAnalytics.logEventWithName(kFIREventSelectContent, parameters: [
            kFIRParameterItemID:"5",
            kFIRParameterItemName:"Add Recipe",
            kFIRParameterContentType:"Recipe Added Successfully"
            ])
    }
    
}
