//
//  InstructionView.swift
//  MyRecipeApp
//
//  Created by Surbhi on 18/11/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class InstructionView: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate
{
    
    @IBOutlet weak var instructionTable: UITableView!
    var instructionArray = [AnyObject]()
    var instructionDict = NSMutableDictionary()

    var count : Int = 3

    
    
    @IBOutlet weak var continueToUpload : UIButton!
    @IBOutlet weak var moreIngredients: UIButton!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        let attrs = [ NSFontAttributeName : UIFont(name: "Helvetica", size: (DeviceType.IS_IPAD ? 17.0 : 15.0))!,
                      NSForegroundColorAttributeName : Colors.Apptextcolor,
                      NSUnderlineStyleAttributeName : 1]
        let  attributedString1 = NSMutableAttributedString(string:"Add More Instructions", attributes:attrs)
        moreIngredients.setAttributedTitle(attributedString1, forState: .Normal)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    
    // MARK:- TABLEVIEW
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("InstructionCell", forIndexPath: indexPath) as! InstructionCell
        cell.stepLabel.text = "Step" + String(indexPath.section + 1)
        
        if instructionDict.count > 0 && instructionDict.count > indexPath.section + 1
        {
            let indexStr = String(indexPath.section + 1)
            let str = instructionDict[indexStr] as! String
            
            if (str == "")
            {
            }
            else
            {
                cell.methodText.text = str;
            }
        }
        
        
        
        cell.methodText.attributedPlaceholder = NSAttributedString(string: "Method",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: "Helvetica", size: (DeviceType.IS_IPAD ? 16.5 : 13.5))!])
        
        if cell.methodText.text == "" {
            cell.stepLabel.hidden = true
        }

        return cell
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            
            if count > 1{
                count -= 1
                
                if instructionArray.count > 1 {
                    instructionArray.removeAtIndex(indexPath.section)
                }
                
                instructionTable.beginUpdates()
                let indexSet = NSMutableIndexSet()
                indexSet.addIndex(indexPath.section)
                instructionTable.deleteSections(indexSet, withRowAnimation: UITableViewRowAnimation.Automatic)
                instructionTable.reloadData()
                instructionTable.endUpdates()
                
            }
        }
    }

    @IBAction func addMorePressed(sender : AnyObject)
    {
        count += 1
        instructionTable.reloadData()
    }
    
    // MARK:- TextField
    func textFieldDidBeginEditing(textField: UITextField) {
        
        let cell = (textField.superview)!.superview as! InstructionCell
        
        if textField == cell.methodText {
            self.ACTIVATETextField(cell.methodText, textLabel: cell.stepLabel, line: cell.methodLine, height: cell.methodLineHeight)
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        let cell = (textField.superview)!.superview as! InstructionCell
        let indexPa = instructionTable.indexPathForCell(cell)
        let index = indexPa?.section
        
        if textField == cell.methodText {
            self.DEACTIVATETextField(cell.methodText, textLabel: cell.stepLabel, line: cell.methodLine, height: cell.methodLineHeight)
        }
        
        self.updateCellInformation(cell ,indexp: indexPa!,index: index!)
    }
    
    
    func ACTIVATETextField(textf : UITextField,textLabel : UILabel , line: UILabel, height : NSLayoutConstraint)
    {
        dispatch_async(dispatch_get_main_queue())
        {
            UIView.animateWithDuration(1.0, delay: 0, options: UIViewAnimationOptions.TransitionFlipFromBottom, animations: {
                
                textLabel.hidden = false
                textf.attributedPlaceholder = NSAttributedString(string: "")
                Helper.activatetextField(line,height: height)
                
                }, completion: { (true) in
                    textLabel.hidden = false
            })
        }
    }
    
    
    func DEACTIVATETextField(textf : UITextField,textLabel : UILabel , line: UILabel, height : NSLayoutConstraint)
    {
        if textf.text == ""
        {
            dispatch_async(dispatch_get_main_queue())
            {
                UIView.animateWithDuration(1.0, animations: {
                    textLabel.hidden = true
                    
                    textf.attributedPlaceholder = NSAttributedString(string: textLabel.text! ,  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: "Helvetica", size: (DeviceType.IS_IPAD ? 16.5 : 13.5))!])
                    }, completion: nil)
            }
        }
        dispatch_async(dispatch_get_main_queue())
        {
            Helper.deActivatetextField(line,height: height)
        }
    }
    
    //MARK:- Update Cell Info
    func updateCellInformation(cell : InstructionCell, indexp : NSIndexPath, index : Int)
    {
        let newDict = NSMutableDictionary ()
        
        if cell.methodText.text == "" || cell.methodText.text == " " {
            
            if instructionArray.count > index
            {
                instructionArray.removeAtIndex(index)
            }
        }
        else
        {
            newDict.setObject(cell.methodText.text!, forKey: String(index + 1))
            
            instructionDict.setObject(cell.methodText.text!, forKey: String(index + 1))
            
            if instructionArray.count > index {
                
                instructionArray.removeAtIndex(index)
                instructionArray.insert(newDict, atIndex: index)
            }
            else
            {
                instructionArray.append(newDict)
            }
        }
    }

}
