//
//  FavVC.swift
//  MyRecipeApp
//
//  Created by Surbhi on 17/11/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit    

class FavVC: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, ENSideMenuDelegate
{
    //MARK:- IBOutlets
    @IBOutlet weak var favCollection: UICollectionView!
    @IBOutlet weak var noCount: UILabel!

    //MARK:- Global Variables
    var favArray = [AnyObject]()

    //MARK:- Start
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getMyFavRecipes()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Side Menu
    
    @IBAction func toggleSideMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        backItem.tintColor = UIColor.lightGrayColor()
        navigationItem.backBarButtonItem = backItem
    }
    
    //MARK:- LOAD DATA
    func getMyFavRecipes()
    {
        if Reachability.isConnectedToNetwork()
        {
            Constants.appDelegate.startIndicator()
            
            let method = "users/my_recipes"
            let paramStr = "user_id=\(NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID)!)&favourite=1"

            Server.postRequestWithURL(Constants.BASEURL+method, paramString: paramStr, completionHandler: { (response) in
                

                if response[Constants.STATE] as! Int == 1
                {
                    self.favArray = response["data"] as! Array<AnyObject>
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        self.favCollection.hidden = false
                        self.noCount.hidden = true
                        self.favCollection.reloadData()
                    }
                    Constants.appDelegate.stopIndicator()

                }
                else
                {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.favCollection.hidden = true
                        self.noCount.hidden = false
                    }

                    Constants.appDelegate.stopIndicator()

                    CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"",
                        "message":response["message"]!])
                }

            })
        }
        else
        {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
            dispatch_async(dispatch_get_main_queue()) {
                self.favCollection.hidden = true
                self.noCount.hidden = false
            }
        }
    }
    
    //MARK:- COllection View
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        if DeviceType.IS_IPAD{
            return CGSizeMake((collectionView.bounds.size.width - 20)/2, (collectionView.bounds.size.width - 20)/2)
        }

        return CGSizeMake((collectionView.bounds.size.width - 25)/2, (collectionView.bounds.size.width - 25)/2)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        if DeviceType.IS_IPAD{
            return 10
        }
        return 5
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        if DeviceType.IS_IPAD{
            return 10
        }
        return 5

    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return favArray.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("FAVCell", forIndexPath: indexPath)
        cell.backgroundColor = UIColor.clearColor()
        cell.contentView.backgroundColor = UIColor.clearColor()
        
        
        let rec_Image = cell.contentView.viewWithTag(1091) as! UIImageView
        rec_Image.image = nil
        rec_Image.backgroundColor = UIColor.whiteColor()
        rec_Image.contentMode = UIViewContentMode.ScaleAspectFill

        let rating = cell.contentView.viewWithTag(1092) as! TPFloatRatingView // "rating"
        let nameLabel = cell.contentView.viewWithTag(1093) as! UILabel // "name"
        let deleteButton = cell.contentView.viewWithTag(1094) as! UIButton // "RemoveRecipe"
        deleteButton.addTarget(self, action: #selector(RemoveRecipe), forControlEvents: UIControlEvents.TouchUpInside)
        
        if favArray.count > 0 {
            let dict = favArray[indexPath.item]
            
            // MEAL IMAGE
            let imgStr =  dict["image_1"] as! String
            
            let myActivityIndicator = UIActivityIndicatorView()
            myActivityIndicator.center = cell.contentView.center
            myActivityIndicator.hidesWhenStopped = true
            myActivityIndicator.activityIndicatorViewStyle = .Gray
            rec_Image.addSubview(myActivityIndicator)
            myActivityIndicator.startAnimating()

            Helper.loadImageFromUrlWithIndicator(imgStr, view: rec_Image, indic: myActivityIndicator)

            
            // MEAL NAME
            nameLabel.text = dict["name"] as? String
            
            // MEAL RATING
            rating.backgroundColor = UIColor.clearColor()
            rating.emptySelectedImage = UIImage.init(named: "starEmpty")
            rating.fullSelectedImage = UIImage.init(named: "starFull")
            rating.contentMode = UIViewContentMode.ScaleAspectFill
            rating.editable = false
            rating.halfRatings = true;
            rating.floatRatings = true;
            rating.maxRating = 5
            rating.minRating = 0
            
            if dict["rating"] != nil
            {
                let rate  = Float((dict["rating"] as? NSNumber)!)
                rating.rating = CGFloat.init(rate)
            }
            else
            {
                rating.rating = 0.0
            }
        }
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        let destViewController  = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("RecipeDetail") as! RecipeDetailView
        let dict = favArray[indexPath.item]
        
        destViewController.recipeIDSTR = dict["id"] as! String
        destViewController.myRecipeStr = "1"
        destViewController.iscomingFromRecipe = false
        
        if self.navigationController == nil {
            self.presentViewController(destViewController, animated: true, completion: nil)
        }
        else
        {
            self.navigationController?.pushViewController(destViewController, animated: true)
        }
    }
    
    //MARK:- REMOVE MODEL & CHANGE MODEL
    
    @IBAction func RemoveRecipe(sender: AnyObject)
    {
        let cell = ((sender.superview)!!.superview)!.superview as! UICollectionViewCell
        let indexp = favCollection.indexPathForCell(cell)
        
        let dict = favArray[indexp!.item]
        let recipeIDSTR = dict["id"] as! String
        
        let alert = UIAlertController.init(title: "Culineria", message: "Are you sure you want to delete your recipe?", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction.init(title: "YES", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
            self.DeleteMethod(recipeIDSTR)
        }))
        alert.addAction(UIAlertAction.init(title: "NO", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
            self.dismissViewControllerAnimated(true, completion: nil)
        }))
        self.presentViewController(alert, animated: true, completion: nil)

    
    }

    func DeleteMethod(idstr :String)
    {
        if Reachability.isConnectedToNetwork()
        {
            Constants.appDelegate.startIndicator()
            
            let method = "recipes/remove_from_favourite"
            let paramStr = "user_id=\(NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID)!)&recipe_id=\(idstr)"
            
            
            Server.postRequestWithURL(Constants.BASEURL+method, paramString: paramStr, completionHandler: { (response) in
                
                Constants.appDelegate.stopIndicator()

                if response[Constants.STATE] as! Int == 1
                {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.getMyFavRecipes()
                        let alert = UIAlertController.init(title: "Culineria", message: response["message"]! as? String, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                            
                            self.dismissViewControllerAnimated(true, completion: nil)
                        }))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"",
                        "message":response["message"]!])
                }
            })
        }
        else
        {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
    }


}
