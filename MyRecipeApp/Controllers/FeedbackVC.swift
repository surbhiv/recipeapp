//
//  FeedbackVC.swift
//  MyRecipeApp
//
//  Created by Surbhi on 17/11/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class FeedbackVC: UIViewController, UITextFieldDelegate, UITextViewDelegate {

    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var nameLine: UILabel!
    @IBOutlet weak var nameLineHeight: NSLayoutConstraint!

    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var mobileText: UITextField!
    @IBOutlet weak var mobileLine: UILabel!
    @IBOutlet weak var mobileLineHeight: NSLayoutConstraint!

    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var emailLine: UILabel!
    @IBOutlet weak var emailLineHeight: NSLayoutConstraint!

    @IBOutlet weak var feedbackLabel: UILabel!
    @IBOutlet weak var feedBackText: UITextView!
    @IBOutlet weak var FeedBackLine: UILabel!
    @IBOutlet weak var FeedBackHeight: NSLayoutConstraint!
    
    @IBOutlet weak var countLabel: UILabel!
    
    let font : CGFloat = (DeviceType.IS_IPAD ? 16.5 : 13.0)
    
    //MARK:- Start
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dict = NSUserDefaults.standardUserDefaults().objectForKey(Constants.PROFILE) as? NSDictionary
        let nameStr = dict![Constants.NAME] as! String
        let mobileStr = dict![Constants.MOB] as! String
        let emailStr = dict![Constants.EMAIL] as! String
        

        nameText.attributedPlaceholder = NSAttributedString(string: "Name*",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: "Helvetica", size: font)!])
        emailText.attributedPlaceholder = NSAttributedString(string: "Email id*",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: "Helvetica", size: font)!])
        mobileText.attributedPlaceholder = NSAttributedString(string: "Mobile No.*",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: "Helvetica", size: font)!])

        nameText.text = nameStr
        mobileText.text = mobileStr
        emailText.text = emailStr
        
        if nameText.text == "" {
            nameLabel.hidden = true
        }
        if mobileText.text == ""{
            mobileLabel.hidden = true
        }
        if emailLabel.text == ""{
            emailLabel.hidden = true
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Side Menu
    @IBAction func toggleSideMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        backItem.tintColor = UIColor.lightGrayColor()
        navigationItem.backBarButtonItem = backItem
    }

    // MARK: TEXTFIELD DELEGATE
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if textField == nameText {
            ACTIVATETextField(nameText, textLabel: nameLabel, line: nameLine, height: nameLineHeight)
        }
        else if textField == emailText {
            ACTIVATETextField(emailText, textLabel: emailLabel, line: emailLine, height: emailLineHeight)
        }
        else if textField == mobileText {
            ACTIVATETextField(mobileText, textLabel: mobileLabel, line: mobileLine, height: mobileLineHeight)
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if textField == nameText {
            DEACTIVATETextField(nameText, textLabel: nameLabel, line: nameLine, height: nameLineHeight)
        }
        else if textField == emailText {
            DEACTIVATETextField(emailText, textLabel: emailLabel, line: emailLine, height: emailLineHeight)
        }
        else if textField == mobileText {
            DEACTIVATETextField(mobileText, textLabel: mobileLabel, line: mobileLine, height: mobileLineHeight)
        }
    }

    
    func ACTIVATETextField(textf : UITextField,textLabel : UILabel , line: UILabel, height : NSLayoutConstraint)
    {
        dispatch_async(dispatch_get_main_queue()) {
            UIView.animateWithDuration(1.0, delay: 0, options: UIViewAnimationOptions.TransitionFlipFromBottom, animations: {
                
                textLabel.hidden = false
                textf.attributedPlaceholder = NSAttributedString(string: "")
                Helper.activatetextField(line,height: height)
                
                }, completion: { (true) in
                    textLabel.hidden = false
            })
        }
    }
    
    
    func DEACTIVATETextField(textf : UITextField,textLabel : UILabel , line: UILabel, height : NSLayoutConstraint) {
        if textf.text == "" {
            dispatch_async(dispatch_get_main_queue()) {
                UIView.animateWithDuration(1.0, animations: {
                    textLabel.hidden = true
                    
                    Helper.deActivatetextField(line,height: height)
                    
                    textf.attributedPlaceholder = NSAttributedString(string: textLabel.text! ,  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: "Helvetica", size: self.font)!])
                    }, completion: nil)
            }
        }
    }
    

    
    //MARK:- TEXTVIEW DELEGATES
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).stringByReplacingCharactersInRange(range, withString: text)
        let numberOfChars = newText.characters.count // for Swift use count(newText)
        
        let remain = 200 - numberOfChars
        
        countLabel.text = "\(remain) / 200"
        
        if DeviceType.IS_IPAD{
            FeedBackHeight.constant = self.heightForView(newText)
            viewHeight.constant = 330 + self.heightForView(newText)
        }
        else{
            FeedBackHeight.constant = self.heightForView(newText)
            viewHeight.constant = 230 + self.heightForView(newText)
        }
        
        return numberOfChars < 200;
    }
    
    func heightForView(text:String) -> CGFloat {
        
        let font1 = (DeviceType.IS_IPAD ? UIFont(name: "Helvetica", size: 16.5) : UIFont(name: "Helvetica", size: 14.0))

        let label:UITextView = (DeviceType.IS_IPAD ? UITextView(frame: CGRectMake(0, 0, ScreenSize.SCREEN_WIDTH - 46, CGFloat.max)) : UITextView(frame: CGRectMake(0, 0, ScreenSize.SCREEN_WIDTH - 36, CGFloat.max)))
        label.font = font1
        label.text = text
        label.sizeToFit()
        
        if DeviceType.IS_IPAD{
            if label.frame.height < 30.0 {
                return 30.0
            }
        }
        else{
            if label.frame.height < 20.0 {
                return 20.0
            }
        }
        return label.frame.height
    }
    
    
    //MARK:- SAVE FEEDBACK
    @IBAction func savePressed(sender: AnyObject)
    {
        nameText.resignFirstResponder()
        mobileText.resignFirstResponder()
        emailText.resignFirstResponder()
        feedBackText.resignFirstResponder()

        if nameText.text! == "" {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"Culineria", "message":"Please enter your name !"])
        }
        else if mobileText.text! == ""{
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"Culineria", "message":"Please enter your mobile no. !"])
        }
        else if !Helper.isValidPhoneNumber(mobileText.text!){
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"Culineria", "message":"Please enter valid mobile no. !"])
        }

        else if emailText.text! == ""{
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"Culineria", "message":"Please enter your email !"])
        }
        else if !Helper.isValidEmail(emailText.text!){
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"Culineria", "message":"Please enter valid email !"])
        }
        else if feedBackText.text == ""{
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"Culineria", "message":"Please enter your feedback !"])
        }
        else {
            if Reachability.isConnectedToNetwork() {
                let method = "feedbacks/create"
                let userID = NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID)!
                let parameter = "name=\(nameText.text!)&email=\(emailText.text!)&mobile=\(mobileText.text!)&message=\(feedBackText.text!)&user_id=\(userID)"

                Constants.appDelegate.startIndicator()
                Server.postRequestWithURL(Constants.BASEURL+method, paramString: parameter) { (response) in
                    
                    Constants.appDelegate.stopIndicator()
                    
                    if response[Constants.STATE] as! Int == 1 {
                        
                        dispatch_async(dispatch_get_main_queue())
                        {
                            self.feedBackText.text = ""
                            
                            let alert = UIAlertController.init(title: "Culineria", message: response["message"]! as? String, preferredStyle: UIAlertControllerStyle.Alert)
                            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                                
                                
                                AppsFlyerTracker.sharedTracker().trackEvent("FeedBack", withValue: "Success") // APPSFlyer

                                
                                
                                self.dismissViewControllerAnimated(true, completion: nil)
                            }))
                            self.presentViewController(alert, animated: true, completion: nil)
                        }

                        
                    }
                    else {
                        let mess = response["message"] as! String
                        CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"", "message":mess])
                    }
                }
            }
            else {
                CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
            }
        }
    }
}
