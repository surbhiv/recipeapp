//
//  RecipeDetailView.swift
//  MyRecipeApp
//
//  Created by Surbhi on 25/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit
import ImageSlideshow
import SDWebImage
import Firebase

class RecipeDetailView: UIViewController ,UICollectionViewDataSource ,UICollectionViewDelegate {
    
    
    @IBOutlet weak var headerView: UIView!

    @IBOutlet weak var imageSlide: ImageSlideshow!
    @IBOutlet weak var recipeNameLabel: UILabel!
    @IBOutlet weak var ratingView: TPFloatRatingView!
  
    @IBOutlet weak var makingTimeButton: UIButton!
    @IBOutlet weak var serveSizeButton: UIButton!
    @IBOutlet weak var diffeicultyButton: UIButton!
    @IBOutlet weak var favButton: UIButton!
    @IBOutlet weak var rateButton: UIButton!

    @IBOutlet weak var collectionvw: UICollectionView!
    
    @IBOutlet weak var applicableModelLabel: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    
    
    
    @IBOutlet weak var headerScroll: UIScrollView!
    @IBOutlet weak var contentScroll: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var contentViewWidth: NSLayoutConstraint!
    let myActivityIndicator = UIActivityIndicatorView()

    var indicatorLabel : UILabel!
    
    var recipeIDSTR = String()
    var myRecipeStr = String()

    var recipeDetailDict = Dictionary<String,AnyObject>()
    var headerArray = ["INGREDIENTS","INSTRUCTIONS","NOTES"]
    var imageArray = [AnyObject]()
    var modelArray = [AnyObject]()
    var ControllerArray = [AnyObject]()
    
    var sdWebImageSource = [AnyObject]()
    var ingredView = RecipeDetailSubView()
    var instructView = RecipeDetailSubView()
    var notesView = RecipeDetailSubView()
    
    
    var iscomingFromRecipe : Bool = false
    
    // MARK: - Loading Application
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NSNotificationCenter.defaultCenter().addObserver( self, selector: #selector(UpdateRecipeDetails), name: "RecipeRatingUpdated", object: nil)
        
        imageSlide.backgroundColor = UIColor.whiteColor()
        imageSlide.slideshowInterval = 3.0
        imageSlide.pageControlPosition = PageControlPosition.InsideScrollView
        imageSlide.pageControl.currentPageIndicatorTintColor = UIColor.lightGrayColor();
        imageSlide.pageControl.pageIndicatorTintColor = UIColor.blackColor();
        
        imageSlide.contentScaleMode = UIViewContentMode.ScaleAspectFill
        imageSlide.zoomEnabled = false
        
        ratingView.backgroundColor = UIColor.clearColor()
        ratingView.emptySelectedImage = UIImage.init(named: "starEmpty")
        ratingView.fullSelectedImage = UIImage.init(named: "starFull")
        ratingView.contentMode = UIViewContentMode.ScaleAspectFill
        ratingView.editable = false
        ratingView.halfRatings = true;
        ratingView.floatRatings = true;
        ratingView.maxRating = 5
        ratingView.minRating = 0

        fetchRecipeDetail()
        
        FIRAnalytics.logEventWithName(kFIREventSelectContent, parameters: [
            kFIRParameterItemID:"6",
            kFIRParameterItemName:"Recipe Detail",
            kFIRParameterContentType:"Recipe Detail Page"
            ])
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
        if Constants.appDelegate.justLoggedIn == true {
            let dict = NSUserDefaults.standardUserDefaults().objectForKey(Constants.PROFILE) as? NSDictionary
            let nameStr = dict![Constants.NAME] as! String
            
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"Culineria",
                "message":"Hi \(nameStr), Welcome to Culineria!\nWe look forward to our culinary journey together."])
            Constants.appDelegate.justLoggedIn = false
        }
    }

    
    func fetchRecipeDetail()
    {
        if Reachability.isConnectedToNetwork()
        {
            Constants.appDelegate.startIndicator()
            let method = "recipes/"
            var paramStr = String()
            //top_category_id
            if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == "" || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil {
                paramStr = "id=\(recipeIDSTR)&user_id=&my_recipe="
            }
            else
            {
                paramStr = "id=\(recipeIDSTR)&user_id=\(NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID)!)&my_recipe=\(myRecipeStr)"
            }
            
            Server.postRequestWithURL(Constants.BASEURL+method, paramString: paramStr, completionHandler: { (response) in                
                if response[Constants.STATE] as! Int == 1
                {
                    let arr = response["data"] as! Array<AnyObject>
                    self.recipeDetailDict = arr.first as! Dictionary<String,AnyObject>
                    dispatch_async(dispatch_get_main_queue())
                    {
                        self.updateView()
                    }
                }
                else
                {
                    Constants.appDelegate.stopIndicator()
                    CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"",
                        "message":response["message"]!])
                }
            })
        }
        else
        {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }

    }
    
    func updateView() {
        
        if recipeDetailDict["images"]?.count >= 3 {
            sdWebImageSource = [SDWebImageSource.init(urlString: (recipeDetailDict["image_1"] as? String)!)!,
                                SDWebImageSource.init(urlString: (recipeDetailDict["image_2"] as? String)!)!,
                                SDWebImageSource.init(urlString: (recipeDetailDict["image_3"] as? String)!)!]
        }
        else if recipeDetailDict["images"]?.count == 2
        {
            sdWebImageSource = [SDWebImageSource.init(urlString: (recipeDetailDict["image_1"] as? String)!)!,
                                SDWebImageSource.init(urlString: (recipeDetailDict["image_2"] as? String)!)!]
        }
        else
        {
            sdWebImageSource = [SDWebImageSource.init(urlString: (recipeDetailDict["image_1"] as? String)!)!]
        }
        
        if myRecipeStr == "1" // approval_status //status // featured //  is_public //
        {
            ratingView.hidden = true
            rateButton.hidden = true
        }
        
        if recipeDetailDict["images"]?.count == 1 {
            imageSlide.pageControl.pageIndicatorTintColor = UIColor.clearColor();
        }
        recipeNameLabel.text = recipeDetailDict["name"] as? String
        
        if modelArray.count > 0 {
            modelArray.removeAll()
        }
        
        
        if iscomingFromRecipe == false  {
            modelArray = recipeDetailDict["models"] as! [AnyObject]
            applicableModelLabel.text = "Applicable Models"
        }
        else
        {
            var modelID = String()
            if NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED) == "" || NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED) == nil
            {
                modelID = ""
                applicableModelLabel.text = "Applicable Model"

                modelArray = recipeDetailDict["models"] as! [AnyObject]
            }
            else
            {
                applicableModelLabel.text = "Selected Model"

                modelID = NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED)!
                let arr = recipeDetailDict["models"] as! [AnyObject]
                
                for item in arr {
                    let dict = item as! Dictionary<String,AnyObject>
                    if  dict["id"] as! String == modelID {
                        modelArray.append(item)
                    }
                }
            }

        }

        pageControl.numberOfPages = modelArray.count
        if DeviceType.IS_IPAD {
            
            if modelArray.count % 2 == 0 {
                pageControl.numberOfPages = modelArray.count / 2
            }
            else {
                pageControl.numberOfPages = (modelArray.count / 2) + 1
            }
        }
        
        
        self.self.collectionvw.reloadData()
        self.imageSlide.setImageInputs(self.sdWebImageSource as! [InputSource] )

        
        
        if recipeDetailDict["rating"] == nil
        {
            ratingView.rating = 0.0
        }
        else
        {
            let WageConversion = recipeDetailDict["rating"]!
            let rate  = WageConversion.floatValue
            ratingView.rating = CGFloat.init(rate)
        }
        diffeicultyButton.setTitle(String(format:"  %@",(recipeDetailDict["level"] as? String)!), forState: UIControlState.Normal)

        let sizeServe = recipeDetailDict["servings"] as? String
        serveSizeButton.setTitle("  Serves "+sizeServe!, forState: UIControlState.Normal)
        
        let prepT = Int((recipeDetailDict["cooking_time"] as? String)!)
        if prepT < 60
        {
            let aStr = String(format: "  %d Min", prepT!)
            makingTimeButton.setTitle(aStr, forState: UIControlState.Normal)
        }
        else
        {
            let aStr = String(format: "  %d Hour", prepT!)
            makingTimeButton.setTitle(aStr, forState: UIControlState.Normal)
        }
        
        if recipeDetailDict["favourite"] == nil {
            
            favButton.selected = false
        }
        else {
            if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == "" || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil {
                
                favButton.selected = false
            }
            else {
                
                let Favouriteval = recipeDetailDict["favourite"]
                let isFavourite = String(Favouriteval!)
                
                if isFavourite == "0"
                {
                    favButton.selected = false
                }
                else
                {
                    favButton.selected = true
                }
            }
        }
        
        dispatch_async(dispatch_get_main_queue())
        {
            self.setHeaderScroll()
            self.setUpScrollView()
        }
        
        NSNotificationCenter.defaultCenter().postNotificationName("RatingViewCanBeShowm", object: UIApplication.sharedApplication())

    }
    
    //MARK:- NOTIFICATION received
    func UpdateRecipeDetails(noti : NSNotification)
    {
        self.fetchRecipeDetail()
    }

    //MARK: - BUTTON ACTIONS
    
    @IBAction func backButtonPressed(sender: AnyObject) {
        
        if self.navigationController == nil
        {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        else
        {
            self.navigationController?.navigationBarHidden = true
            self.navigationController?.popViewControllerAnimated(true)
        }
    }

    
    @IBAction func RateButtonPressed(sender: AnyObject) {
        
        
        if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == ""
        {
            let alert = UIAlertController.init(title: "Culineria", message: "Login to rate the recipe.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction.init(title: "LOGIN", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                
                let destinationController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("LoginView") as! LoginView
                Constants.appDelegate.isRateView = true
                self.navigationController?.presentViewController(destinationController, animated: true, completion: nil)
                
            }))
            alert.addAction(UIAlertAction.init(title: "CANCEL", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                self.dismissViewControllerAnimated(true, completion: nil)
            }))

            self.presentViewController(alert, animated: true, completion: nil)
        }
        else
        {
            if Reachability.isConnectedToNetwork() {
                
                let rate = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("RateRecipeView") as! RateRecipeView
                rate.recipeID = recipeDetailDict["id"] as! String
                rate.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext
                self.navigationController?.presentViewController(rate, animated: true, completion: nil)
            }
            else {
                CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
            }
        }
    }
    
    @IBAction func ShareButtonPressed(sender: AnyObject) {
        
        let instructionDict = recipeDetailDict["method"] as! Dictionary<String,AnyObject>
        var instructionString : String = ""
        let shareUrl = "https://itunes.apple.com/us/app/whirlpool-culineria/id1185192155?ls=1&mt=8"
        let myWebsite = NSURL.init(string: shareUrl)
        
        
        let sortedKeysAndValues = instructionDict.sort { $0.00 < $1.00 }
        for arrayIndex in 0 ..< sortedKeysAndValues.count
        {
            for item in instructionDict {
                var str = String()
                if item.0 == String(arrayIndex + 1) {
                    
                    str = "\n\(arrayIndex + 1). \(item.1)"
                }
                instructionString = instructionString + str
            }
        }
        
        let title = recipeDetailDict["name"] as? String

        // set up activity view controller
        let shareSheet = UIActivityViewController.init(activityItems: [title!,instructionString,myWebsite!], applicationActivities: nil)
        let excludeActivities = [UIActivityTypeAirDrop,UIActivityTypePrint,UIActivityTypeAssignToContact,UIActivityTypeSaveToCameraRoll,UIActivityTypeAddToReadingList]
        shareSheet.excludedActivityTypes = excludeActivities;

        shareSheet.popoverPresentationController?.sourceView = self.view
        shareSheet.setValue("Whirlpool Culineria", forKey: "subject")
        self.presentViewController(shareSheet, animated: true, completion: nil)
    }

    @IBAction func FavButtonPressed(sender: AnyObject) {
        
        
        if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == "" || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil
        {
            let alert = UIAlertController.init(title: "Culineria", message: "Login to add favourite.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction.init(title: "LOGIN", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                let mo = self.sideMenuController()?.sideMenu?.menuViewController as! MenuTableView
                mo.LoginPressed()
            }))
            alert.addAction(UIAlertAction.init(title: "CANCEL", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                self.dismissViewControllerAnimated(true, completion: nil)
            }))

            self.presentViewController(alert, animated: true, completion: nil)
        }
        else
        {
            if favButton.selected {
                favButton.selected = false
            }
            else
            {
                favButton.selected = true
            }
            updateFavouriteList()
        }
    }

    //MARK:- UPDATE FAVOURITE LIST
    func updateFavouriteList()
    {
        var method = String()

        if favButton.selected {
            method = "recipes/add_to_favourite"
        }
        else
        {
            method = "recipes/remove_from_favourite"

        }
        
        let param = "user_id=\(NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID)!)&recipe_id=\(recipeIDSTR))"
        
        if Reachability.isConnectedToNetwork() {
            
            Constants.appDelegate.startIndicator()
            Server.postRequestWithURL(Constants.BASEURL+method, paramString: param) { (response) in
                
                Constants.appDelegate.stopIndicator()
                if response[Constants.STATE] as! Int == 1
                {
                    
                    let alert = UIAlertController.init(title: "Culineria", message: response["message"]! as? String, preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                        
                        self.dismissViewControllerAnimated(true, completion: nil)
                        self.fetchRecipeDetail()
                    }))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                else
                {
                    if self.favButton.selected == true{
                        self.favButton.selected = false
                    }
                    else
                    {
                        self.favButton.selected = true
                    }
                    CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"","message":response["message"]!])
                }
            }
        }
        else
        {
            if favButton.selected {
                favButton.selected = false
            }
            else
            {
                favButton.selected = true
            }
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }

    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return modelArray.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("modelCell", forIndexPath: indexPath)
        cell.backgroundColor = UIColor.whiteColor()
        
        if DeviceType.IS_IPAD {
            cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, (collectionView.frame.width - 15)/2, 90)
        }
        else{
            cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, collectionView.frame.width - 10, 70)
        }
        
        
        let modelImage = cell.contentView.viewWithTag(1011) as? UIImageView
        let modelName = cell.contentView.viewWithTag(1012) as? UILabel
        let modelvolume = cell.contentView.viewWithTag(1013) as? UILabel

        modelName?.text = modelArray[indexPath.row]["title"] as? String
        modelvolume?.text = modelArray[indexPath.row]["capacity"] as? String
        
        modelImage!.image = nil
        let imgURL: String = (modelArray[indexPath.row]["image"] as? String)!
        
        myActivityIndicator.center = modelImage!.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.activityIndicatorViewStyle = .Gray
        modelImage!.addSubview(myActivityIndicator)
        myActivityIndicator.startAnimating()

        
        Helper.loadImageFromUrlWithIndicator(imgURL, view: modelImage!, indic: myActivityIndicator)

        let sze = pageControl.sizeForNumberOfPages(modelArray.count)
        
        if sze.width > ScreenSize.SCREEN_WIDTH {
            let rat = sze.width / ScreenSize.SCREEN_WIDTH
            if rat > 1
            {
                pageControl.transform = CGAffineTransformMakeScale(0.85, 0.85);
            }
            else if rat > 2{
                pageControl.transform = CGAffineTransformMakeScale(0.60, 0.60);
            }
            else if rat > 3{
                pageControl.transform = CGAffineTransformMakeScale(0.40, 0.40);
            }
            else if rat > 4{
                pageControl.transform = CGAffineTransformMakeScale(0.25, 0.25);
            }
        }
        pageControl.currentPage = indexPath.row
        if DeviceType.IS_IPAD {
            pageControl.currentPage = indexPath.row / 2
        }
        return cell
    }
    
    // MARK: - Label ScrollView
    
    func setHeaderScroll()
    {
        var scrollContentCount = 0;
        var frame : CGRect = CGRectMake(0, 0, 0, 0)
        var labelx : CGFloat = 0.0
        let width  : CGFloat = ScreenSize.SCREEN_WIDTH / 3
        let firstLabelWidth : CGFloat = ScreenSize.SCREEN_WIDTH / 3
        
        
        indicatorLabel = UILabel.init(frame: CGRectMake(labelx, 38, firstLabelWidth, 3))
        if DeviceType.IS_IPAD {
            indicatorLabel.frame = CGRectMake(labelx, 47, firstLabelWidth, 3);
        }

        indicatorLabel.backgroundColor = UIColor.whiteColor()
        indicatorLabel.text = "";
        headerScroll.addSubview(indicatorLabel)
        
        for arrayIndex in 0 ..< headerArray.count
        {
            frame.origin.x = labelx
            // set scorllview properties
            frame.origin.y = 0;
            frame.size.width=width;
            frame.size.height=40;

            if DeviceType.IS_IPAD {
                frame.size.height=50;
            }
            
            
            
            let accountLabelButton = UIButton.init(type: UIButtonType.Custom)
            accountLabelButton.addTarget(self, action:#selector(HeaderlabelSelected), forControlEvents: UIControlEvents.TouchUpInside)
            
            accountLabelButton.frame = frame;
            accountLabelButton.tag = arrayIndex;
            accountLabelButton.setAttributedTitle(NSAttributedString(string: headerArray[arrayIndex],  attributes: [NSFontAttributeName : UIFont.init(name: "HelveticaNeue-Medium", size: 14.5)!, NSForegroundColorAttributeName : UIColor.whiteColor()]), forState: UIControlState.Normal)
            
            if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS {
                accountLabelButton.setAttributedTitle(NSAttributedString(string: headerArray[arrayIndex],  attributes: [NSForegroundColorAttributeName : Colors.Lighttextcolor,NSFontAttributeName : UIFont.init(name: "HelveticaNeue-Medium", size: 13.5)!]), forState: UIControlState.Normal)
            }
            if DeviceType.IS_IPAD {
                accountLabelButton.setAttributedTitle(NSAttributedString(string: headerArray[arrayIndex],  attributes: [NSForegroundColorAttributeName : Colors.Lighttextcolor,NSFontAttributeName : UIFont.init(name: "HelveticaNeue-Medium", size: 18.5)!]), forState: UIControlState.Normal)

            }
            accountLabelButton.titleLabel?.textAlignment = NSTextAlignment.Center
            
            headerScroll.addSubview(accountLabelButton)
            
            if arrayIndex == 0
            {
                accountLabelButton.alpha=1.0;
            }
            else
            {
                accountLabelButton.alpha=0.7;
            }
            
            labelx=labelx+width;
            scrollContentCount = scrollContentCount + 1;
        }
        
        // set scroll content size
        headerScroll.contentSize = CGSizeMake(frame.width, 40)//CGSizeMake(frame.origin.x+frame.size.width, 40);
        if DeviceType.IS_IPAD {
            headerScroll.contentSize = CGSizeMake(frame.width, 50)
        }
        automaticallyAdjustsScrollViewInsets = false;
        contentScroll.delegate=self;
    }
    
    
    func HeaderlabelSelected(sender: UIButton)
    {
        contentScroll.tag = sender.tag
        
        var yAxi : CGFloat
        if DeviceType.IS_IPAD {
            yAxi = 47
        }
        else{
            yAxi = 38
        }
        
        let xAxis : CGFloat = ScreenSize.SCREEN_WIDTH * CGFloat(sender.tag)
        contentScroll.setContentOffset(CGPointMake(xAxis, 0), animated: true)
        
        let buttonArray = NSMutableArray()
        
        for view in headerScroll.subviews
        {
            if view.isKindOfClass(UIButton) {
                let labelButton = view as! UIButton
                buttonArray.addObject(labelButton)
                
                if labelButton.tag == sender.tag {
                    labelButton.alpha = 1.0
                }
                else
                {
                    labelButton.alpha = 0.7
                }
            }
        }
        
        let labelButton = buttonArray.objectAtIndex(sender.tag)
        var frame : CGRect = labelButton.frame
        frame.origin.y = yAxi
        frame.size.height = 3
        
        UIView.animateWithDuration(0.2, animations: { 
            if sender.tag == 0
            {
                self.indicatorLabel.frame =  CGRectMake(0,yAxi, ScreenSize.SCREEN_WIDTH/3.0, 3)
            }
            else
            {
                self.indicatorLabel.frame =  CGRectMake((ScreenSize.SCREEN_WIDTH/3.0) * CGFloat(sender.tag) ,yAxi, ScreenSize.SCREEN_WIDTH/3.0, 3)
            }
            
            self.headerScroll.scrollRectToVisible(self.indicatorLabel.frame, animated: false)

            }, completion: nil)
    }
    
    // MARK: -  Main Scrollview
    func setUpScrollView()
    {
        for arrayIndex in headerArray
        {
            if (arrayIndex == "INGREDIENTS")
            {
                ingredView = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("RecipeDetailSub") as! RecipeDetailSubView
                ingredView.isNotesView = false
                ingredView.isIngredientsTable = true
                ingredView.ingredientArray = recipeDetailDict["ingredients"] as! Array<AnyObject>
                ControllerArray.insert(ingredView, atIndex: 0)
            }
        
            if (arrayIndex == "INSTRUCTIONS")
            {
                instructView = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("RecipeDetailSub") as! RecipeDetailSubView
                instructView.isNotesView = false
                instructView.isIngredientsTable = false
                instructView.instructionDict = recipeDetailDict["method"] as! Dictionary<String,AnyObject>

                ControllerArray.insert(instructView, atIndex: 1)
            }
        
            if (arrayIndex == "NOTES")
            {
                notesView = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("RecipeDetailSub") as! RecipeDetailSubView
                notesView.isNotesView = true
                notesView.recipeIDSTR = self.recipeIDSTR
                
                
                if recipeDetailDict["notes"] as? String == "" || recipeDetailDict["notes"] as? String == nil {
                    notesView.notesString = ""
                }
                else
                {
                    notesView.notesString = recipeDetailDict["notes"] as! String
                }
                ControllerArray.insert(notesView, atIndex: 2)
            }
        }
        setViewControllers(ControllerArray, animated: false)
    }
    
    //  Add and remove view controllers
    
    func setViewControllers(viewControllers : NSArray, animated : Bool)
    {
        if self.childViewControllers.count > 0
        {
            for vC in self.childViewControllers
            {
                vC.willMoveToParentViewController(nil)
                vC.removeFromParentViewController()
            }
        }
        
        for vC in viewControllers
        {
            
            self.addChildViewController(vC as! UIViewController)
            vC.didMoveToParentViewController(self)
        }
        //TODO animations
        if ((contentScroll) != nil)
        {
            reloadPages()
        }
    }

    func reloadPages()
    {
        var scrollContentCount = 0;
        
        for arrayIndex in 0 ..< headerArray.count
        {
            // set scorllview properties
            var frame : CGRect = CGRectMake(0, 0, 0, 0)
            frame.origin.x = ScreenSize.SCREEN_WIDTH * CGFloat(arrayIndex)
            frame.origin.y = 0;
            frame.size = ScreenSize.SCREEN.size
            
            contentScroll.pagingEnabled = true
            let contentWidth : CGFloat = ScreenSize.SCREEN_WIDTH * CGFloat(scrollContentCount+1)
            contentViewWidth.constant = contentWidth;
            
            let vC = self.childViewControllers[arrayIndex]
            addView(vC.view, contentView: contentView, frame: frame)
            
            scrollContentCount = scrollContentCount + 1;
//            previousScrollIndex=0;
        }
        
        contentScroll.contentSize = CGSizeMake(contentViewWidth.constant, contentScroll.frame.size.height)
        contentScroll.delegate = self
        Constants.appDelegate.stopIndicator()
    }

    // adding views to scrollview
    func addView(view : UIView, contentView : UIView, frame : CGRect)
    {
        view.frame = frame
        view.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(view)
        
        contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: contentView, attribute:  NSLayoutAttribute.Leading, multiplier: 1.0, constant: frame.origin.x))
        contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute:  NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: frame.size.width))
        contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: contentView, attribute:  NSLayoutAttribute.Top, multiplier: 1.0, constant: 0))
        
        contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute:  NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: contentView.frame.size.height ))
    }
    
}

extension RecipeDetailView : UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if DeviceType.IS_IPAD{
            return CGSize(width: (ScreenSize.SCREEN_WIDTH - 15)/2,height: 90.0)
        }
        return CGSize(width: ScreenSize.SCREEN_WIDTH - 5, height: 70.0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
 }
