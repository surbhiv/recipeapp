//
//  RecipeDetailSubView.swift
//  MyRecipeApp
//
//  Created by Surbhi on 25/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class RecipeDetailSubView: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var notesView: UIView!
    
    @IBOutlet weak var notestextView: UITextView!
    @IBOutlet weak var addNotesButton: UIButton!
    
    var isNotesView : Bool?
    var isIngredientsTable : Bool?
    
    var ingredientArray = Array<AnyObject>()
    var instructionDict =  Dictionary<String,AnyObject>()
    var instructionArray = Array<AnyObject>()
    var recipeIDSTR = String()

    var notesString  = String()
    
    // MARK: - Loading Application
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(false)
        
        if isNotesView == true
        {
            notesView.hidden = false
            tableview.hidden = true
            
            notestextView.layer.borderColor = Colors.appThemeColor.CGColor
            notestextView.layer.borderWidth = 1.0
            notestextView.clipsToBounds = true
            
            notestextView.tintColor = Colors.Apptextcolor
            
            if notesString != "" {
                notestextView.text = notesString
            }
            
        }
        else
        {
            notesView.hidden = true
            tableview.hidden = false
            
            
            if isIngredientsTable == false 
            {
                let sortedKeysAndValues = instructionDict.sort { $0.00 < $1.00 }
                for arrayIndex in 0 ..< sortedKeysAndValues.count
                {
                    for item in instructionDict {
                        if item.0 == String(arrayIndex + 1) {
                            instructionArray.insert(item.1, atIndex: arrayIndex)
                        }
                    }
                }
                
                tableview.estimatedRowHeight = 50.0
                tableview.rowHeight = UITableViewAutomaticDimension
            }
            else
            {
                tableview.estimatedRowHeight = 45.0
                tableview.rowHeight = UITableViewAutomaticDimension
            }
            tableview .reloadData()
        }
    }
    
    // Mark: - TableView
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isIngredientsTable == true
        {
            return ingredientArray.count
        }
        else
        {
            return instructionArray.count
        }
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if isIngredientsTable == true {
            
            var cell = tableView.dequeueReusableCellWithIdentifier("IngredientTypeCell")
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "IngredientTypeCell")
            }
            cell?.backgroundColor = UIColor.clearColor()
            
            let ingredientName = cell?.contentView.viewWithTag(1021) as! UILabel
            ingredientName.text = ingredientArray[indexPath.row]["name"] as? String
            
            let ingredientquantity = cell?.contentView.viewWithTag(1022) as! UILabel
            let val = ingredientArray[indexPath.row]["quantity"] as! String
            let Name = ingredientArray[indexPath.row]["unit"] as! String
            
            if Name == "Number" {
                ingredientquantity.text =  val
            }
            else{
                ingredientquantity.text =  val + " " + Name.lowercaseString
            }
            
            tableView.estimatedRowHeight = 50
            tableView.rowHeight = UITableViewAutomaticDimension
            
            return cell!
        }
        else
        {
            var cell = tableView.dequeueReusableCellWithIdentifier("InstructionsCell")
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "IngredientTypeCell")
            }
            cell?.backgroundColor = UIColor.clearColor()

            let instructionLabel = cell?.contentView.viewWithTag(1024) as! UILabel
            instructionLabel.text = instructionArray[indexPath.row] as? String
            
            return cell!
        }
    }
    
    //MARK: - notes button added
    @IBAction func AddNotesPressed(sender: AnyObject)
    {
        if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == "" || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil
        {
            let alert = UIAlertController.init(title: "Culineria", message: "Please Login/Register to add notes.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction.init(title: "LOGIN", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in

                let destinationController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("LoginView") as! LoginView
                Constants.appDelegate.isRateView = true
                self.navigationController?.presentViewController(destinationController, animated: true, completion: nil)
                
            }))
            alert.addAction(UIAlertAction.init(title: "CANCEL", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                self.dismissViewControllerAnimated(true, completion: nil)
            }))
            
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else
        {
            if notestextView.text! != ""
            {
                self.notestextView.resignFirstResponder()
                AddNOTESmethod()
            }
            else
            {
                CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title": "Culineria", "message" : "Please enter notes."])
            }
        }
        
        
    }
    
    func AddNOTESmethod()
    {
        let method = "recipes/notes"
        let param = "user_id=\(NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID)!)&recipe_id=\(recipeIDSTR)&notes=\(notestextView.text!)"
        
        if Reachability.isConnectedToNetwork() {
         
            Constants.appDelegate.startIndicator()
            Server.postRequestWithURL(Constants.BASEURL+method, paramString: param) { (response) in
                
                Constants.appDelegate.stopIndicator()
                if response[Constants.STATE] as! Int == 1
                {
                    dispatch_async(dispatch_get_main_queue())
                    {
                        CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"","message":response["message"]!])
                    }
                }
                else
                {
                    CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"","message":response["message"]!])
                }
            }
        }
        else {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }

}
