//
//  DashBoardVC.swift
//  TestSwiftApp
//
//  Created by Surbhi on 21/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit
import SafariServices
import Firebase

class DashBoardVC: UIViewController, ENSideMenuDelegate, SFSafariViewControllerDelegate {
    
    @IBOutlet weak var addRecipeButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Constants.appDelegate.isSafariOpen == true {
            let urlString = "https://www.whirlpoolindia.com/termofuse"
            let svc = SFSafariViewController(URL: NSURL(string: urlString)!)
            svc.delegate = self
            self.presentViewController(svc, animated: false, completion: nil)
            Constants.appDelegate.isSafariOpen = false
        }
        else {
            
            if NSUserDefaults.standardUserDefaults().boolForKey("LoginRequiredError") == true
            {
                let mo = sideMenuController()?.sideMenu?.menuViewController as! MenuTableView
                mo.tableView(mo.tableView, didSelectRowAtIndexPath: NSIndexPath.init(forRow: 4, inSection: 0))
                toggleSideMenuView()
                mo.tableView.selectRowAtIndexPath(NSIndexPath(forRow: 4, inSection: 0), animated: false, scrollPosition: .Middle)
                NSUserDefaults.standardUserDefaults().setBool(false, forKey: "LoginRequiredError")
            }
            else
            {
                if Constants.appDelegate.justStarted == true {
                    
                    Constants.appDelegate.justStarted = false
                    
                    if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == "" || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil
                    {
                        CustomAlertController.showAlertOnController(self.parentViewController, withAlertType: Simple, andAttributes: ["title":"Culineria", "message":"Hi Guest, Welcome to Culineria!\nWe look forward to our culinary journey together."])
                    }
                    else {
                        
                        let dict = NSUserDefaults.standardUserDefaults().objectForKey(Constants.PROFILE) as? NSDictionary
                        let nameStr = dict![Constants.NAME] as! String

                        CustomAlertController.showAlertOnController(self.parentViewController, withAlertType: Simple, andAttributes: ["title":"Culineria", "message":"Hi \(nameStr), Welcome to Culineria!\nWe look forward to our culinary journey together."])
                    }
                }
                else if Constants.appDelegate.justLoggedIn == true
                {
                    Constants.appDelegate.justLoggedIn = false
                    let dict = NSUserDefaults.standardUserDefaults().objectForKey(Constants.PROFILE) as? NSDictionary
                    let nameStr = dict![Constants.NAME] as! String
                    
                    CustomAlertController.showAlertOnController(self.parentViewController, withAlertType: Simple, andAttributes: ["title":"Culineria", "message":"Hi \(nameStr), Welcome to Culineria!\nWe look forward to our culinary journey together."])
                }
            }
        }
        
        FIRAnalytics.logEventWithName(kFIREventSelectContent, parameters: [
            
            kFIRParameterItemID:"1",
            kFIRParameterItemName:"Dashboard",
            kFIRParameterContentType:"Dashboard"
            ])
    }
    
    override func viewWillAppear(animated: Bool) {
        self.sideMenuController()?.sideMenu?.delegate = self
        self.navigationController?.navigationBarHidden = true

            }

    @IBAction func toggleSideMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        backItem.tintColor = UIColor.lightGrayColor()
        navigationItem.backBarButtonItem = backItem
    }
    
    
    @IBAction func RecipeOfDayPressed(sender: AnyObject) {
        
        
        FIRAnalytics.logEventWithName(kFIREventSelectContent, parameters: [
            kFIRParameterItemID:"2",
            kFIRParameterItemName:"DayRecipe",
            kFIRParameterContentType:"Recipe Of Day"
            ])

        
//        NSUserDefaults.standardUserDefaults().setObject(response["data"], forKey:Constants.RECIPEofDAY)
        let recipeDetail = NSUserDefaults.standardUserDefaults().objectForKey(Constants.RECIPEofDAY)
        if recipeDetail?.count > 0 {
            let destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("RecipeDetail") as! RecipeDetailView
            
            var dict = Dictionary<String,AnyObject>()
            let dataArray = NSUserDefaults.standardUserDefaults().objectForKey(Constants.RECIPEofDAY) as! NSArray
            destViewController.iscomingFromRecipe = false
            
            dict = dataArray[0] as! Dictionary<String,AnyObject>
            destViewController.recipeIDSTR = dict["id"] as! String
            self.navigationController?.pushViewController(destViewController, animated: true)
        }
    }
    
    @IBAction func OurRangePressed(sender: AnyObject) {

        let mo = sideMenuController()?.sideMenu?.menuViewController as! MenuTableView
        mo.tableView(mo.tableView, didSelectRowAtIndexPath: NSIndexPath.init(forRow: 1, inSection: 0))
        toggleSideMenuView()
        mo.tableView.selectRowAtIndexPath(NSIndexPath(forRow: 1, inSection: 0), animated: false, scrollPosition: .Middle)

    }
    
    @IBAction func VideosPressed(sender: AnyObject) {
        
        let mo = sideMenuController()?.sideMenu?.menuViewController as! MenuTableView
        mo.tableView(mo.tableView, didSelectRowAtIndexPath: NSIndexPath.init(forRow: 3, inSection: 0))
        toggleSideMenuView()
        mo.tableView.selectRowAtIndexPath(NSIndexPath(forRow: 3, inSection: 0), animated: false, scrollPosition: .Middle)

    }
    
    @IBAction func RecipesPressed(sender: AnyObject) {

        let mo = sideMenuController()?.sideMenu?.menuViewController as! MenuTableView
        mo.tableView(mo.tableView, didSelectRowAtIndexPath: NSIndexPath.init(forRow: 2, inSection: 0))
        toggleSideMenuView()
        mo.tableView.selectRowAtIndexPath(NSIndexPath(forRow: 2, inSection: 0), animated: false, scrollPosition: .Middle)
    }
    
    @IBAction func AddRecipePressed(sender: AnyObject) {
        
        let mo = sideMenuController()?.sideMenu?.menuViewController as! MenuTableView
        mo.tableView(mo.tableView, didSelectRowAtIndexPath: NSIndexPath.init(forRow: 4, inSection: 0))
        toggleSideMenuView()
        mo.tableView.selectRowAtIndexPath(NSIndexPath(forRow: 4, inSection: 0), animated: false, scrollPosition: .Middle)
        
    }

}
