//
//  VideoVC.swift
//  MyRecipeApp
//
//  Created by Surbhi on 09/11/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class VideoVC: UIViewController, ENSideMenuDelegate , UIScrollViewDelegate{//

    @IBOutlet weak var headerScroll: UIScrollView!
    @IBOutlet weak var detailScroll: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    
    private var indicatorLabel : UILabel!
    private var ControllerArray = [AnyObject]()
    private var headerArray = ["PRODUCT","RECIPES"]
    
    private var productArray = [AnyObject]()
    private var recipeArray = [AnyObject]()
    private var modelArray = [AnyObject]()
    private var modelDetailArray = [AnyObject]()
    private var videosArray = [AnyObject]()
    private var modelSPecificProductArray = [AnyObject]()

    var proView = VideoSubVC()
    var recView = VideoSubVC()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.sideMenuController()?.sideMenu?.delegate = self
        self.navigationController?.navigationBarHidden = true
        self.setHeaderScroll()
        getData()
    }

    
    //MARK:- SIDE MENU ACTIONS
    @IBAction func toggleSideMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        backItem.tintColor = UIColor.lightGrayColor()
        navigationItem.backBarButtonItem = backItem
    }
    
    //MARK:- Arrays for Categories
    func getData() {
        if Reachability.isConnectedToNetwork() {
            Constants.appDelegate.startIndicator()
            let method = "videos"
            
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                if response[Constants.STATE] as! Int == 1 {
                    NSUserDefaults.standardUserDefaults().setObject(response["data"], forKey:Constants.VIDEOS)
                    self.createArraysForDisplay()
                }
                else {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.setUpScrollView()
                    }
                    Constants.appDelegate.stopIndicator()
                    CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"",
                        "message":response["message"]!])
                }
            })
        }
        else {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
            dispatch_async(dispatch_get_main_queue()) {
                self.setUpScrollView()
            }
        }
    }
    
    func createArraysForDisplay() {
        modelDetailArray = NSUserDefaults.standardUserDefaults().objectForKey(Constants.VIDEOS) as! Array<AnyObject>

        for item in modelDetailArray {
            var dict = item as! Dictionary<String,AnyObject>
            let Modelstr = dict["model_name"] as! String

            modelArray.append(Modelstr)
            
            let tempArray = dict["videos"] as! Array<AnyObject>
            
            for item2 in tempArray {
                let dict2 = item2 as! Dictionary<String,AnyObject>
                let str  = dict2["type"] as! String
                if str == "Products" {
                    productArray.append(dict2)
                }
                else if str == "Recipes" {
                    recipeArray.append(dict2)
                }
            }
        }
        dispatch_async(dispatch_get_main_queue()) {
            // Do stuff to UI
            self.setUpScrollView()
        }
        Constants.appDelegate.stopIndicator()
    }
    
    
    // MARK: - Label ScrollView
    
    func setHeaderScroll() {
        var scrollContentCount = 0;
        var frame : CGRect = CGRectMake(0, 0, 0, 0)
        var labelx : CGFloat = 0.0
        let width  : CGFloat = ScreenSize.SCREEN_WIDTH / 2
        let firstLabelWidth : CGFloat = ScreenSize.SCREEN_WIDTH / 2
        
        indicatorLabel = UILabel.init(frame: CGRectMake(labelx, 38, firstLabelWidth, 3))
        indicatorLabel.backgroundColor = UIColor.whiteColor()
        indicatorLabel.text = "";
        headerScroll.addSubview(indicatorLabel)
        
        for arrayIndex in 0 ..< headerArray.count {
            frame.origin.x = labelx
            // set scorllview properties
            frame.origin.y = 0;
            frame.size.width=width;
            frame.size.height=39;
            
            let accountLabelButton = UIButton.init(type: UIButtonType.Custom)
            accountLabelButton.addTarget(self, action:#selector(HeaderlabelSelected), forControlEvents: UIControlEvents.TouchUpInside)
            
            accountLabelButton.frame = frame;
            accountLabelButton.tag = arrayIndex;
            accountLabelButton.setAttributedTitle(NSAttributedString(string: headerArray[arrayIndex],  attributes: [NSFontAttributeName : UIFont.init(name: "Helvetica-Bold", size: 15.0)!, NSForegroundColorAttributeName : UIColor.whiteColor()]), forState: UIControlState.Normal)
            
            if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS {
                accountLabelButton.setAttributedTitle(NSAttributedString(string: headerArray[arrayIndex],  attributes: [NSForegroundColorAttributeName : UIColor.whiteColor(),NSFontAttributeName : UIFont.init(name: "Helvetica-Bold", size: 15.0)!]), forState: UIControlState.Normal)
            }
            
            if DeviceType.IS_IPAD{
                accountLabelButton.setAttributedTitle(NSAttributedString(string: headerArray[arrayIndex],  attributes: [NSForegroundColorAttributeName : UIColor.whiteColor(),NSFontAttributeName : UIFont.init(name: "Helvetica-Bold", size: 18.0)!]), forState: UIControlState.Normal)
                
            }

            accountLabelButton.titleLabel?.textAlignment = NSTextAlignment.Center
            
            headerScroll.addSubview(accountLabelButton)
            
            if arrayIndex == 0 {
                accountLabelButton.alpha=1.0;
            }
            else {
                accountLabelButton.alpha=0.7;
            }
            
            labelx=labelx+width;
            scrollContentCount = scrollContentCount + 1;
        }
        
        // set scroll content size
        
        dispatch_async(dispatch_get_main_queue(),{
            self.headerScroll.contentSize = CGSizeMake(frame.width, 40)
        })
        automaticallyAdjustsScrollViewInsets = false;
        detailScroll.delegate=self;
    }
    
    
    func HeaderlabelSelected(sender: UIButton) {
        detailScroll.tag = sender.tag
        
        let xAxis : CGFloat = ScreenSize.SCREEN_WIDTH * CGFloat(sender.tag)
        detailScroll.setContentOffset(CGPointMake(xAxis, 0), animated: true)
        
        let buttonArray = NSMutableArray()
        
        for view in headerScroll.subviews {
            if view.isKindOfClass(UIButton) {
                let labelButton = view as! UIButton
                buttonArray.addObject(labelButton)
                
                if labelButton.tag == sender.tag {
                    labelButton.alpha = 1.0
                }
                else {
                    labelButton.alpha = 0.7
                }
            }
        }
        
        let labelButton = buttonArray.objectAtIndex(sender.tag)
        var frame : CGRect = labelButton.frame
        frame.origin.y = 38
        frame.size.height = 3
        
        UIView.animateWithDuration(0.2, animations: {
            if sender.tag == 0 {
                self.indicatorLabel.frame =  CGRectMake(0,38, ScreenSize.SCREEN_WIDTH/2.0, 3)
            }
            else {
                self.indicatorLabel.frame =  CGRectMake((ScreenSize.SCREEN_WIDTH/2.0) * CGFloat(sender.tag) ,38, ScreenSize.SCREEN_WIDTH/2.0, 3)
            }
            
            self.headerScroll.scrollRectToVisible(self.indicatorLabel.frame, animated: false)
            
        }, completion: nil)
    }
    
    // MARK: -  Main Scrollview
    func setUpScrollView() {
        for arrayIndex in headerArray {
            if (arrayIndex == "PRODUCT")  {
                proView = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("VideoSubVC") as! VideoSubVC
                proView.isProduct = true
                proView.modelArray = modelArray as Array<AnyObject>
                proView.productArray = productArray as Array<AnyObject>
                proView.productMainArray = getProductSUbARray()
                ControllerArray.append(proView)
            }
            if (arrayIndex == "RECIPES") {
                recView = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("VideoSubVC") as! VideoSubVC
                recView.isProduct = false
                recView.recipeArray =  recipeArray as Array<AnyObject>
                ControllerArray.append(recView)
            }
        }
        setViewControllers(ControllerArray, animated: false)
    }
    
    
    //  Add and remove view controllers
    func setViewControllers(viewControllers : NSArray, animated : Bool) {
        if self.childViewControllers.count > 0 {
            for vC in self.childViewControllers {
                vC.willMoveToParentViewController(nil)
                vC.removeFromParentViewController()
            }
        }
        
        for vC in viewControllers {
            self.addChildViewController(vC as! UIViewController)
            vC.didMoveToParentViewController(self)
        }
        
        //TODO animations
        if ((detailScroll) != nil) {
            reloadPages()
        }
    }
    
    func reloadPages() {
        var scrollContentCount = 0;
        
        for arrayIndex in 0 ..< headerArray.count {
            // set scorllview properties
            var frame : CGRect = CGRectMake(0, 0, 0, 0)
            frame.origin.x = ScreenSize.SCREEN_WIDTH * CGFloat(arrayIndex)
            frame.origin.y = 0;
            frame.size = ScreenSize.SCREEN.size
            
            detailScroll.pagingEnabled = true
            let contentWidth : CGFloat = ScreenSize.SCREEN_WIDTH * CGFloat(scrollContentCount+1)
            widthConstraint.constant = contentWidth;
            
            let vC = self.childViewControllers[arrayIndex]
            addView(vC.view, contentView: contentView, frame: frame)
            
            scrollContentCount = scrollContentCount + 1;
        }
        
        dispatch_async(dispatch_get_main_queue(),{
            self.detailScroll.contentSize = CGSizeMake(self.widthConstraint.constant, self.detailScroll.frame.size.height)
        })
        detailScroll.delegate = self
    }
    
    // adding views to scrollview
    func addView(view : UIView, contentView : UIView, frame : CGRect) {
        view.frame = frame
        view.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(view)
        
        dispatch_async(dispatch_get_main_queue(),{
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: contentView, attribute:  NSLayoutAttribute.Leading, multiplier: 1.0, constant: frame.origin.x))
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute:  NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: frame.size.width))
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: contentView, attribute:  NSLayoutAttribute.Top, multiplier: 1.0, constant:0))
            
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute:  NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: frame.size.height - 140))
            })
    }
    
    //MARK: - create SubArray for Product Videos
    func getProductSUbARray() -> Array<AnyObject> {
        var newArray = [AnyObject]()
        for arrayIndex in 0 ..< modelArray.count {
            let newDict = NSMutableDictionary()
            var tempArray = [AnyObject]()
    
            for item in productArray {
                let dict = item as! Dictionary<String,AnyObject>
                
                if dict["model_name"] as! String == modelArray[arrayIndex] as! String {
                    tempArray.append(dict)
                }
            }
            
            newDict.setObject(modelArray[arrayIndex] as! String, forKey: "model_name")
            newDict.setObject(tempArray, forKey: modelArray[arrayIndex] as! String)
            newArray.append(newDict)
        }
        return newArray
    }
}
