//
//  VideoSubVC.swift
//  MyRecipeApp
//
//  Created by Surbhi on 09/11/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class VideoSubVC: UIViewController , UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var noCount: UILabel!
    @IBOutlet weak var tableViewTop: NSLayoutConstraint!
    
    var isProduct = Bool()
    var recipeArray = [AnyObject]()
    var productArray = [AnyObject]()
    var productMainArray = [AnyObject]()
    var modelArray = [AnyObject]()

    private var expandedSections = NSMutableIndexSet()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.delegate = self
        tableview.dataSource = self

        if isProduct == true {
            if modelArray.count > 0 {
                tableview.hidden = false
                noCount.hidden = true
                self.tableViewTop.constant = 3
                self.tableview.reloadData()
            }
            else {
                tableview.hidden = true
                noCount.hidden = false
            }
        }
        else {
            if recipeArray.count > 0 {
                tableview.hidden = false
                noCount.hidden = true
                self.tableViewTop.constant = -30
                self.tableview.reloadData()
            }
            else {
                tableview.hidden = true
                noCount.hidden = false
            }
        }
    }

    func OpenSection(sender : AnyObject) {
        let button = sender as? UIButton
        let index =  (button?.tag)! - 2000
        
        let currentlyExpanded : Bool = expandedSections.containsIndex(index)
        
        if currentlyExpanded == true {
            expandedSections.removeIndex(index)
            UIView.animateWithDuration(0.5, animations: {
                self.tableview.reloadSections(NSIndexSet.init(index: index), withRowAnimation: UITableViewRowAnimation.Automatic)
            })
        }
        else {
            var otherindex = Int()

            if expandedSections.count > 0 {
                otherindex = expandedSections.firstIndex
                expandedSections.removeIndex(otherindex)
//                UIView.animateWithDuration(0.5, animations: {
                    self.tableview.reloadSections(NSIndexSet.init(index: otherindex), withRowAnimation: UITableViewRowAnimation.Automatic)
//                })
            }
            expandedSections.addIndex(index)
            UIView.animateWithDuration(0.5, animations: {
                self.tableview.reloadSections(NSIndexSet.init(indexesInRange: NSRange.init(location: 0, length: self.productMainArray.count)), withRowAnimation:  UITableViewRowAnimation.None)
            })
        }
    }

    
    //MARK: - TABLEVIEW
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if isProduct == true {
            return productMainArray.count
        }
        else {
            return recipeArray.count
        }
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if isProduct == true {
            if DeviceType.IS_IPAD {
                return 50
            }
            return 40
        }
        else {
            return 0
        }
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if isProduct == true {
            
            if DeviceType.IS_IPAD {
                let vw = UIView.init(frame: CGRectMake(0, 0, ScreenSize.SCREEN_WIDTH, 50))
                vw.backgroundColor = Colors.appThemeColor
                vw.layer.cornerRadius = 5.0
                vw.clipsToBounds = true
                
                let name = UILabel.init(frame: CGRectMake(20, 5.5, ScreenSize.SCREEN_WIDTH - 80, 40))
                name.backgroundColor = UIColor.clearColor()
                name.text = ( productMainArray[section] as! Dictionary<String,AnyObject> )["model_name"] as? String
                name.textColor = UIColor.whiteColor()
                name.font = UIFont.init(name: "Helvetica-Bold", size: 19.0)!
                
                let button = UIButton.init(frame: CGRectMake(0, 0, ScreenSize.SCREEN_WIDTH, 40 ))
                button.backgroundColor = UIColor.clearColor()
                button.addTarget(self, action:#selector(OpenSection), forControlEvents: UIControlEvents.TouchUpInside)
                button.tag = 2000 + section
                
                let arrow = UIImageView.init(frame: CGRectMake(ScreenSize.SCREEN_WIDTH - 60, 10, 30, 30))
                arrow.image = UIImage.init(named: "DownArrowWhite.png")
                arrow.tag = section
                
                let currentlyExpanded : Bool = expandedSections.containsIndex(section)
                
                if currentlyExpanded == true {
                    arrow.transform = CGAffineTransformMakeRotation(0)
                }
                else {
                    arrow.transform = CGAffineTransformMakeRotation(CGFloat(-M_PI)/2)
                }
                
                vw.addSubview(name)
                vw.addSubview(button)
                vw.addSubview(arrow)
                
                return vw

            }
            else{
                let vw = UIView.init(frame: CGRectMake(0, 0, ScreenSize.SCREEN_WIDTH, 40))
                vw.backgroundColor = Colors.appThemeColor
                vw.layer.cornerRadius = 5.0
                vw.clipsToBounds = true
                
                let name = UILabel.init(frame: CGRectMake(10, 5.5, ScreenSize.SCREEN_WIDTH - 20, 30))
                name.backgroundColor = UIColor.clearColor()
                name.text = ( productMainArray[section] as! Dictionary<String,AnyObject> )["model_name"] as? String
                name.textColor = UIColor.whiteColor()
                name.font = UIFont.init(name: "Helvetica-Bold", size: 15.0)!
                
                let button = UIButton.init(frame: CGRectMake(0, 0, ScreenSize.SCREEN_WIDTH, 40 ))
                button.backgroundColor = UIColor.clearColor()
                button.addTarget(self, action:#selector(OpenSection), forControlEvents: UIControlEvents.TouchUpInside)
                button.tag = 2000 + section
                
                let arrow = UIImageView.init(frame: CGRectMake(ScreenSize.SCREEN_WIDTH - 40, 10, 20, 20))
                arrow.image = UIImage.init(named: "DownArrowWhite.png")
                arrow.tag = section
                
                let currentlyExpanded : Bool = expandedSections.containsIndex(section)
                
                if currentlyExpanded == true {
                    arrow.transform = CGAffineTransformMakeRotation(0)
                }
                else {
                    arrow.transform = CGAffineTransformMakeRotation(CGFloat(-M_PI)/2)
                }
                
                vw.addSubview(name)
                vw.addSubview(button)
                vw.addSubview(arrow)
                
                return vw
            }
        }
        else {
            return nil
        }
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isProduct == true {
            if expandedSections.contains(section) {
                let dict = productMainArray[section] as! Dictionary<String,AnyObject>
                let name = dict["model_name"] as! String
                let arry = dict[name] as! Array<AnyObject>
                
                return arry.count
            }
            else {
                return 0
            }
        }
        else {
            return 1
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("VideoSubCell")
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "VideoSubCell")
        }

        let head = cell?.contentView.viewWithTag(1062) as! UILabel
        let date = cell?.contentView.viewWithTag(1063) as! UILabel
        let img = cell?.contentView.viewWithTag(1061) as! UIImageView
        img.contentMode = UIViewContentMode.ScaleAspectFill
        
        let myActivityIndicator = UIActivityIndicatorView()
        myActivityIndicator.center = img.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.activityIndicatorViewStyle = .Gray
        img.addSubview(myActivityIndicator)
        myActivityIndicator.startAnimating()
        
        if isProduct == true {
            cell?.layer.borderColor = Colors.ShadowColor.CGColor
            cell?.layer.borderWidth = 0.5
            cell?.layer.cornerRadius = 3.0
            cell?.backgroundColor = UIColor.clearColor()
            cell?.contentView.backgroundColor = UIColor.whiteColor()

            cell?.clipsToBounds = true

            let dict = productMainArray[indexPath.section] as! Dictionary<String,AnyObject>
            let name = dict["model_name"] as! String
            let arry = dict[name] as! Array<AnyObject>
            let newdict = arry[indexPath.row] as! Dictionary<String,AnyObject>
            
            head.text = newdict["title"] as? String
            date.text = newdict["model_name"] as? String
            Helper.loadImageFromUrlWithIndicator(newdict["video_thumb"] as! String, view: img, indic: myActivityIndicator)
        }
        else {
            cell?.layer.shadowColor = Colors.ShadowColor.CGColor
            cell?.layer.cornerRadius = 3.0
            cell?.backgroundColor = UIColor.clearColor()
            cell?.contentView.backgroundColor = UIColor.whiteColor()
            cell?.clipsToBounds = true
            
            head.text = (recipeArray[indexPath.section] as! Dictionary<String,AnyObject>)["title"] as? String
            date.text = (recipeArray[indexPath.section] as! Dictionary<String,AnyObject>)["model_name"] as? String
            Helper.loadImageFromUrlWithIndicator((recipeArray[indexPath.section] as! Dictionary<String,AnyObject>)["video_thumb"] as! String, view: img, indic: myActivityIndicator)
        }
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if isProduct == true {
            let dict = productMainArray[indexPath.section] as! Dictionary<String,AnyObject>
            let name = dict["model_name"] as! String
            let arry = dict[name] as! Array<AnyObject>
            
            let dict2 = arry[indexPath.row] as! Dictionary<String, AnyObject>
            let videoID = dict2["video_id"] as! String

            let  destinationController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("YouTubeVideoPlayer") as! YouTubeVideoPlayer
            
            if videoID != "" {
                destinationController.videoID = videoID
                self.navigationController?.pushViewController(destinationController, animated: true)
            }
        }
        else {
            let dict = recipeArray[indexPath.section] as! Dictionary<String, AnyObject>
            let videoID = dict["video_id"] as! String
            
            let  destinationController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("YouTubeVideoPlayer") as! YouTubeVideoPlayer
            
            if videoID != "" {
                destinationController.videoID = videoID
                self.navigationController?.pushViewController(destinationController, animated: true)
            }
        }
    }
}
