 //
//  SplashView.swift
//  MyRecipeApp
//
//  Created by Surbhi on 26/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class SplashView: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        Constants.appDelegate.registerForPush()
        loadRecipeOfTheDay()
    }
    
    
    func GoToLogin() {
        
        SVProgressHUD.dismiss()
//        CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "LOGIN", "message":"GO TO LOGIN"])

        
        if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == ""  {
            
            if NSUserDefaults.standardUserDefaults().boolForKey("ApplicationLaunchedFirstTime") == false {
                dispatch_async(dispatch_get_main_queue(), {
                    
                    let login = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("TutorialVC")
                    UIApplication.sharedApplication().keyWindow?.rootViewController = login
//                    self.navigationController?.presentViewController(login, animated: true, completion: nil)
//                    self.presentViewController(login, animated: true, completion: nil)
                })
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), {
                    
                    let login = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("LoginView")
                    UIApplication.sharedApplication().keyWindow?.rootViewController = login
//                    self.presentViewController(login, animated: true, completion: nil)
                })
            }
        }
        else {
            dispatch_async(dispatch_get_main_queue(), {
                
//                CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "def", "message":"default"])

                let viewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("SideMenuNavigation")
                UIApplication.sharedApplication().keyWindow?.rootViewController = viewController
            })
        }
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    
    //MARK: - RECIPE OF Day
    func loadRecipeOfTheDay() {
        if Reachability.isConnectedToNetwork() {

            SVProgressHUD.show()
            let method = "recipes/featured/user_id/"
            var userID = String()
            
            if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == "" ||  (NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil) {
                userID = ""
            }
            else {
                userID = NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID)!
            }
            
            Server.getRequestWithURL(Constants.BASEURL+method+userID, completionHandler: { (response) in

                if response[Constants.STATE] as! Int == 1 {
                    NSUserDefaults.standardUserDefaults().setObject(response["data"], forKey:Constants.RECIPEofDAY)
                }
//                self.ourRange()
                self.ourTOPRange()
            })
        }
        else {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    //MARK: - ALL RECIPES
    func loadRecipes() {// recipes/
        if Reachability.isConnectedToNetwork() {

            SVProgressHUD.show()
            let method = "recipes/"
            Server.postRequestWithURL(Constants.BASEURL+method, paramString: "", completionHandler: { (response) in
                
                 SVProgressHUD.dismiss()
                if response[Constants.STATE] as! Int == 1 {
                    NSUserDefaults.standardUserDefaults().setObject(response["data"], forKey:Constants.RECIPE)
                }
                self.GoToLogin()
            })
        }
        else  {
            SVProgressHUD.dismiss()
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    //MARK: - OUR RANGE //http://neuronimbusapps.com/mwo-recipes/api/microwaves/top_categories
    
    func ourTOPRange(){
        if Reachability.isConnectedToNetwork() {
            SVProgressHUD.show()

            let method = "microwaves/top_categories"
            
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                SVProgressHUD.dismiss()
                if response[Constants.STATE] as! Int == 1 {
                    NSUserDefaults.standardUserDefaults().setObject(response["data"], forKey:Constants.TOPRANGE)
                    var arr = [AnyObject]()
                    
                    for item in response["data"] as! Array<AnyObject>
                    {
                        let dict = item as! Dictionary<String, AnyObject>
                        let newDIct = NSMutableDictionary()
                        
                        let idStr = dict["id"] as! String
                        let nameStr = dict["title"] as! String
                        newDIct.setObject(idStr, forKey: "id")
                        newDIct.setObject(nameStr, forKey: "name")
                        arr.append(newDIct)
                    }
                    
                    NSUserDefaults.standardUserDefaults().setObject(arr, forKey: Constants.TOPMODEL)
                }
                self.getRecipeFilter()
            })
        }
        else {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }

    
    

    func getRecipeFilter() {
        if Reachability.isConnectedToNetwork() {

            SVProgressHUD.show()
            let method = "recipes/filters"
            
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                 SVProgressHUD.dismiss()
                if response[Constants.STATE] as! Int == 1 {
                    let DICT  = response["data"] as! Dictionary<String,AnyObject>
                    let levArry  = DICT["levels"] as! Array<AnyObject>
                    let CuiArry = DICT["cuisines"] as! Array<AnyObject>
                    NSUserDefaults.standardUserDefaults().setObject(levArry, forKey: Constants.LEVEL)
                    NSUserDefaults.standardUserDefaults().setObject(CuiArry, forKey: Constants.CUISINE)
                }
                 self.getIngredientunits()
            })
        }
        else {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
    }
    
    func getIngredientunits() {

        if Reachability.isConnectedToNetwork() {
            SVProgressHUD.show()
            let method = "recipes/ingredient_units"
            
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                SVProgressHUD.dismiss()
                if response[Constants.STATE] as! Int == 1 {
                    let unitArray  = response["data"] as! Array<AnyObject>
                    NSUserDefaults.standardUserDefaults().setObject(unitArray, forKey: Constants.UNITS)
                }
                self.loadRecipes()
            })
        }
        else {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
    }
}
