//
//  webViewVC.swift
//  MyRecipeApp
//
//  Created by Surbhi on 10/11/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class webViewVC: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var mywebview: UIWebView!
    var idStr = String()
    var loadingURLSTR = String()

    @IBOutlet weak var toggleButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    //MARK: - VIEW LOADING

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mywebview.layer.shadowColor = Colors.ShadowColor.CGColor
        mywebview.layer.borderColor = Colors.ShadowColor.CGColor
        mywebview.layer.borderWidth = 0.5
        mywebview.clipsToBounds = true
        self.navigationController?.navigationBarHidden = true
        
        let mo = sideMenuController()?.sideMenu?.menuViewController as! MenuTableView
        if mo.selectedMenuItem == 11
        {
            idStr = "about-us"
            headingLabel.text = "About Us"
            toggleButton.hidden = false
            backButton.hidden = true
        }
        else if mo.selectedMenuItem == 12
        {
             idStr = "contact-us"
            headingLabel.text = "Contact Us"
            toggleButton.hidden = false
            backButton.hidden = true
            AppsFlyerTracker.sharedTracker().trackEvent("Contact Us", withValue: "Viewed") // APPSFlyer
        }
        else if mo.selectedMenuItem == 13
        {
            idStr = "terms-of-use"
            headingLabel.text = "Terms & Conditions"
            toggleButton.hidden = true
            backButton.hidden = false
        }


        if idStr != "" {
            loadDescription()
        }
        else
        {
            let alert = UIAlertController.init(title: "Culineria", message: "Some error Occurred while Loading Details!", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                self.dismissViewControllerAnimated(true, completion: nil)
            }))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
    }
    
    //MARK: - GET DESCRIPTION
    func loadDescription() // recipes/featured/user_id/
    {
        if Reachability.isConnectedToNetwork()
        {
            Constants.appDelegate.startIndicator()
            let method = "content/"
            
            Server.getRequestWithURL(Constants.BASEURL+method+idStr, completionHandler: { (response) in
                
                if response[Constants.STATE] as! Int == 1
                {
                    let dict = response["data"] as! Array<AnyObject>
                    self.loadingURLSTR = dict[0]["description"] as! String
                    self.headingLabel.text = dict[0]["title"] as? String
                    self.mywebview.loadHTMLString(self.loadingURLSTR, baseURL: nil)
                }
                else
                {
                    let alert = UIAlertController.init(title: "Culineria", message: response["message"]! as? String, preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                        self.dismissViewControllerAnimated(true, completion: nil)
                    }))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            })
        }
        else
        {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }

    
    
    //MARK: - SIDE MENU
    @IBAction func toggleSideMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        backItem.tintColor = UIColor.lightGrayColor()
        navigationItem.backBarButtonItem = backItem
    }

    
    @IBAction func BackButtonPressed(sender: AnyObject) {
        
        let mo = sideMenuController()?.sideMenu?.menuViewController as! MenuTableView
        mo.tableView.reloadData()
        mo.tableView(mo.tableView, didSelectRowAtIndexPath: NSIndexPath.init(forRow: mo.lastSelectedMenuItem, inSection: 0))
        mo.tableView.selectRowAtIndexPath(NSIndexPath(forRow: mo.selectedMenuItem, inSection: 0), animated: false, scrollPosition: .Middle)
        toggleSideMenuView()
        
    }
    
    //MARK: - Webview Loading
    func webViewDidFinishLoad(webView: UIWebView) {
        Constants.appDelegate.stopIndicator()
    }
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if request.URL?.scheme == "mailto" || request.URL?.scheme == "tel" {
            
            print(request.URL)
            let url = request.URL!
            UIApplication.sharedApplication().openURL(url)
        }
        
        return true
    }

}
