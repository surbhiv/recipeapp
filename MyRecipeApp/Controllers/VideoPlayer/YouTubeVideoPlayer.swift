//
//  YouTubeVideoPlayer.swift
//  MyRecipeApp
//
//  Created by Surbhi on 09/11/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit
import YouTubePlayer

class YouTubeVideoPlayer: UIViewController {

    
    @IBOutlet var playerView: YouTubePlayerView!
    
    var videoID = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver( self, selector: #selector(DismissLoader), name: "VideoLoaded", object: nil)
        
        // Do any additional setup after loading the view, typically from a nib.
        playerView.playerVars = [
            "playsinline": "1",
            "controls": "1",
            "showinfo": "0"
        ]
        
        
//        webView.allowsInlineMediaPlayback = true
//        webView.mediaPlaybackRequiresUserAction = false
//        
//        let videoID = "zN-GGeNPQEg" // https://www.youtube.com/watch?v=zN-GGeNPQEg
//        
//        let embededHTML = "<html><body style='margin:0px;padding:0px;'><script type='text/javascript' src='http://www.youtube.com/iframe_api'></script><script type='text/javascript'>function onYouTubeIframeAPIReady(){ytplayer=new YT.Player('playerId',{events:{onReady:onPlayerReady}})}function onPlayerReady(a){a.target.playVideo();}</script><iframe id='playerId' type='text/html' width='\(self.view.frame.size.width)' height='\(self.view.frame.size.height)' src='http://www.youtube.com/embed/\(videoID)?enablejsapi=1&rel=0&playsinline=1&autoplay=1' frameborder='0'></body></html>"
//        
//        webView.loadHTMLString(embededHTML, baseURL: NSBundle.mainBundle().resourceURL)

        
        Constants.appDelegate.startIndicator()
        
        if Reachability.isConnectedToNetwork() {
        
            playerView.loadVideoID(videoID)
        }
        else {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    
    func DismissLoader(noti : NSNotification) {
        Constants.appDelegate.stopIndicator()

    }
    
    @IBAction func play(sender: UIButton) {
        if playerView.ready {
            if playerView.playerState != YouTubePlayerState.Playing {
                playerView.play()
            } else {
                playerView.pause()
            }
        }
    }
    
    func showAlert(message: String) {
        self.presentViewController(alertWithMessage(message), animated: true, completion: nil)
    }
    
    func alertWithMessage(message: String) -> UIAlertController {
        let alertController =  UIAlertController(title: "Culineria", message: message, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: nil))
        
        return alertController
    }
    
    //MARK:- BUTTON Actions
    
    @IBAction func BackButtonPressed(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
