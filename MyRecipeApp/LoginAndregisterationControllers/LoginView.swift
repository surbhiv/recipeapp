//
//  LoginView.swift
//  MyRecipeApp
//
//  Created by Surbhi on 26/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit
import Firebase
import Google
//import FacebookCore
//import FacebookLogin


class LoginView: UIViewController, UITextFieldDelegate, GIDSignInUIDelegate, GIDSignInDelegate {

    // MARK: IBOutlets Defined
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var emailLine: UILabel!
    @IBOutlet weak var lineHeight: NSLayoutConstraint!
    @IBOutlet weak var passLabel: UILabel!
    @IBOutlet weak var passLine: UILabel!
    @IBOutlet weak var passText: UITextField!
    @IBOutlet weak var passLineHeight: NSLayoutConstraint!
    
    @IBOutlet weak var logoviewHeight: NSLayoutConstraint!
    var fontsze : CGFloat = (DeviceType.IS_IPAD ? 16.0 : 13.0)
    
    private var indexToredirect : Int?
    private var sideMenuObj = MenuTableView()
    
    // MARK: Start
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIApplication.sharedApplication().keyWindow?.rootViewController?.isKindOfClass(SideMenuNavigation) == true {
            sideMenuObj = sideMenuController()!.sideMenu!.menuViewController as! MenuTableView
            if sideMenuObj.lastSelectedMenuItem == 4
            {
                indexToredirect = (sideMenuController()?.sideMenu?.menuViewController as! MenuTableView).lastSelectedMenuItem
            }
        }
        
        dispatch_async(dispatch_get_main_queue()) {
            // Do stuff to UI
            self.emailText.attributedPlaceholder = NSAttributedString(string: " Email Id*",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: "Helvetica", size: self.fontsze)!])
            
            self.passText.attributedPlaceholder = NSAttributedString(string: " Password*",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: "Helvetica", size: self.fontsze)!])

        }

    }
    

    override func viewWillAppear(animated: Bool) {
        if Constants.appDelegate.justLoggedIn == true && Constants.appDelegate.isRateView == true
        {
            Constants.appDelegate.isRateView = false
            Constants.appDelegate.isRecipeDetail = false
            sideMenuObj.UpdateMenuView()
            self.dismissViewControllerAnimated(false, completion: nil)
        }
    }
    
    override func prefersStatusBarHidden() -> Bool {
        
        return true
    }

    
    
    // MARK: TEXTFIELD DELEGATE
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if textField == emailText {

            
            UIView.animateWithDuration(1.0, animations: { 
                self.emailLabel.hidden = false
                self.emailText.attributedPlaceholder = NSAttributedString(string: "")
                Helper.activatetextField(self.emailLine,height: self.lineHeight)

                },
                completion: { (true) in
                    dispatch_async(dispatch_get_main_queue(), {
                        self.emailLabel.hidden = false
                        self.view.bringSubviewToFront(self.emailLabel)
                        self.emailText.attributedPlaceholder = NSAttributedString(string: "")
                        Helper.activatetextField(self.emailLine,height: self.lineHeight)
                        
                        if self.passText.text == nil || self.passText.text == "" {
                            self.passLabel.hidden = true
                        }
                        else {
                            self.passLabel.hidden = false
                        }
                    })
            })
            
            
        }
        if textField == passText {
            if emailText.text == nil || emailText.text == "" {
             emailLabel.hidden = true
            }
            else {
                emailLabel.hidden = false
            }
            UIView.animateWithDuration(1.0, delay: 0, options: UIViewAnimationOptions.TransitionFlipFromBottom, animations: {
                self.passLabel.hidden = false
                self.passText.attributedPlaceholder = NSAttributedString(string: "")
                Helper.activatetextField(self.passLine,height: self.passLineHeight)
                }, completion: nil)
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if textField == emailText {
            if textField.text == nil || textField.text == ""
            {
                UIView.animateWithDuration(1.0, animations: {
                    self.emailLabel.hidden = true
                    Helper.deActivatetextField(self.emailLine,height: self.lineHeight)
                    self.emailText.attributedPlaceholder = NSAttributedString(string: " Email Id*",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: "Helvetica", size: self.fontsze)!])
                    }, completion: nil)
            }
        }
        if textField == passText {
            if textField.text == nil || textField.text == ""
            {
                UIView.animateWithDuration(1.0, animations: {
                    self.passLabel.hidden = true
                    Helper.deActivatetextField(self.passLine,height: self.passLineHeight)
                    self.passText.attributedPlaceholder = NSAttributedString(string: " Password",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: "Helvetica", size: self.fontsze)!])
                    }, completion: nil)
            }
        }

    }
    
    
    // MARK: BUTTON ACTIONS
    @IBAction func SignInPressed(sender: AnyObject) {
        
        emailText.resignFirstResponder()
        passText.resignFirstResponder()

        
        if emailText.text == "" {
            
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message": "Email Id cannot be empty."] )
        }
        else if passText.text == "" {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message": "Password cannot be empty."] )
        }
        else if !Helper.isValidEmail(emailText.text!) {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message": "Enter a valid email id."] )
        }
        else {
            if Reachability.isConnectedToNetwork() {
                Constants.appDelegate.startIndicator()
                
                Server.LoginToApp(emailText.text!, authType: "", authId: "", pass: passText.text!, completionHandler: { (response) in
                    
                    Constants.appDelegate.stopIndicator()
                    if response[Constants.STATE] as! Int == 1 {
                        let dict = response["data"]
                        NSUserDefaults.standardUserDefaults().setObject(dict![0]["id"], forKey:Constants.UserID)
                        NSUserDefaults.standardUserDefaults().setObject(dict![0], forKey:Constants.PROFILE)
                        let passKey = dict![0]["pwd_blank"]
                        
                        self.firebaseAnalyticMethod("0")
                        
                        if passKey! == nil
                        {
                            NSUserDefaults.standardUserDefaults().setObject(NSNumber.init(int: 0), forKey:Constants.PASSWORD)
                        }
                        else
                        {
                            NSUserDefaults.standardUserDefaults().setObject(passKey, forKey:Constants.PASSWORD)
                        }

                        dispatch_async(dispatch_get_main_queue(), {
                            Constants.appDelegate.justLoggedIn = true
                            
                            if  Constants.appDelegate.isRateView == true  || Constants.appDelegate.isRecipeDetail == true
                            {
                                self.sideMenuObj.UpdateMenuView()
                                Constants.appDelegate.isRecipeDetail = false
                                self.dismissViewControllerAnimated(true, completion: nil)
                            }
                            else
                            {
                                let viewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("SideMenuNavigation")
                                UIApplication.sharedApplication().keyWindow?.rootViewController = viewController;
                            }
                            
                        })
                    }
                    else {
                        let mess = response["message"] as! String
                        CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"", "message":mess])
                    }
                })
            }
            else {
                CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
            }
        }
    }
    
    
    @IBAction func ForgotPasswordPressed(sender: AnyObject) {
        
        passText.resignFirstResponder()
        emailText.resignFirstResponder()

        let forgot = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("ForgotPassword") as! ForgotPasswordView
        forgot.isForgetView = true
        forgot.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext
        self.presentViewController(forgot, animated: true, completion: nil)

    }
    
    @IBAction func SignUpPressed(sender: AnyObject) {
        
        dispatch_async(dispatch_get_main_queue(), {
            let login = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("Registeration") as! RegisterationView
            self.presentViewController(login, animated: true, completion: nil)
        })
    }
    @IBAction func ContinuewAsGuestPressed(sender: AnyObject) {
        
        passText.resignFirstResponder()
        emailText.resignFirstResponder()
        Constants.appDelegate.justLoggedIn = false
        
        if  Constants.appDelegate.isRateView == true
        {
            sideMenuObj.UpdateMenuView()
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        else
        {
            Constants.appDelegate.isRecipeDetail = false
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "LoginRequiredError")
            let viewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("SideMenuNavigation")
            UIApplication.sharedApplication().keyWindow?.rootViewController = viewController;
        }
    }
    
    //MARK: - FACEBOOK
    
    @IBAction func FaceBookLoginPressed(sender: AnyObject) {

        passText.resignFirstResponder()
        emailText.resignFirstResponder()
        
        if Reachability.isConnectedToNetwork() {
            
            Constants.appDelegate.startIndicator()
            dispatch_async(dispatch_get_main_queue())
            {
                let logManager = FBSDKLoginManager.init()
                //[FBSDKSettings sdkVersion]
                print(FBSDKSettings.sdkVersion())
                logManager.loginBehavior = FBSDKLoginBehavior.Web
                logManager.logOut()
                logManager.logInWithReadPermissions(["public_profile", "email"], fromViewController: self) { (result, error) in
                    if error != nil {
                        Constants.appDelegate.stopIndicator()
                        CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title": "Culineria", "message" : "Some error Occurred !"])
                    }
                    else if result.isCancelled == true {
                        Constants.appDelegate.stopIndicator()
//                        print("Is Cancelled")
                    }
                    else {
                        if result.token.tokenString != "" || result.token != nil {
                            self.fetchUserInfo()
                        }
                    }
                }
            }
        }
        else {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    func fetchUserInfo()
    {
        
        if Reachability.isConnectedToNetwork() {
            
            
            let req = FBSDKGraphRequest.init(graphPath: "me" , parameters: ["fields": "id, name, picture.width(500).height(500), email, gender"])
            dispatch_async(dispatch_get_main_queue())
            {
                req.startWithCompletionHandler
                    { (connection, result, error) in
                        if(error == nil) {
                            
                            let resDict = result as! Dictionary<String,AnyObject>
                            self.checkUserAuthonticationWithUserInfo(resDict)
                        }
                        else {
                            Constants.appDelegate.stopIndicator()
                            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"Culineria","message":"Some error occured"])
                        }
                }
            }
        }
        else {
             Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
    }
    
    func checkUserAuthonticationWithUserInfo(result : NSDictionary) {
        
        if Reachability.isConnectedToNetwork() {

            let userId = result["id"] as! String
            let fullName = result["name"] as! String
            var email = result["email"] as? String
            
            if email == nil {
                email = ""
            }
            
            let picDic = result["picture"] as! Dictionary<String,AnyObject>
            let picData = picDic["data"] as! Dictionary<String,AnyObject>
            let profileImage = picData["url"] as! String

            
            Server.LoginToApp(email!, authType: "1", authId: userId, pass: "", completionHandler: { (response) in
        
                if response[Constants.STATE] as! Int == 1
                {
                    Constants.appDelegate.stopIndicator()
                    let dict = response["data"]
                    NSUserDefaults.standardUserDefaults().setObject(dict![0]["id"], forKey:Constants.UserID)
                    NSUserDefaults.standardUserDefaults().setObject(dict![0], forKey:Constants.PROFILE)
                    let passKey = dict![0]["pwd_blank"]
                    
                    if passKey! == nil
                    {
                        NSUserDefaults.standardUserDefaults().setObject(NSNumber.init(int: 1), forKey:Constants.PASSWORD)
                    }
                    else
                    {
                        NSUserDefaults.standardUserDefaults().setObject(passKey, forKey:Constants.PASSWORD)
                    }

                    
                    self.firebaseAnalyticMethod("1")
                    dispatch_async(dispatch_get_main_queue(), {
                        Constants.appDelegate.justLoggedIn = true

                        if  Constants.appDelegate.isRateView == true || Constants.appDelegate.isRecipeDetail == true
                        {
                            Constants.appDelegate.isRateView = false
                            Constants.appDelegate.isRecipeDetail = false
                            self.sideMenuObj.UpdateMenuView()
                            self.dismissViewControllerAnimated(true, completion: nil)
                        }
                        else
                        {
                            let viewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("SideMenuNavigation")
                            UIApplication.sharedApplication().keyWindow?.rootViewController = viewController;
                        }
                    })
                }
                else {
                    // go to registeration
//                    print("Registeration Required")
                    Server.RegisterationForApp(fullName, email: email!, mobile: "", authType: "1", authId: userId, pass: "", profileImage: profileImage, completionHandler: { (response) in
                        
                        Constants.appDelegate.stopIndicator()
                        if response[Constants.STATE] as! Int == 1 {
                            let dict = response["data"]
                        
                            NSUserDefaults.standardUserDefaults().setObject(dict!["id"], forKey:Constants.UserID)
                            NSUserDefaults.standardUserDefaults().setObject(dict!, forKey:Constants.PROFILE)
                            let passKey = dict!["pwd_blank"]
                            
                            self.firebaseAnalyticMethod("1")
                            
                            if passKey! == nil
                            {
                                NSUserDefaults.standardUserDefaults().setObject(NSNumber.init(int: 1), forKey:Constants.PASSWORD)
                            }
                            else
                            {
                                NSUserDefaults.standardUserDefaults().setObject(passKey, forKey:Constants.PASSWORD)
                            }
                            
                            dispatch_async(dispatch_get_main_queue(), {
                                Constants.appDelegate.justLoggedIn = true
                                
                                
                                if  Constants.appDelegate.isRateView == true || Constants.appDelegate.isRecipeDetail == true
                                {
                                    Constants.appDelegate.isRateView = false
                                    Constants.appDelegate.isRecipeDetail = false
                                    self.sideMenuObj.UpdateMenuView()
                                    self.dismissViewControllerAnimated(true, completion: nil)
                                }
                                else
                                {
                                    let viewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("SideMenuNavigation")
                                    UIApplication.sharedApplication().keyWindow?.rootViewController = viewController;
                                }
                            })
                        }
                        else {
                             Constants.appDelegate.stopIndicator()
                            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":response["message"]!])
                        }
                    })
                }
            })
        }
        else {
            
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    
    //MARK: - GOOGLE
    
    @IBAction func GooglePlusLoginPressed(sender: AnyObject) {
        passText.resignFirstResponder()
        emailText.resignFirstResponder()
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().clientID = FIRApp.defaultApp()?.options.clientID
        GIDSignIn.sharedInstance().signIn()
    }
    
    func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
        if (error == nil)
        {
        }
        else
        {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":error.localizedDescription])
        }
    }
    
    func signIn(signIn: GIDSignIn!, presentViewController viewController: UIViewController!) {
        self.presentViewController(viewController, animated: true, completion: nil)
    }
    
    func signIn(signIn: GIDSignIn!, dismissViewController viewController: UIViewController!) {
        
        Constants.appDelegate.startIndicator()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK: - Google Methods
    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!, withError error: NSError!)
    {
        if Reachability.isConnectedToNetwork() {
            if (error == nil)
            {
                let userId = user.userID
                let fullName = user.profile.name
                let email = user.profile.email
                var profileImage : String = ""

                if user.profile.hasImage == true
                {
                    let imageUrl = signIn.currentUser.profile.imageURLWithDimension(500)

                    if imageUrl != ""  || imageUrl != nil{
                        profileImage = imageUrl.absoluteString
                    }
                    else
                    {
                        profileImage = ""
                    }
                    
                    self.signInMethodCalled(email!, userId: userId,profileImage: profileImage, fullName: fullName)
                    
                }
                else
                {
                    self.signInMethodCalled(email!, userId: userId,profileImage: profileImage, fullName: fullName)
                }
               
            }
            else {
                Constants.appDelegate.stopIndicator()
                CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":error.localizedDescription])
            }

        }
        else {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
    }
    
    
    func signInMethodCalled(email: String, userId : String , profileImage : String, fullName : String)
    {
        Server.LoginToApp(email, authType: "2", authId: userId, pass: "", completionHandler: { (response) in
            if response[Constants.STATE] as! Int == 1
            {
                Constants.appDelegate.stopIndicator()
                let dict = response["data"]
                NSUserDefaults.standardUserDefaults().setObject(dict![0]["id"], forKey:Constants.UserID)
                NSUserDefaults.standardUserDefaults().setObject(dict?.objectAtIndex(0) as! Dictionary<String,AnyObject>, forKey:Constants.PROFILE)
                let passKey = dict![0]["pwd_blank"]
                NSUserDefaults.standardUserDefaults().setObject(passKey, forKey:Constants.PASSWORD)
                
                self.firebaseAnalyticMethod("2")
                
                dispatch_async(dispatch_get_main_queue(), {
                    Constants.appDelegate.justLoggedIn = true
                    if  Constants.appDelegate.isRateView == true || Constants.appDelegate.isRecipeDetail == true
                    {
                        Constants.appDelegate.isRateView = false
                        Constants.appDelegate.isRecipeDetail = false
                        self.sideMenuObj.UpdateMenuView()
                        self.dismissViewControllerAnimated(true, completion: nil)
                    }
                    else
                    {
                        let viewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("SideMenuNavigation")
                        UIApplication.sharedApplication().keyWindow?.rootViewController = viewController;
                    }
                })
            }
            else
            {
                // go to registeration
                Server.RegisterationForApp(fullName, email: email, mobile: "", authType: "2", authId: userId, pass: "", profileImage: profileImage, completionHandler: { (response) in
                    
                    Constants.appDelegate.stopIndicator()
                    if response[Constants.STATE] as! Int == 1
                    {
                        let dict = response["data"]
                        
                        NSUserDefaults.standardUserDefaults().setObject(dict!["id"], forKey:Constants.UserID)
                        NSUserDefaults.standardUserDefaults().setObject(dict!, forKey:Constants.PROFILE)
                        let passKey = dict!["pwd_blank"]
                        self.firebaseAnalyticMethod("2")
                        
                        if passKey! == nil
                        {
                            NSUserDefaults.standardUserDefaults().setObject(NSNumber.init(int: 1), forKey:Constants.PASSWORD)
                        }
                        else
                        {
                            NSUserDefaults.standardUserDefaults().setObject(passKey, forKey:Constants.PASSWORD)
                        }
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            Constants.appDelegate.justLoggedIn = true
                            if  Constants.appDelegate.isRateView == true || Constants.appDelegate.isRecipeDetail == true
                            {
                                Constants.appDelegate.isRateView = false
                                Constants.appDelegate.isRecipeDetail = false
                                self.sideMenuObj.UpdateMenuView()
                                self.dismissViewControllerAnimated(true, completion: nil)
                            }
                            else
                            {
                                let viewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("SideMenuNavigation")
                                UIApplication.sharedApplication().keyWindow?.rootViewController = viewController;
                            }
                        })
                    }
                    else
                    {
                        CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":response["message"]!])
                    }
                })
            }
        })
    }
    
    
    
    func signIn(signIn: GIDSignIn!, didDisconnectWithUser user:GIDGoogleUser!,
                withError error: NSError!) {
        
        
//        print("Logging Out GOOGLE ")
        
    }

    
    func firebaseAnalyticMethod(authType:String)
    {
        if authType == "1"
        {
            FIRAnalytics.logEventWithName(kFIREventSelectContent, parameters: [
                kFIRParameterItemID:"9",
                kFIRParameterItemName:"Facebook",
                kFIRParameterContentType:"Facebook"
                ])
        }
        else if authType == "2"
        {
            FIRAnalytics.logEventWithName(kFIREventSelectContent, parameters: [
                kFIRParameterItemID:"10",
                kFIRParameterItemName:"Google",
                kFIRParameterContentType:"Google Plus"
                ])

        }
        else
        {
            FIRAnalytics.logEventWithName(kFIREventSelectContent, parameters: [
                kFIRParameterItemID:"7",
                kFIRParameterItemName:"Login",
                kFIRParameterContentType:"LogIn Success"
                ])

        }
    }
    
}
