//
//  ForgotPasswordView.swift
//  MyRecipeApp
//
//  Created by Surbhi on 26/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class ForgotPasswordView: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var CancelButton: UIButton!
    @IBOutlet weak var forgotView: UIView!
    
    @IBOutlet weak var changePassView: UIView!
    
    @IBOutlet weak var oldPassLabel: UILabel!
    @IBOutlet weak var oldPassText: UITextField!
    @IBOutlet weak var oldPassLine: UILabel!
    @IBOutlet weak var oldPassLineHeight: NSLayoutConstraint!

    @IBOutlet weak var newPasslabel: UILabel!
    @IBOutlet weak var newPassText: UITextField!
    @IBOutlet weak var newPassLine: UILabel!
    @IBOutlet weak var newPassLineHeight: NSLayoutConstraint!

    @IBOutlet weak var confirmPassLabel: UILabel!
    @IBOutlet weak var confirmPassText: UITextField!
    @IBOutlet weak var confirmPassLine: UILabel!
    @IBOutlet weak var confirmPassLineHeight: NSLayoutConstraint!
    var fontsze : CGFloat = (DeviceType.IS_IPAD ? 16.0 : 13.0)

    
    var isForgetView = Bool()
    
    // MARK:- START
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if isForgetView == true {
            forgotView.layer.shadowColor = Colors.ShadowColor.CGColor
            forgotView.layer.cornerRadius = 5.0
            forgotView.clipsToBounds = true
            forgotView.hidden = false
            changePassView.hidden = true
        }
        else {
            changePassView.layer.shadowColor = Colors.ShadowColor.CGColor
            changePassView.layer.cornerRadius = 5.0
            changePassView.clipsToBounds = true
            changePassView.hidden = false
            forgotView.hidden = true
            oldPassText.delegate = self
            newPassText.delegate = self
            confirmPassText.delegate = self
            
            oldPassLabel.hidden = true
            oldPassText.attributedPlaceholder = NSAttributedString(string: " Old Password",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: "Helvetica", size: self.fontsze)!])

            newPasslabel.hidden = true
            newPassText.attributedPlaceholder = NSAttributedString(string: " New Password",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: "Helvetica", size: self.fontsze)!])

            confirmPassLabel.hidden = true
            confirmPassText.attributedPlaceholder = NSAttributedString(string: " Confirm Password",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: "Helvetica", size: self.fontsze)!])
        }
    }
    
    // MARK:- ACTIONS FORGOTVIEw
    
    @IBAction func cancelPressed(sender: AnyObject) {
        emailTextField.resignFirstResponder()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func OkPressed(sender: AnyObject) {
        
        emailTextField.resignFirstResponder()
        let method = "users/forgot_password"
        let param = "email=\(emailTextField.text!)"
        
        if Reachability.isConnectedToNetwork() {
            
            Constants.appDelegate.startIndicator()
            Server.postRequestWithURL(Constants.BASEURL+method, paramString: param) { (response) in
                
                Constants.appDelegate.stopIndicator()
                if response[Constants.STATE] as! Int == 1 {
                    let alert = UIAlertController.init(title: "Culineria", message: response["message"]! as? String, preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                        self.dismissViewControllerAnimated(true, completion: nil)
                    }))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                else {
                    let alert = UIAlertController.init(title: "Culineria", message: response["message"]! as? String, preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                        self.emailTextField.becomeFirstResponder()
                    }))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }
        }
        else {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    @IBAction func FadeViewPressed(sender: AnyObject) {
        emailTextField.resignFirstResponder()

        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    // MARK:- ACTIONS CHANGE PASSWORD VIEW
    
    @IBAction func cancelPressed_Pass(sender: AnyObject) {
        newPassText.resignFirstResponder()
        confirmPassText.resignFirstResponder()
        oldPassText.resignFirstResponder()

        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func OkPressed_Pass(sender: AnyObject) {
        
        newPassText.resignFirstResponder()
        confirmPassText.resignFirstResponder()
        oldPassText.resignFirstResponder()
        
        if oldPassText.text == "" {
            
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message": "Enter your old password"] )
        }
        else if newPassText.text == "" {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message": "Enter your new password"] )
        }
        else if confirmPassText.text == "" {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message": "You confirm password is not matched to new password"] )
        }
        else if newPassText.text! != confirmPassText.text! {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message": "You confirm password is not matched to new password"] )
        }
        else {
            let passwordStrOLD = Helper.md5(string: oldPassText.text!)
            let passwordStrNEW = Helper.md5(string: newPassText.text!)
            
            let method = "users/change_password"
            let param = "user_id=\(NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID)!)&new_password=\(passwordStrNEW)&current_password=\(passwordStrOLD)"
            
            if Reachability.isConnectedToNetwork() {
                
                Constants.appDelegate.startIndicator()
                Server.postRequestWithURL(Constants.BASEURL+method, paramString: param) { (response) in
                    
                    Constants.appDelegate.stopIndicator()
                    if response[Constants.STATE] as! Int == 1
                    {
                        dispatch_async(dispatch_get_main_queue())
                        {
                            let alert = UIAlertController.init(title: "Culineria", message: response["message"]! as? String, preferredStyle: UIAlertControllerStyle.Alert)
                            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                                
                                self.logoutAfterDeactivate()
                                self.dismissViewControllerAnimated(true, completion: nil)
                                
                            }))
                            self.presentViewController(alert, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        dispatch_async(dispatch_get_main_queue())
                        {
                            let alert = UIAlertController.init(title: "Culineria", message: response["message"]! as? String, preferredStyle: UIAlertControllerStyle.Alert)
                            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                            }))
                            self.presentViewController(alert, animated: true, completion: nil)
                        }
                    }
                }
            }
            else {
                CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
            }
        }
    }
    
    // MARK: TEXTFIELD DELEGATE
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if textField == oldPassText {
            UIView.animateWithDuration(1.0, delay: 0, options: UIViewAnimationOptions.TransitionFlipFromBottom, animations: {
                self.oldPassLabel.hidden = false
                self.oldPassText.attributedPlaceholder = NSAttributedString(string: "")
                
                Helper.activatetextField(self.oldPassLine!,height: self.oldPassLineHeight!)
                }, completion: { (true) in
                    self.oldPassLabel.hidden = false
            })
        }
        if textField == newPassText {
            UIView.animateWithDuration(1.0, delay: 0, options: UIViewAnimationOptions.TransitionFlipFromBottom, animations: {
                self.newPasslabel.hidden = false
                self.newPassText.attributedPlaceholder = NSAttributedString(string: "")
                
                Helper.activatetextField(self.newPassLine,height: self.newPassLineHeight)
                }, completion: { (true) in
                    self.newPasslabel.hidden = false
            })
        }
        if textField == confirmPassText {
            UIView.animateWithDuration(1.0, delay: 0, options: UIViewAnimationOptions.TransitionFlipFromBottom, animations: {
                self.confirmPassLabel.hidden = false
                self.confirmPassText.attributedPlaceholder = NSAttributedString(string: "")
                
                Helper.activatetextField(self.confirmPassLine,height: self.confirmPassLineHeight)
                }, completion: { (true) in
                    self.confirmPassLabel.hidden = false
            })
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if textField == oldPassText {
            if oldPassText.text == nil || oldPassText.text == "" {
                UIView.animateWithDuration(1.0, animations: {
                    self.oldPassLabel.hidden = true
                    Helper.deActivatetextField(self.oldPassLine,height: self.oldPassLineHeight)
                    self.oldPassText.attributedPlaceholder = NSAttributedString(string: " Old Password",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: "Helvetica", size: self.fontsze)!])
                    }, completion: nil)
            }
            else
            {
                self.oldPassLabel.hidden = false
            }
        }
        if textField == newPassText {
            if newPassText.text == nil || newPassText.text == "" {
                UIView.animateWithDuration(1.0, animations: {
                    self.newPasslabel.hidden = true
                    Helper.deActivatetextField(self.newPassLine,height: self.newPassLineHeight)
                    self.newPassText.attributedPlaceholder = NSAttributedString(string: " New Password",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: "Helvetica", size: self.fontsze)!])
                    }, completion: nil)
            }
            else
            {
                self.newPasslabel.hidden = false
            }
        }
        if textField == confirmPassText {
            if confirmPassText.text == nil || confirmPassText.text == "" {
                UIView.animateWithDuration(1.0, animations: {
                    self.confirmPassLabel.hidden = true
                    Helper.deActivatetextField(self.confirmPassLine,height: self.confirmPassLineHeight)
                    self.confirmPassText.attributedPlaceholder = NSAttributedString(string: " Confirm Password",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: "Helvetica", size: self.fontsze)!])
                    }, completion: nil)
            }
            else
            {
                self.newPasslabel.hidden = false
            }
        }

    }

    func logoutAfterDeactivate() {
        
        NSNotificationCenter.defaultCenter().postNotificationName("PasswordChanged", object: nil)
        self.dismissViewControllerAnimated(true, completion: nil)        
    }
}
