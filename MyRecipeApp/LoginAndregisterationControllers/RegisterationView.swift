//
//  RegisterationView.swift
//  MyRecipeApp
//
//  Created by Surbhi on 26/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit
import Firebase

class RegisterationView: UIViewController, UITextFieldDelegate {

    // MARK: IBOutlets Defined
    @IBOutlet weak var logoText: UIImageView!
    @IBOutlet weak var logoHeight: NSLayoutConstraint!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var lineName: UILabel!
    @IBOutlet weak var nameHeight: NSLayoutConstraint!

    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var lineEmail: UILabel!
    @IBOutlet weak var emailHeight: NSLayoutConstraint!

    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var mobileText: UITextField!
    @IBOutlet weak var lineMobile: UILabel!
    @IBOutlet weak var mobileHeight: NSLayoutConstraint!

    @IBOutlet weak var passLabel: UILabel!
    @IBOutlet weak var passText: UITextField!
    @IBOutlet weak var linePass: UILabel!
    @IBOutlet weak var passHeight: NSLayoutConstraint!

    @IBOutlet weak var confirmPassLabel: UILabel!
    @IBOutlet weak var confirmPassText: UITextField!
    @IBOutlet weak var lineConfirmPass: UILabel!
    @IBOutlet weak var confirmPassHeight: NSLayoutConstraint!

    @IBOutlet weak var termsTextButton: UIButton!
    @IBOutlet weak var termsAcceptButton: UIButton!

    var fontsze : CGFloat = (DeviceType.IS_IPAD ? 16.0 : 13.0)

    
    var authType: Int = 0
    var indexToredirect : Int?
    
    
    // MARK: Start
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        nameText.attributedPlaceholder = NSAttributedString(string: "Name*",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: "Helvetica", size: self.fontsze)!])
        emailText.attributedPlaceholder = NSAttributedString(string: "Email id*",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: "Helvetica", size: self.fontsze)!])
        mobileText.attributedPlaceholder = NSAttributedString(string: "Mobile No",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: "Helvetica", size: self.fontsze)!])
        passText.attributedPlaceholder = NSAttributedString(string: "Password*",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: "Helvetica", size: self.fontsze)!])
        confirmPassText.attributedPlaceholder = NSAttributedString(string: "Confirm Password*",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: "Helvetica", size: self.fontsze)!])
        

        let attrs = [ NSFontAttributeName : UIFont(name: "Helvetica", size: self.fontsze)!,
                      NSForegroundColorAttributeName : Colors.Apptextcolor,
                      NSUnderlineStyleAttributeName : 1]
        let  attributedString1 = NSMutableAttributedString(string:"I accept terms & conditions", attributes:attrs)
        termsTextButton.setAttributedTitle(attributedString1, forState: .Normal)
        
//        let logoviewWidth =  ScreenSize.SCREEN_WIDTH - 20.0
//        let ratio = logoviewWidth/300
//        logoHeight.constant = ratio * 150
        
    }
    

    // MARK: TEXTFIELD DELEGATE
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if textField == nameText {
            ACTIVATETextField(nameText, textLabel: nameLabel, line: lineName, height: nameHeight)
        }
        else if textField == emailText {
            ACTIVATETextField(emailText, textLabel: emailLabel, line: lineEmail, height: emailHeight)
        }
        else if textField == mobileText {
            ACTIVATETextField(mobileText, textLabel: mobileLabel, line: lineMobile, height: mobileHeight)
        }
        else if textField == passText {
            ACTIVATETextField(passText, textLabel: passLabel, line: linePass, height: passHeight)
        }
        else if textField == confirmPassText {
            ACTIVATETextField(confirmPassText, textLabel: confirmPassLabel, line: lineConfirmPass, height: confirmPassHeight)
        }
    }
    
    func ACTIVATETextField(textf : UITextField,textLabel : UILabel , line: UILabel, height : NSLayoutConstraint)
    {
         UIView.animateWithDuration(1.0, delay: 0, options: UIViewAnimationOptions.TransitionFlipFromBottom, animations: {
            
            textLabel.hidden = false
            textf.attributedPlaceholder = NSAttributedString(string: "")
            Helper.activatetextField(line,height: height)

            },
            completion: { (true) in
                textLabel.hidden = false
         })
    }
    
    
    func DEACTIVATETextField(textf : UITextField,textLabel : UILabel , line: UILabel, height : NSLayoutConstraint)
    {
        if textf.text == ""
        {
            UIView.animateWithDuration(1.0, animations: {
                textLabel.hidden = true
                
                Helper.deActivatetextField(line,height: height)
                
                textf.attributedPlaceholder = NSAttributedString(string: textLabel.text! ,  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: "Helvetica", size: self.fontsze)!])
                }, completion: nil)
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if textField == nameText {
            DEACTIVATETextField(nameText, textLabel: nameLabel, line: lineName, height: nameHeight)
        }
        else if textField == emailText {
            DEACTIVATETextField(emailText, textLabel: emailLabel, line: lineEmail, height: emailHeight)
        }
        else if textField == mobileText {
            DEACTIVATETextField(mobileText, textLabel: mobileLabel, line: lineMobile, height: mobileHeight)
        }
        else if textField == passText {
            DEACTIVATETextField(passText, textLabel: passLabel, line: linePass, height: passHeight)
        }
        else if textField == confirmPassText {
            DEACTIVATETextField(confirmPassText, textLabel: confirmPassLabel, line: lineConfirmPass, height: confirmPassHeight)
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == mobileText {
            
            if (range.location > 9)
            {
                return false
            }
        }
        
        return true
    }

    // MARK: BUTTON ACTIONS
    
    @IBAction func AcceptTermsAndConditionsPressed(sender: AnyObject) {
        
        if termsAcceptButton.selected {
            termsAcceptButton.selected = false
        }
        else{
            termsAcceptButton.selected = true
        }
    }
    
    @IBAction func OpenTermsandConditionView(sender: AnyObject) {
        
        let dest = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("TermsConditionVC") as! TermsConditionVC
        dest.isComingFromSideMenu = false
        self.presentViewController(dest, animated: true, completion: nil)
    }
    
    @IBAction func SignUpPressed(sender: AnyObject) {
        
        nameText.resignFirstResponder()
        passText.resignFirstResponder()
        emailText.resignFirstResponder()
        confirmPassText.resignFirstResponder()

        
        if nameText.text == "" {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"", "message":"Enter User Name !"])
        }
        else if emailText.text == ""{
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"", "message":"Enter Email Id !"])
        }
        else if !Helper.isValidEmail(emailText.text!){
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"", "message":"Enter Valid Email Id !"])
        }
        else if passText.text == ""{
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"", "message":"Enter Password !"])
        }
        else if confirmPassText.text == ""{
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"", "message":"Enter Confirm Password !"])
        }
        else if confirmPassText.text != passText.text{
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"", "message":"Confirm Password does not match."])
        }
        else if termsAcceptButton.selected == false{
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"", "message":"Please agree with terms & conditions"])
        }
        else {
            if mobileText.text == ""{
             
                self.RegisterMethodCalled()
            }
            else
            {
                if !Helper.isValidPhoneNumber(mobileText.text!) {
                    CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"", "message":"Enter Valid Mobile No !"])
                }
                else
                {
                    self.RegisterMethodCalled()
                }
            }
        }
    }
    
    
    func RegisterMethodCalled()
    {
        if Reachability.isConnectedToNetwork() {
            
            let nameStr = nameText.text! as String
            Constants.appDelegate.startIndicator()
            Server.RegisterationForApp(nameStr, email: emailText.text!, mobile: mobileText.text!, authType: "", authId: "", pass: passText.text!, profileImage: "", completionHandler: { (response) in
                
                Constants.appDelegate.stopIndicator()
                if response[Constants.STATE] as! Int == 1 {
                    let dict = response["data"]
                    
                    NSUserDefaults.standardUserDefaults().setObject(dict!["id"], forKey:Constants.UserID)
                    NSUserDefaults.standardUserDefaults().setObject(dict!, forKey:Constants.PROFILE)
                    let passKey = dict!["pwd_blank"]
                    
                    if passKey! == nil
                    {
                        NSUserDefaults.standardUserDefaults().setObject(NSNumber.init(int: 0), forKey:Constants.PASSWORD)
                    }
                    else
                    {
                        NSUserDefaults.standardUserDefaults().setObject(passKey, forKey:Constants.PASSWORD)
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        Constants.appDelegate.justLoggedIn = true
                        self.firebaseAnalyticMethod()
                        AppsFlyerTracker.sharedTracker().trackEvent("Registered", withValue: "Success") // APPSFlyer

                        if  Constants.appDelegate.isRateView == true
                        {
                            self.dismissViewControllerAnimated(false, completion: nil)
                        }
                        else
                        {
                            let viewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("SideMenuNavigation") as! UINavigationController
                            UIApplication.sharedApplication().keyWindow?.rootViewController = viewController;
                        }
                    })
                }
                else {
                    CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes: ["title":"",
                        "message":response["message"]!])
                }
            })
        }
        else {
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    @IBAction func GoToLoginViewPressed(sender: AnyObject) {
        nameText.resignFirstResponder()
        passText.resignFirstResponder()
        emailText.resignFirstResponder()
        confirmPassText.resignFirstResponder()
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    
    func firebaseAnalyticMethod()
    {
        FIRAnalytics.logEventWithName(kFIREventSelectContent, parameters: [
            kFIRParameterItemID:"8",
            kFIRParameterItemName:"Register",
            kFIRParameterContentType:"Registeration Success"
            ])
    }
}
