//
//  Server.swift
//  TestSwiftApp
//
//  Created by Surbhi on 21/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit
//import AFNetworking
//import Alamofire


class Server: NSObject {
    
    static func getRequestWithURL(urlString: String, completionHandler:(response: Dictionary <String, AnyObject>) -> Void) {
        
        let defaultConfigObject = NSURLSessionConfiguration.defaultSessionConfiguration()
        let defaultSession = NSURLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        
        let urlRequest = NSMutableURLRequest(URL: NSURL(string: urlString)!)
        urlRequest.HTTPMethod = "GET"
        
        // Handling Basic HTTPS Authorization
        let authString = "whirlpoolapps@gmail.com:Neuro@1009#@!"
        let authData = authString.dataUsingEncoding(NSUTF8StringEncoding)
        let authValue = "Basic \(authData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength))"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
        
        let task = defaultSession.dataTaskWithRequest(urlRequest) { (let data, let response, let error) -> Void in
            
            if error != nil {
                print("Error occurred: "+(error?.localizedDescription)!)
                Constants.appDelegate.stopIndicator()
                return;
            }
            do {
                    let responseObjc = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! [String: AnyObject]
                    completionHandler(response: responseObjc)
            }
            catch {
                print("Error occurred parsing data: \(error)")
                completionHandler(response: [:])
            }
        }
        task.resume()
    }
    
    static func postRequestWithURL(urlString: String, paramString: String, completionHandler:(response: Dictionary <String, AnyObject>) -> Void) {
        
        let defaultConfigObject = NSURLSessionConfiguration.defaultSessionConfiguration()
        let defaultSession = NSURLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        
        let urlRequest = NSMutableURLRequest(URL: NSURL(string: urlString)!)
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = paramString.dataUsingEncoding(NSUTF8StringEncoding)

        // Handling Basic HTTPS Authorization
        let authString = "whirlpoolapps@gmail.com:Neuro@1009#@!"
        let authData = authString.dataUsingEncoding(NSUTF8StringEncoding)
        let authValue = "Basic \(authData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength))"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
        
        let task = defaultSession.dataTaskWithRequest(urlRequest) { (let data, let response, let error) -> Void in
            
            if error != nil {
                print("Error occurred: "+(error?.localizedDescription)!)
                Constants.appDelegate.stopIndicator()
                return;
            }
            
            do {
                print("SUCCESS")
                
                let responseObjc = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as! [String: AnyObject]
                completionHandler(response: responseObjc)
            }
            catch {
                print("Error occurred parsing data: \(error)")
                completionHandler(response: [:])
            }
        }
        task.resume()
    }
    
    static func getGooglePlusProfileImage (urlString: String,  completionHandler:(response: Dictionary <String, AnyObject>) -> Void)
    {
        let defaultConfigObject = NSURLSessionConfiguration.defaultSessionConfiguration()
        let defaultSession = NSURLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        let urlRequest = NSMutableURLRequest(URL: NSURL(string: urlString)!)
        let task = defaultSession.dataTaskWithRequest(urlRequest) { (let data, let response, let error) -> Void in
            
            
            if error != nil {
                print("Error occurred: "+(error?.localizedDescription)!)
                Constants.appDelegate.stopIndicator()
                return;
            }
            
            do {
                let responseObjc = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as! [String: AnyObject]
                completionHandler(response: responseObjc)
            }
            catch {
                print("Error occurred parsing data: \(error)")
                completionHandler(response: [:])
            }
        }
        task.resume()
    }
    
    
    
    static func LoginToApp(email : String, authType : String , authId : String , pass : String , completionHandler:(response: Dictionary <String, AnyObject>) -> Void)
    {
        
        let defaultConfigObject = NSURLSessionConfiguration.defaultSessionConfiguration()
        let defaultSession = NSURLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        var paramString = String()
        
        if authType == "1" // FaceBook
        {
            paramString = "email=\(email)&password=&auth_type=1&auth_id=\(authId)"
        }
        else if authType == "2" // Google
        {
            paramString = "email=\(email)&password=&auth_type=2&auth_id=\(authId)"
        }
        else // Normal
        {
            let passwordStr = Helper.md5(string: pass)
            paramString = "email=\(email)&password=\(passwordStr)&auth_type=0&auth_id="
        }
        
        let loginMethod = "users/login"
        let urlString = Constants.BASEURL+loginMethod

        let urlRequest = NSMutableURLRequest(URL: NSURL(string: urlString)!)
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = paramString.dataUsingEncoding(NSUTF8StringEncoding)

        
        // Handling Basic HTTPS Authorization
        let authString = "whirlpoolapps@gmail.com:Neuro@1009#@!"
        let authData = authString.dataUsingEncoding(NSUTF8StringEncoding)
        let authValue = "Basic \(authData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength))"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
        
        let task = defaultSession.dataTaskWithRequest(urlRequest) { (let data, let response, let error) -> Void in
            
            if error != nil {
                print("Error occurred: "+(error?.localizedDescription)!)
                Constants.appDelegate.stopIndicator()
                return;
            }
            
            do {
                let responseObjc = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as! [String: AnyObject]
                completionHandler(response: responseObjc)
            }
            catch {
                print("Error occurred parsing data: \(error)")
                completionHandler(response: [:])
            }
        }
        task.resume()
    }
    
    
    static func RegisterationForApp(name : String, email : String, mobile: String, authType : String , authId : String , pass : String ,profileImage : String, completionHandler:(response: Dictionary <String, AnyObject>) -> Void)
    {
        
        
        let defaultConfigObject = NSURLSessionConfiguration.defaultSessionConfiguration()
        let defaultSession = NSURLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        var paramString = String()
        
        let customAllowedSet =  NSCharacterSet(charactersInString:"&").invertedSet
        let escapedString = profileImage.stringByAddingPercentEncodingWithAllowedCharacters(customAllowedSet)
        
        if authType == "1" // FaceBook
        {
            paramString = "name=\(name)&email=\(email)&mobile=&password=&auth_type=1&auth_id=\(authId)&device_type=ios&device_id=\(AppDelegate.getUniqueDeviceIdentifierAsString())&device_token=\(AppDelegate.getDeviceToken())&device_model=\(UIDevice.currentDevice().model)&device_version=\(UIDevice.currentDevice().systemVersion)&profile_pic=\(escapedString!)"
        }
        else if authType == "2" // Google
        {
            paramString = "name=\(name)&email=\(email)&mobile=&password=&auth_type=2&auth_id=\(authId)&device_type=ios&device_id=\(AppDelegate.getUniqueDeviceIdentifierAsString())&device_token=\(AppDelegate.getDeviceToken())&device_model=\(UIDevice.currentDevice().model)&device_version=\(UIDevice.currentDevice().systemVersion)&profile_pic=\(escapedString!)"
        }
        else // Normal
        {
            let passwordStr = Helper.md5(string: pass)
            paramString = "name=\(name)&email=\(email)&mobile=\(mobile)&password=\(passwordStr)&auth_type=0&auth_id=&device_type=ios&device_id=\(AppDelegate.getUniqueDeviceIdentifierAsString())&device_token=\(AppDelegate.getDeviceToken())&device_model=\(UIDevice.currentDevice().model)&device_version=\(UIDevice.currentDevice().systemVersion)&profile_pic="
        }
        
        let signUpMethod = "users/create"
        let urlString = Constants.BASEURL+signUpMethod

        let urlRequest = NSMutableURLRequest(URL: NSURL(string: urlString)!)
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = paramString.dataUsingEncoding(NSUTF8StringEncoding)
        
        // Handling Basic HTTPS Authorization
        let authString = "whirlpoolapps@gmail.com:Neuro@1009#@!"
        let authData = authString.dataUsingEncoding(NSUTF8StringEncoding)
        let authValue = "Basic \(authData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength))"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
        urlRequest.setValue( "application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")

        let task = defaultSession.dataTaskWithRequest(urlRequest) { (let data, let response, let error) -> Void in
            
            if error != nil {
                print("Error occurred: "+(error?.localizedDescription)!)
                Constants.appDelegate.stopIndicator()
                return;
            }
            
            do {
                let responseObjc = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as! [String: AnyObject]
                completionHandler(response: responseObjc)
            }
            catch {
                print("Error occurred parsing data: \(error)")
                completionHandler(response: [:])
            }
        }
        task.resume()
    }

    
    
    static func getNotificationForApp( completionHandler:(response: Dictionary <String, AnyObject>) -> Void)
    {
        let defaultConfigObject = NSURLSessionConfiguration.defaultSessionConfiguration()
        let defaultSession = NSURLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        let url = "\(Constants.BASEURL)notifications"

        let urlRequest = NSMutableURLRequest(URL: NSURL(string: url)!)
        urlRequest.HTTPMethod = "GET"
        
        // Handling Basic HTTPS Authorization
        let authString = "whirlpoolapps@gmail.com:Neuro@1009#@!"
        let authData = authString.dataUsingEncoding(NSUTF8StringEncoding)
        let authValue = "Basic \(authData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength))"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
        
        let task = defaultSession.dataTaskWithRequest(urlRequest) { (let data, let response, let error) -> Void in
            
            if error != nil {
                print("Error occurred: "+(error?.localizedDescription)!)
                Constants.appDelegate.stopIndicator()
                return;
            }
            
            do {
                let responseObjc = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! [String: AnyObject]
                
                completionHandler(response: responseObjc)
            }
            catch {
                print("Error occurred parsing data: \(error)")
            }
        }
        task.resume()
    }
    
}



