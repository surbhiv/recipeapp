//
//  AppDelegate.swift
//  MyRecipeApp
//
//  Created by Surbhi on 24/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit
import Firebase
import Google
import Fabric
import Crashlytics
//import apprater
import Siren


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate{

    var window: UIWindow?
    var deviceTokenString = String()
    var activityView1  = UIView()
    var imageForR = UIImageView ()
    var load = UILabel()
    var isSafariOpen : Bool = false
    var justLoggedIn : Bool = false
    var isRateView : Bool = false
    var justStarted : Bool = true
    var isRecipeDetail : Bool = false
    var notiCount = Int()
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        if self.window == nil {
            print("nil window")
            self.window = UIWindow.init(frame: UIScreen.mainScreen().bounds)
        }
        let root =  Constants.mainStoryboard.instantiateViewControllerWithIdentifier("SplashView") as! SplashView
        self.window?.rootViewController = root
        
        let notificationType: UIUserNotificationType = [UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound]
        let pushNotificationSettings = UIUserNotificationSettings(forTypes: notificationType, categories: nil)
        application.registerUserNotificationSettings(pushNotificationSettings)
        application.registerForRemoteNotifications()
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        
        //App Rating
        let appRater = APAppRater.sharedInstance
        let apID : String = (NSBundle.mainBundle().objectForInfoDictionaryKey(kCFBundleIdentifierKey as String) as? String) ?? "" //CFBundleIdentifier
        appRater.appId = apID
        
        //CRASHLYTICS
        Fabric.with([Crashlytics.self])

        // Initialize sign-in with Google
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        GIDSignIn.sharedInstance().clientID = FIRApp.defaultApp()?.options.clientID//"10775809234-i2pq3o3o7aq5mh2qt3984bskh7mrr6bl.apps.googleusercontent.com" // Here You Have To Change Your App ID
    
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(loadNotification), name: Constants.pushNotification, object: nil)
        loadNotification()
        
        self.window?.makeKeyAndVisible()
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        self.createIndicator()
        self.setupSiren()
        
        
        //APPFLYERS
        AppsFlyerTracker.sharedTracker().appsFlyerDevKey = "7248xRffvRhDkfqgnKamGS"
        AppsFlyerTracker.sharedTracker().appleAppID = "1185192155"
        AppsFlyerTracker.sharedTracker().trackAppLaunch()

        return true
    }

    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
         Siren.sharedInstance.checkVersion(.Immediately)
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
        Siren.sharedInstance.checkVersion(.Daily)
        
        AppsFlyerTracker.sharedTracker().trackAppLaunch()
        

    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
        
    }

    //MARK:- UNIQUE Identifier
    static func getUniqueDeviceIdentifierAsString() -> String {
        
        let appName = NSBundle.mainBundle().infoDictionary![kCFBundleNameKey as String] as! String
        
        if let appUUID = SSKeychain.passwordForService(appName, account: "com.WhirlpoolCulineria") {//"com.neuronimbus.MyRecipeApp"
            return appUUID
        }
        else {
            
            let appUUID = UIDevice.currentDevice().identifierForVendor?.UUIDString
            SSKeychain.setPassword(appUUID, forService: appName, account: "com.WhirlpoolCulineria")//"com.neuronimbus.MyRecipeApp"
            return appUUID!
        }
        
//        [AppsFlyerTracker sharedTracker].customerUserID = @"YOUR_CUSTOM_DEVICE_ID";

    }
    
    
    //MARK: - PushNotificationDelegate Methods
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {

        deviceTokenString = deviceToken.description.componentsSeparatedByCharactersInSet(NSCharacterSet.alphanumericCharacterSet().invertedSet).joinWithSeparator("")

        if deviceTokenString == "" || deviceTokenString.isEmpty == true {
            deviceTokenString = "6aa2790400e270717622b325f412951bd8b3183c9b4a099c497681b8ea2dd8db"
        }
        
        AppsFlyerTracker.sharedTracker().registerUninstall(deviceToken)
        AppsFlyerTracker.sharedTracker().useUninstallSandbox = false
        
        NSUserDefaults.standardUserDefaults().setObject(deviceTokenString, forKey: "deviceToken")
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print(error)
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        savePushMessages(userInfo["aps"] as! [String: AnyObject])
        loadNotification()

    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        savePushMessages(userInfo["aps"] as! [String: AnyObject])
        loadNotification()
        
    }

    func savePushMessages(notificationDic:[String: AnyObject]) {
        
        var msgArray = NSMutableArray()
        let prefs = NSUserDefaults.standardUserDefaults()
        
        if let array = prefs.objectForKey("PUSH_MESSAGES") {
            msgArray = array as! NSMutableArray
        }
        msgArray.insertObject(notificationDic, atIndex: 0)
        prefs.setObject(msgArray, forKey: "PUSH_MESSAGES")
        
        var badgeValue: Int = prefs.integerForKey("BADGE_COUNT")
        badgeValue = badgeValue + 1
        prefs.setInteger(badgeValue, forKey: "BADGE_COUNT")
        
        prefs.synchronize()
        NSNotificationCenter.defaultCenter().postNotificationName(Constants.pushNotification, object: nil)
    }

    func loadNotification() {
        if Reachability.isConnectedToNetwork() {
            
            Server.getNotificationForApp({ (response) in
                
                if response[Constants.STATE] as! Int == 1 {
                    
                    let arr = response["data"] as! Array<AnyObject>
                    let count = arr.count
                    
                    
                    let oldCount = NSUserDefaults.standardUserDefaults().integerForKey("notiCount")

                    if count > 0
                    {
                        NSUserDefaults.standardUserDefaults().setInteger(count, forKey: "NotificationCount")
                        
                    }
                    else
                    {
                        NSUserDefaults.standardUserDefaults().setInteger(oldCount, forKey: "NotificationCount")
                    }
                    
                    NSNotificationCenter.defaultCenter().postNotificationName(Constants.pushNotificationToUpdate, object: nil)
                }
                else {
                    
                }
            })
        }
        else {
        }
    }

    
    
    //MARK:- DEVICE Token
    static func getDeviceToken() -> String {
        var str = NSUserDefaults.standardUserDefaults().stringForKey("deviceToken")
        if str == nil || (str?.isEmpty)! {
            str = "6aa2790400e270717622b325f412951bd8b3183c9b4a099c497681b8ea2dd8db"
        }
        
        return str!
    }
    
    // DEEP LINK
    func application(application: UIApplication, continueUserActivity userActivity: NSUserActivity, restorationHandler: ([AnyObject]?) -> Void) -> Bool {
        AppsFlyerTracker.sharedTracker().continueUserActivity(userActivity, restorationHandler: restorationHandler)
        return true
    }// DEEP LINK
    
    

    //MARK:- OPEN URL
    func application(application: UIApplication, openURL url: NSURL, options: [String: AnyObject]) -> Bool {
        
        GIDSignIn.sharedInstance().handleURL(url, sourceApplication: options[UIApplicationOpenURLOptionsSourceApplicationKey] as? String, annotation: options[UIApplicationOpenURLOptionsAnnotationKey])
        FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: options[UIApplicationOpenURLOptionsSourceApplicationKey] as? String, annotation: options[UIApplicationOpenURLOptionsAnnotationKey])
        AppsFlyerTracker.sharedTracker().handleOpenUrl(url, options: options) // DEEP LINK

        return true
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        GIDSignIn.sharedInstance().handleURL(url, sourceApplication: sourceApplication, annotation: annotation)
        FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
        AppsFlyerTracker.sharedTracker().handleOpenURL(url, sourceApplication: sourceApplication, withAnnotation: annotation)// DEEP LINK

        return true

    }

    //MARK: PUSH NOTIFICATION
    func registerForPush()
    {
        var userID = String()

        if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == "" || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil
        {
            userID = ""
        }
        else
        {
            userID = NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID)!
        }
        
        if Reachability.isConnectedToNetwork() {
            SVProgressHUD.show()
            let method = "users/register_device"

            
            let param = "user_id=\(userID)&device_type=ios&device_id=\(AppDelegate.getUniqueDeviceIdentifierAsString())&device_token=\(AppDelegate.getDeviceToken())&device_model=\(UIDevice.currentDevice().model)&device_version=\(UIDevice.currentDevice().systemVersion)"
            
            AppsFlyerTracker.sharedTracker().customerUserID = AppDelegate.getUniqueDeviceIdentifierAsString(); // APPSFlYER 

            Server.postRequestWithURL(Constants.BASEURL+method, paramString: param, completionHandler: { (response) in
                
                SVProgressHUD.dismiss()
                if response[Constants.STATE] as! Int == 1 {
                }
                else {
                }
            })
        }
        else  {
            print("REGISTER DEVICE FAILURE")
            SVProgressHUD.dismiss()
        }
    }
    
    
    
    //MARK:- INDICATOR VIEW
    func createIndicator()
    {
        activityView1 = UIView.init(frame: CGRectMake(0, 0, ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT))
        activityView1.backgroundColor = UIColor.clearColor()

        let back = UIImageView.init(frame: CGRectMake(0, 0, ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT))
        back.backgroundColor = UIColor.blackColor()
        back.alpha = 0.5
        activityView1.addSubview(back)
        
        let bgView = UIView.init(frame: CGRectMake((ScreenSize.SCREEN_WIDTH - 120)/2,(ScreenSize.SCREEN_HEIGHT - 150)/2, 120,150))
        bgView.backgroundColor = Colors.appNavigationColor
        bgView.layer.cornerRadius = 18.0
        bgView.layer.shadowColor = Colors.Lighttextcolor.CGColor
        bgView.clipsToBounds = true

        let bgImage = UIImageView.init(frame: CGRectMake(36,36, 48,48))
        bgImage.backgroundColor = UIColor.clearColor()
        bgImage.image = UIImage.init(named: "loader_icon.png")

        
        imageForR = UIImageView.init(frame: CGRectMake(25,25, 70,70))
        imageForR.image = UIImage.init(named: "mouse.png")
        imageForR.backgroundColor = UIColor.clearColor()
        
        
        load = UILabel.init(frame: CGRectMake(0, 120, 120, 25))
        load.text = "LOADING..."
        load.textColor = UIColor.whiteColor()
        load.font = UIFont.boldSystemFontOfSize(14.0)
        load.textAlignment = NSTextAlignment.Center
        
        bgView.addSubview(imageForR)
        bgView.addSubview(bgImage)
        bgView.addSubview(load)
        
        activityView1.addSubview(bgView)
        print(self.window)
        self.window?.addSubview(activityView1)
        self.window?.sendSubviewToBack(activityView1)
    }
   
    func startIndicator()
    {
        
        dispatch_async(dispatch_get_main_queue()) {
            self.window!.addSubview(self.activityView1)
            self.rotate360Degrees()
        }
    }
    
    func stopIndicator()
    {
        dispatch_async(dispatch_get_main_queue()) {
            self.imageForR.layer.removeAllAnimations()
            self.window?.willRemoveSubview(self.activityView1)
            self.activityView1.removeFromSuperview()
        }
    }
    
    func rotate360Degrees(duration: CFTimeInterval = 3.5, completionDelegate: AnyObject? = nil)
    {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(M_PI * -2.0)
        rotateAnimation.duration = duration
        rotateAnimation.repeatCount = 25
        
        if let delegate: AnyObject = completionDelegate {
            rotateAnimation.delegate = delegate
        }
        imageForR.layer.addAnimation(rotateAnimation, forKey: nil)
    }
    
    
    
    //MARK:- SIREN - APp version Updation
    func setupSiren() {
        
        let siren = Siren.sharedInstance
        
        // Optional
        siren.delegate = self
        
        // Optional
        siren.debugEnabled = true
        
        // Optional - Defaults to .Option
        //        siren.alertType = .Option // or .Force, .Skip, .None
        // Optional - Can set differentiated Alerts for Major, Minor, Patch, and Revision Updates (Must be called AFTER siren.alertType, if you are using siren.alertType)
        siren.majorUpdateAlertType = .Option
        siren.minorUpdateAlertType = .Option
        siren.patchUpdateAlertType = .Option
        siren.revisionUpdateAlertType = .Option
        siren.forceLanguageLocalization = SirenLanguageType.English
        siren.alertType = SirenAlertType.Skip
        
        // Optional - Sets all messages to appear in Spanish. Siren supports many other languages, not just English and Russian.
        //        siren.forceLanguageLocalization = .Russian
        // Optional - Set this variable if your app is not available in the U.S. App Store. List of codes: https://developer.apple.com/library/content/documentation/LanguagesUtilities/Conceptual/iTunesConnect_Guide/Appendices/AppStoreTerritories.html
        //        siren.countryCode = ""
        // Optional - Set this variable if you would only like to show an alert if your app has been available on the store for a few days. The number 5 is used as an example.
        //        siren.showAlertAfterCurrentVersionHasBeenReleasedForDays = 5
        // Required
        siren.checkVersion(SirenVersionCheckType.Daily)
        
        
    }
}




extension AppDelegate: SirenDelegate
{
    func sirenDidShowUpdateDialog(alertType: SirenAlertType) {
        print(#function, alertType)
    }
    
    func sirenUserDidCancel() {
        print(#function)
    }
    
    func sirenUserDidSkipVersion() {
        print(#function)
    }
    
    func sirenUserDidLaunchAppStore() {
        print(#function)
    }
    
    func sirenDidFailVersionCheck(error: NSError) {
        print(#function, error)
    }
    
    func sirenLatestVersionInstalled() {
        print(#function, "Latest version of app is installed")
    }
    
    /**
     This delegate method is only hit when alertType is initialized to .None
     */
    func sirenDidDetectNewVersionWithoutAlert(message: String) {
        print(#function, "\(message)")
    }
}