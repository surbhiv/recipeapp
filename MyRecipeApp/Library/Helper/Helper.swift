//
//  Helper.swift
//  NBA
//
//  Created by HiteshDhawan on 12/04/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit


class Helper: NSObject {

    static func isValidEmail(emailString: String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPredicate.evaluateWithObject(emailString)
        
    }
    
    static func isValidPhoneNumber(phoneNumber: String) -> Bool {
        
        let numberOnly = NSCharacterSet.init(charactersInString: "0123456789")
        let charactersetFromTF = NSCharacterSet.init(charactersInString: phoneNumber)
        let stringIsValid = numberOnly.isSupersetOfSet(charactersetFromTF)

        if phoneNumber.characters.count == 10 && stringIsValid == true{
            return true
        }
        return false
    }
    
    static func setImageOntextField(textField: UITextField){
        
        let imageV = UIImageView(frame:CGRectMake(0, 0, 15, 15))
        imageV.image = UIImage.init(named: "downArrow")
        textField.rightViewMode = UITextFieldViewMode.Always
        textField.rightView = imageV
    }
    
    static func activatetextField(lineLabel:UILabel, height : NSLayoutConstraint){
        
        lineLabel.backgroundColor = Colors.appThemeColor
        height.constant = 2.0
    }
    
    static func deActivatetextField(lineLabel:UILabel, height : NSLayoutConstraint){
        
        lineLabel.backgroundColor = Colors.Apptextcolor
        height.constant = 1.0
        
    }
    
    
    static func loadImageFromUrl(urls: String, view: UIImageView, width:CGFloat){
        
        // Create Url from string
        
        if  urls == ""{
            
            view.image = UIImage.init(named: "no_image.png")
        }
        else
        {
            let url = NSURL(string: urls)!

            
            let task = NSURLSession.sharedSession().dataTaskWithURL(url) { (responseData, responseUrl, error) -> Void in

                if let data = responseData{
                    
                    // execute in UI thread
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        let img = UIImage(data: data)

                        if img != nil
                        {
                            let newImg = self.resizeImage(img!, newWidth: width)
                            view.image = newImg
                        }
                        else
                        {
                            view.image = UIImage.init(named: "profile_PlaceHolder.png")
                        }
                    })
                }
            }
            
            // Run task
            task.resume()
        }
    }
    
    
    static func loadImageFromUrlWithIndicator(url: String, view: UIImageView, indic : UIActivityIndicatorView){
        
        // Create Url from string
        let url = NSURL(string: url)!
        
        // Download task:
        // - sharedSession = global NSURLCache, NSHTTPCookieStorage and NSURLCredentialStorage objects.
        let task = NSURLSession.sharedSession().dataTaskWithURL(url) { (responseData, responseUrl, error) -> Void in
            // if responseData is not null...
            if let data = responseData{
                
                // execute in UI thread
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    view.image = UIImage(data: data)
                    indic.stopAnimating()
                })
            }
        }
        
        // Run task
        task.resume()
    }

    
    static func loadImageFromUrlWithIndicatorAndSize(url: String, view: UIImageView, indic : UIActivityIndicatorView, size: CGSize){
        
        // Create Url from string
        let url = NSURL(string: url)!
        
        // Download task:
        // - sharedSession = global NSURLCache, NSHTTPCookieStorage and NSURLCredentialStorage objects.
        let task = NSURLSession.sharedSession().dataTaskWithURL(url) { (responseData, responseUrl, error) -> Void in
            // if responseData is not null...
            if let data = responseData{
                
                // execute in UI thread
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    let img = UIImage(data: data)
                    if img != nil
                    {
                        let newImage = self.resizeImage(img!, newWidth: size.width)
                        view.image = newImage
                    }
                    else
                    {
                        view.image = UIImage.init(named: "no_image.png")

                    }
                    indic.stopAnimating()
                })
            }
        }
        
        // Run task
        task.resume()
    }

    
    
    static func md5(string string: String) -> String {
        var digest = [UInt8](count: Int(CC_MD5_DIGEST_LENGTH), repeatedValue: 0)
        if let data = string.dataUsingEncoding(NSUTF8StringEncoding) {
            CC_MD5(data.bytes, CC_LONG(data.length), &digest)
        }
        
        var digestHex = ""
        for index in 0..<Int(CC_MD5_DIGEST_LENGTH) {
            digestHex += String(format: "%02x", digest[index])
        }
        
        return digestHex
    }
    
    static func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.height
        let newHeight = image.size.width * scale
        UIGraphicsBeginImageContext(CGSizeMake(newHeight,newWidth))
        image.drawInRect(CGRectMake(0, 0, newHeight, newWidth))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    

    
    static func getOpenCount() -> Int {
        let launches = NSUserDefaults.standardUserDefaults().integerForKey(Constants.RecipeOpenCount)
        return launches
    }
    
    static func incrementOpenCount(){
        var launches = NSUserDefaults.standardUserDefaults().integerForKey(Constants.RecipeOpenCount)
        launches += 1
        NSUserDefaults.standardUserDefaults().setInteger(launches, forKey: Constants.RecipeOpenCount)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    static func resetCount(){
        var launches = NSUserDefaults.standardUserDefaults().integerForKey(Constants.RecipeOpenCount)
        launches = 0
        NSUserDefaults.standardUserDefaults().setInteger(launches, forKey: Constants.RecipeOpenCount)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    
    static func GetModelArrayBasedOnTopCategory(type: Int){
        
    }
    
}
