//
//  CustomAlertController.m
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 06/05/16.
//  Copyright © 2016 Surbhi Varma. All rights reserved.
//

#import "CustomAlertController.h"

@implementation CustomAlertController

+(void)showAlertOnController : (UIViewController*)controller
               withAlertType : (AlertType)alertType
               andAttributes : (NSDictionary*)attributes {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"" preferredStyle:UIAlertControllerStyleAlert];

    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok"
                                                       style:UIAlertActionStyleDefault
                                                     handler:nil];
    
    [alertController addAction:okAction];
    
    switch (alertType) {
            
        case Simple:{
            
            [alertController setTitle:attributes[@"title"]];
            [alertController setMessage:attributes[@"message"]];
            
            break;
        }
        case Network:{
            
            [alertController setTitle:@"Mobile Data is Turned Off"];
            [alertController setMessage:@"Turn on mobile data or use Wi-Fi to access data"];
            
            [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
            UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:@"Settings"
                                                               style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                            
                                                                 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                                             }];
            [alertController addAction:settingsAction];
            
            break;
        }
    }
    
    [controller presentViewController:alertController
                                 animated:YES
                               completion:Nil];
}
@end
