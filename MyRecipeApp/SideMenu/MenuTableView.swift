//
//  MenuTableView.swift
//  TestSwiftApp
//
//  Created by Surbhi on 24/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit
import SafariServices

class MenuTableView: UITableViewController,SFSafariViewControllerDelegate {
    
    var menuArray : NSMutableArray = [ "Home", "Our Range", "Recipes", "Videos", "Add Recipe", "My Recipe", "Favourites", "User Notes","Notifications", "Share App", "Feedback", "About Us", "Contact Us", "Terms & Conditions", "Brand Term of Use","Quick Tour", "Login" ]

    var menuImageArray : NSMutableArray = [ "home", "range", "recipes", "videos", "edit", "myrecipe", "favourites", "usernotes","notiIcon", "shareApp", "feedback", "aboutUs", "contactus", "terms", "terms","tour", "logout" ]
    
    
    var selectedMenuItem : Int = 0
    var lastSelectedMenuItem : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ReloadMenuView), name: Constants.pushNotificationToUpdate, object: nil)
        
        
        self.navigationController?.navigationBarHidden = true

        // Customize apperance of table view
        tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0) //
        tableView.separatorStyle = .None
        tableView.backgroundColor = UIColor.clearColor()
        tableView.scrollsToTop = false
        
        self.view.backgroundColor = UIColor.darkGrayColor()
        
        // Preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false
        
        tableView.selectRowAtIndexPath(NSIndexPath(forRow: selectedMenuItem, inSection: 0), animated: false, scrollPosition: .Middle)

        if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == ""  || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil
        {
            menuArray = [ "Home", "Our Range", "Recipes", "Videos", "Add Recipe", "My Recipe","Favourites", "User Notes","Notifications", "Share App", "Feedback", "About Us", "Contact Us", "Terms & Conditions", "Brand Term of Use","Quick Tour", "Login" ]
        }
        else
        {
            menuArray = [ "Home", "Our Range", "Recipes", "Videos", "Add Recipe", "My Recipe", "Favourites", "User Notes","Notifications", "Share App", "Feedback", "About Us", "Contact Us", "Terms & Conditions", "Brand Term of Use","Quick Tour", "Logout" ]
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
   
    
    func  UpdateMenuView(){
        
        if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == ""  || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil {
            menuArray = [ "Home", "Our Range", "Recipes", "Videos", "Add Recipe", "My Recipe", "Favourites", "User Notes","Notifications", "Share App", "Feedback", "About Us", "Contact Us", "Terms & Conditions", "Brand Term of Use","Quick Tour", "Login" ]
        }
        else
        {
            menuArray = [ "Home", "Our Range", "Recipes", "Videos", "Add Recipe", "My Recipe", "Favourites", "User Notes","Notifications", "Share App", "Feedback", "About Us", "Contact Us", "Terms & Conditions", "Brand Term of Use","Quick Tour", "Logout" ]
        }
        
        tableView.reloadData()
    }
    
    
    func  ReloadMenuView(){
        
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if DeviceType.IS_IPAD {
            if NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED) == "" || NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED) == nil {
                return 100.0
            }
            else
            {
                return 150.0
            }
        }
        else{
            if NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED) == "" || NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED) == nil {
                return 75.0
            }
            else
            {
                return 105.0
            }
        }
    }
    
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        var headervw = UIView()
        if NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED) == "" || NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED) == nil
        {
            if DeviceType.IS_IPAD {
                headervw = UIView(frame: CGRectMake(0,0,tableView.frame.size.width,100))
                headervw.backgroundColor = UIColor.blackColor()
                self.addSubviewsToMainView(headervw)
            }
            else{
                headervw = UIView(frame: CGRectMake(0,0,tableView.frame.size.width,75))
                headervw.backgroundColor = UIColor.blackColor()
                self.addSubviewsToMainView(headervw)
            }
        }
        else
        {
            
            let modelArray = NSUserDefaults.standardUserDefaults().objectForKey(Constants.MODEL) as! Array<AnyObject>
            var Dict = Dictionary<String,AnyObject>()
            for item in modelArray {
                let dc = item as! Dictionary<String,AnyObject>
                let idSTRing = dc["id"] as! String
                let topID = dc["top_category_id"] as! String
                if idSTRing ==  NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED)  && topID == NSUserDefaults.standardUserDefaults().stringForKey(Constants.TOPModelSELCTED){
                    Dict = dc
                }
            }

            if DeviceType.IS_IPAD {
                headervw = UIView(frame: CGRectMake(0,0,tableView.frame.size.width,150))
                let bg = UIImageView.init(frame: CGRectMake(0, 0, tableView.frame.size.width, 100))
                bg.backgroundColor = UIColor.blackColor()
                headervw.addSubview(bg)
                self.addSubviewsToMainView(headervw)
                
                let modelImg = UIImageView.init(frame: CGRectMake(10, 110, 30, 30))
                let name = UILabel.init(frame: CGRectMake(50, 110, tableView.frame.size.width - 90, 30))
                name.font = UIFont(name: "Helvetica", size: 16)!
                name.numberOfLines = 1
                name.textColor = UIColor.whiteColor()
                let crossButton = UIButton.init(frame: CGRectMake(tableView.frame.size.width - 40, 108, 35, 35))
                crossButton.layer.cornerRadius = 11
                crossButton.clipsToBounds = true
                crossButton.contentMode = UIViewContentMode.Redraw
                
                modelImg.image = UIImage.init(named: "range")
                name.text = Dict["name"] as? String
                crossButton.setImage(UIImage.init(named: "whiteCross.png"), forState: UIControlState.Normal)
                crossButton.addTarget(self, action: #selector(DeleTEModelSelection), forControlEvents: UIControlEvents.TouchUpInside)
                
                
                headervw.addSubview(modelImg)
                headervw.addSubview(name)
                headervw.addSubview(crossButton)

            }
            else{
                headervw = UIView(frame: CGRectMake(0,0,tableView.frame.size.width,100))
                let bg = UIImageView.init(frame: CGRectMake(0, 0, tableView.frame.size.width, 75))
                bg.backgroundColor = UIColor.blackColor()
                headervw.addSubview(bg)
                self.addSubviewsToMainView(headervw)
                
                let modelImg = UIImageView.init(frame: CGRectMake(5, 80, 20, 20))
                let name = UILabel.init(frame: CGRectMake(30, 80, tableView.frame.size.width - 70, 20))
                name.font = UIFont(name: "Helvetica", size: 13)!
                name.numberOfLines = 1
                name.textColor = UIColor.whiteColor()
                let crossButton = UIButton.init(frame: CGRectMake(tableView.frame.size.width - 25, 78, 22, 22))
                crossButton.layer.cornerRadius = 11
                crossButton.clipsToBounds = true
                crossButton.contentMode = UIViewContentMode.Redraw
                
                modelImg.image = UIImage.init(named: "range")
                name.text = Dict["name"] as? String
                crossButton.setImage(UIImage.init(named: "whiteCross.png"), forState: UIControlState.Normal)
                crossButton.addTarget(self, action: #selector(DeleTEModelSelection), forControlEvents: UIControlEvents.TouchUpInside)
                
                
                headervw.addSubview(modelImg)
                headervw.addSubview(name)
                headervw.addSubview(crossButton)

            }
            
            headervw.backgroundColor = Colors.appThemeColor

        }
        
        return headervw
    }
    
    func addSubviewsToMainView(hederView : UIView)
    {
        let profile = UIImageView(frame: CGRectMake(10, 12.5, 50, 50))
        profile.layer.cornerRadius = 25
        profile.layer.borderWidth = 1
        profile.layer.borderColor = UIColor.whiteColor().CGColor
        profile.layer.backgroundColor = UIColor.clearColor().CGColor
        profile.image = UIImage.init(named: "profile_PlaceHolder.png")
        profile.contentMode = UIViewContentMode.ScaleAspectFill
        profile.clipsToBounds = true
        
        let nameLabel = UILabel(frame: CGRectMake (65,10,tableView.frame.size.width-70,22))
        nameLabel.font = UIFont(name: "HelveticaNeue-Medium", size: 14)!
        nameLabel.textColor = UIColor.whiteColor()
        
        let editProfileBtn = UIButton(frame: CGRectMake (65,35,tableView.frame.size.width-70,22))
        editProfileBtn.setImage(UIImage.init(named: "loginEdit"), forState:UIControlState.Normal)
        editProfileBtn.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        editProfileBtn.titleLabel?.font = UIFont(name: "Helvetica", size: 13)!
        editProfileBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left

        if DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_6P
        {
            nameLabel.font = UIFont(name: "HelveticaNeue-Medium", size: 15)!
            editProfileBtn.titleLabel?.font = UIFont(name: "HelveticaNeue-Medium", size: 15)!
        }
        
        if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == "" || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil {
            
            nameLabel.text = "Guest"
            editProfileBtn.setTitle(" Login", forState: UIControlState.Normal)
            editProfileBtn.addTarget(self, action: #selector(LoginPressed), forControlEvents: UIControlEvents.TouchUpInside)
        }
        else
        {
            let dict = NSUserDefaults.standardUserDefaults().objectForKey(Constants.PROFILE) as? NSDictionary
            nameLabel.text = dict!.valueForKey(Constants.NAME)! as? String
            editProfileBtn.setTitle("Edit Profile", forState: UIControlState.Normal)
            editProfileBtn.addTarget(self, action: #selector(editProfilePressed), forControlEvents: UIControlEvents.TouchUpInside)
            
            let pict = dict!["profile_pic"] as? String
            if pict != "" || pict != nil {
                Helper.loadImageFromUrl(pict!, view:profile,width: 50.0)
            }
        }
        
        if DeviceType.IS_IPAD {
            profile.frame = CGRectMake(15, 12.5, 75, 75)
            profile.layer.cornerRadius = 37.5
            nameLabel.frame = CGRectMake (97,15,tableView.frame.size.width-105,30)
            editProfileBtn.frame = CGRectMake (97,45,tableView.frame.size.width-105,30)
            
            nameLabel.font = UIFont(name: "HelveticaNeue-Medium", size: 19)!
            editProfileBtn.titleLabel?.font = UIFont(name: "HelveticaNeue-Medium", size: 19)!
        }
        
        hederView.addSubview(profile)
        hederView.addSubview(nameLabel)
        hederView.addSubview(editProfileBtn)

    }
    
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_6P
        {
            return 45;
        }
        else if DeviceType.IS_IPAD{
            return 60
        }
        return 40;
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return menuArray.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("CELL")
        
    
        if (cell == nil) {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "CELL")
            cell!.backgroundColor = UIColor.clearColor()
            cell!.textLabel?.textColor = UIColor.whiteColor()
            cell!.textLabel?.font = UIFont(name: "Helvetica", size: 13)!
            
            if DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_6P
            {
                cell!.textLabel?.font = UIFont(name: "Helvetica", size: 14.5)!
            }
            if DeviceType.IS_IPAD {
                cell!.textLabel?.font = UIFont(name: "Helvetica", size: 18.0)!
            }

            
            let selectedBackgroundView = UIView(frame: CGRectMake(0, 0, cell!.frame.size.width, cell!.frame.size.height - 4.5))
            selectedBackgroundView.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.2)
            cell!.selectedBackgroundView = selectedBackgroundView
        }

        cell!.textLabel?.text = menuArray[indexPath.row] as? String
        cell?.imageView?.image = UIImage.init(named: (menuImageArray[indexPath.row] as? String)!)
        
        if (cell?.contentView.viewWithTag(11)) != nil
        {
            let vw = cell?.contentView.viewWithTag(11)
            vw?.removeFromSuperview()
        }
        
        
        let line = UILabel.init(frame: CGRectMake(0, (cell?.frame.size.height)! - 3.5, (cell?.frame.size.width)!, 1))
        line.tag = 11
        
        if DeviceType.IS_IPAD {
            line.frame = CGRectMake(0, (cell!.textLabel!.frame.minY  + cell!.textLabel!.frame.size.height) + 1, (cell?.frame.size.width)!, 1.5)
        }
        line.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.5)
        cell?.contentView.addSubview(line)
        
        
        if (cell?.contentView.viewWithTag(12)) != nil
        {
            let vw = cell?.contentView.viewWithTag(12)
            vw?.removeFromSuperview()
        }
        
        if indexPath.row == 8 {
            
            let countlabel = UILabel.init(frame: CGRectMake(cell!.frame.size.width - 30, (cell!.frame.size.height - 22)/2, 22, 22))
            countlabel.tag = 12
            
            if DeviceType.IS_IPAD {
                countlabel.frame = CGRectMake(cell!.frame.size.width - 40, (cell!.frame.size.height - 35)/2, 35, 35)
            }

            
            let oldCount = NSUserDefaults.standardUserDefaults().integerForKey("notiCount")
            let newCount = NSUserDefaults.standardUserDefaults().integerForKey("NotificationCount")
            
            if newCount > oldCount
            {
                countlabel.font = UIFont(name: "Helvetica", size: 12.5)!
                countlabel.layer.cornerRadius = 11.0

                if DeviceType.IS_IPAD {
                    countlabel.font = UIFont(name: "Helvetica", size: 15)!
                    countlabel.layer.cornerRadius = 17.5
                }

                let finalCount = newCount - oldCount
                countlabel.text = String(format: "\(finalCount)")
                countlabel.layer.borderColor = UIColor.whiteColor().CGColor
                countlabel.layer.borderWidth = 1.0
                countlabel.textAlignment = NSTextAlignment.Center
                countlabel.textColor = UIColor.whiteColor()
                countlabel.clipsToBounds = true
                cell?.contentView.addSubview(countlabel)
                countlabel.hidden = false
            }
            else
            {
                countlabel.hidden = true
            }
        }
        
        return cell!
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if (indexPath.row == selectedMenuItem && lastSelectedMenuItem != selectedMenuItem){
            
            if (selectedMenuItem == 2 && (NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED) == "" || NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED) == nil))
            {
                NSNotificationCenter.defaultCenter().postNotificationName("ShowModelSelectionView", object: nil, userInfo: nil)
            }
            if (selectedMenuItem == 1)
            {
                NSNotificationCenter.defaultCenter().postNotificationName("ShowModelSelectionVw", object: nil, userInfo: nil)
            }
            
            hideSideMenuView()
            return
        }

        //Present new view controller
        var destViewController : UIViewController
        switch (indexPath.row) {
        case 0:
            destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("DashBoard")
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            Constants.appDelegate.isSafariOpen = false
            sideMenuController()?.setContentViewController(destViewController)
            break

        case 1:
            destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("OurRangeMain")
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            sideMenuController()?.setContentViewController(destViewController)
            break
            
        case 2:
            destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("RecipeCategoryVC")
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            sideMenuController()?.setContentViewController(destViewController)
            break

        case 3:
            destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("VideoVC")
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row

            sideMenuController()?.setContentViewController(destViewController)
            break
            
        case 4: // ADD RECIPE //AddRecipeVC
            if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == "" || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil
            {
                let alert = UIAlertController.init(title: "Culineria", message: "Login to create your own recipe.", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction.init(title: "LOGIN", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                    self.lastSelectedMenuItem = indexPath.row
                    
                    NSUserDefaults.standardUserDefaults().setBool(true, forKey: "LoginRequiredError")
                    
                    self.LoginPressed()
                }))
                alert.addAction(UIAlertAction.init(title: "CANCEL", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                    self.dismissViewControllerAnimated(true, completion: nil)
                }))
                self.presentViewController(alert, animated: true, completion: nil)
            }
            else
            {
                destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("AddRecipeVC")
                lastSelectedMenuItem = selectedMenuItem
                selectedMenuItem = indexPath.row
                sideMenuController()?.setContentViewController(destViewController)
            }
            break
            
        case 5: // MY RECIPE
            if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == "" || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil
            {
                let alert = UIAlertController.init(title: "Culineria", message: "Login to continue.", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction.init(title: "LOGIN", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                    self.lastSelectedMenuItem = indexPath.row

                    self.LoginPressed()
                }))
                alert.addAction(UIAlertAction.init(title: "CANCEL", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                    self.dismissViewControllerAnimated(true, completion: nil)
                }))
                self.presentViewController(alert, animated: true, completion: nil)

            }
            else
            {
                destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("MyRecipeVC")
                lastSelectedMenuItem = selectedMenuItem
                selectedMenuItem = indexPath.row
                sideMenuController()?.setContentViewController(destViewController)
            }
            break
            
        case 6:
            if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == "" || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil
            {
                let alert = UIAlertController.init(title: "Culineria", message: "Login to continue.", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction.init(title: "LOGIN", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                    self.lastSelectedMenuItem = indexPath.row

                    self.LoginPressed()
                }))
                alert.addAction(UIAlertAction.init(title: "CANCEL", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                    self.dismissViewControllerAnimated(true, completion: nil)
                }))
                self.presentViewController(alert, animated: true, completion: nil)
                
            }
            else
            {
                destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("FavVC")
                lastSelectedMenuItem = selectedMenuItem
                selectedMenuItem = indexPath.row
                sideMenuController()?.setContentViewController(destViewController)
            }
            break
            
        case 7: // USER NOTES
            if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == "" || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil
            {
                let alert = UIAlertController.init(title: "Culineria", message: "Login to continue.", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction.init(title: "LOGIN", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                    self.lastSelectedMenuItem = indexPath.row

                    self.LoginPressed()
                }))
                alert.addAction(UIAlertAction.init(title: "CANCEL", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                    self.dismissViewControllerAnimated(true, completion: nil)
                }))
                self.presentViewController(alert, animated: true, completion: nil)
                
            }
            else
            {
                print(Constants.mainStoryboard)
                print(DeviceType.IS_IPAD)
                print(ScreenSize.SCREEN_MAX_LENGTH)
                destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("NotesVC")
                lastSelectedMenuItem = selectedMenuItem
                selectedMenuItem = indexPath.row
                sideMenuController()?.setContentViewController(destViewController)
            }
            break

//        NotificationVC
        case 8:
            destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("NotificationVC")
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            
            sideMenuController()?.setContentViewController(destViewController)
            break
            
        case 9: // SHARE APP
            let shareUrl = "https://itunes.apple.com/us/app/whirlpool-culineria/id1185192155?ls=1&mt=8"
            let myWebsite = NSURL.init(string: shareUrl)
            
            let activityVC = UIActivityViewController.init(activityItems: [myWebsite!], applicationActivities: nil)
            let excludeActivities = [UIActivityTypeAirDrop,UIActivityTypePrint,UIActivityTypeAssignToContact,UIActivityTypeSaveToCameraRoll,UIActivityTypeAddToReadingList]
            activityVC.excludedActivityTypes = excludeActivities;
            
            
            if UIApplication.sharedApplication().keyWindow?.rootViewController?.isKindOfClass(SideMenuNavigation) == true {
                
                let baseVC = UIApplication.sharedApplication().keyWindow?.rootViewController?.childViewControllers.first
                
                if DeviceType.IS_IPAD {
                    activityVC.popoverPresentationController!.sourceView = baseVC!.view;
                }

                baseVC?.presentViewController(activityVC, animated: true, completion: {
                    self.toggleSideMenuView()
                    AppsFlyerTracker.sharedTracker().trackEvent("Share App", withValue: "Success") // APPSFlyer

                })

            }
          
            break
            

        case 10: // FEEDBACK
            if NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == "" || NSUserDefaults.standardUserDefaults().stringForKey(Constants.UserID) == nil
            {
                let alert = UIAlertController.init(title: "Culineria", message: "Login to continue.", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction.init(title: "LOGIN", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                    self.lastSelectedMenuItem = indexPath.row
                    self.LoginPressed()
                }))
                alert.addAction(UIAlertAction.init(title: "CANCEL", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                    self.dismissViewControllerAnimated(true, completion: nil)
                }))
                self.presentViewController(alert, animated: true, completion: nil)
                
            }
            else
            {
                destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("FeedbackVC")
                lastSelectedMenuItem = selectedMenuItem
                selectedMenuItem = indexPath.row
                sideMenuController()?.setContentViewController(destViewController)
            }
            break
            
            
        case 11: // ABOUT US
            destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("webViewVC") as! webViewVC
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row

            sideMenuController()?.setContentViewController(destViewController)
            break
            
        case 12: // CONTACT US
            destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("webViewVC") as! webViewVC
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            
            sideMenuController()?.setContentViewController(destViewController)
            break
            
        case 13: // TERMS & CONDITIONS
            destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("TermsConditionVC") as! TermsConditionVC
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            sideMenuController()?.setContentViewController(destViewController)

            break
            
        case 14: // BRAND TERMS OF USE

            destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("DashBoard") as! DashBoardVC
            Constants.appDelegate.isSafariOpen = true
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = 0

            sideMenuController()?.setContentViewController(destViewController)
            break
        
        case 15: // Tutorial
            let destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("TutorialVC")
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            sideMenuController()?.setContentViewController(destViewController)
            break
            
        case 16:
//            lastSelectedMenuItem = selectedMenuItem
//            selectedMenuItem = indexPath.row

                if self.menuArray[indexPath.row] as! String == "Login"
                {
                    destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("LoginView")
                    self.presentViewController(destViewController, animated: true, completion: nil);
                }
                else
                {
                    let alertController = UIAlertController.init(title: "Culineria", message: "Are you sure, you want to logout?", preferredStyle: UIAlertControllerStyle.Alert)
                    let  yesButton = UIAlertAction.init(title: "OK", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                        
//                        GIDSignIn.sharedInstance().delegate = self
                        GIDSignIn.sharedInstance().signOut()

                        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
                        loginManager.logOut()

                        Helper.resetCount()
                        
                        NSUserDefaults.standardUserDefaults().removeObjectForKey(Constants.UserID)
                        NSUserDefaults.standardUserDefaults().removeObjectForKey(Constants.PROFILE)
                        NSUserDefaults.standardUserDefaults().removeObjectForKey(Constants.PASSWORD)

                        self.UpdateMenuView()
                        let destinationController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("LoginView")
                        self.presentViewController(destinationController, animated: true, completion: nil);

                    })
                    let noButton = UIAlertAction.init(title: "CANCEL", style: UIAlertActionStyle.Cancel, handler: { (action:UIAlertAction) in
//                        self.UpdateMenuView()
//                        let destViewController2 = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("DashBoard")
//                        self.sideMenuController()?.setContentViewController(destViewController2)
                        self.tableView.selectRowAtIndexPath(NSIndexPath(forRow: self.selectedMenuItem, inSection: 0), animated: false, scrollPosition: UITableViewScrollPosition.None)

                    })
                    alertController.addAction(noButton)
                    alertController.addAction(yesButton)

                    self.presentViewController(alertController, animated: true, completion: {
                    })
                }
            break
            
        default:
            break
            
        }
    }
    
    func editProfilePressed(){
        if selectedMenuItem != 200 {
            selectedMenuItem = 200
            let destinationController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("EditProfile")
            sideMenuController()?.setContentViewController(destinationController)
        }
    }
    
    func LoginPressed(){
        if selectedMenuItem != 200 {
            selectedMenuItem = 200
            
            let destinationController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("LoginView")
            self.presentViewController(destinationController, animated: true, completion: nil);
        }
    }
    
    
    func UPDATEHeaderView(selection : Int)
    {
        if NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED) == "" || NSUserDefaults.standardUserDefaults().stringForKey(Constants.ModelSELCTED) == nil
        {
            NSUserDefaults.standardUserDefaults().setObject(false, forKey: Constants.ISModelSELCTED)
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setObject(true, forKey: Constants.ISModelSELCTED)
        }
        
        dispatch_async(dispatch_get_main_queue())
        {
            if selection == 0{
                let vie = self.tableView(self.tableView, viewForHeaderInSection: 0)
                vie?.setNeedsDisplay()
//                self.lastSelectedMenuItem = self.selectedMenuItem
//                self.selectedMenuItem = 0
                self.tableView.reloadSections(NSIndexSet.init(index: 0), withRowAnimation: UITableViewRowAnimation.Automatic)
                self.tableView.selectRowAtIndexPath(NSIndexPath(forRow: self.selectedMenuItem, inSection: 0), animated: false, scrollPosition: .Middle)

            }
            else
            {
                let vie = self.tableView(self.tableView, viewForHeaderInSection: 0)
                vie?.setNeedsDisplay()
                self.lastSelectedMenuItem = self.selectedMenuItem
                self.selectedMenuItem = 2
                
                self.tableView.reloadSections(NSIndexSet.init(index: 0), withRowAnimation: UITableViewRowAnimation.Automatic)
                self.tableView.selectRowAtIndexPath(NSIndexPath(forRow: self.selectedMenuItem, inSection: 0), animated: false, scrollPosition: .Middle)

            }
        }
    }
    
    func DeleTEModelSelection()
    {
        NSUserDefaults.standardUserDefaults().removeObjectForKey(Constants.ModelSELCTED)
        self .UPDATEHeaderView(0)
    }
    
//    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!, withError error: NSError!) {
//        
//    }
//    
//    
//    
//    func signIn(signIn: GIDSignIn!, didDisconnectWithUser user:GIDGoogleUser!,
//                withError error: NSError!) {
//        
//        
//        
//        //        print("Logging Out GOOGLE ")
//        
//    }
}
